<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Customer;

use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    /**
     * @var object
     */
    protected $resultPageFactory;

    /**
     * Constuctor
     *
     * @param Data $helper
     * @param Config $config
     */
    public function __construct(
        Data $helper,
        Config $config,
        RedirectFactory $resultRedirectFactory,
        PageFactory $resultPageFactory
    ) {
        $this->helper = $helper;
        $this->config = $config;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $isEnabled = $this->config->getModuleStatus();
        $customerIsLogin = $this->helper->getLoginCustomerId();
        if ($isEnabled == 1 && !empty($customerIsLogin)) {
            $resultPage = $this->resultPageFactory->create();
            return $resultPage;
            
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account');
            return $resultRedirect;
        }
    }
}
