<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsers;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Message\ManagerInterface;

class Delete implements HttpPostActionInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param ClubSilhouetteUsers $clubsilhouetteFactory
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     * @param Redirect $resultRedirect
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ClubSilhouetteUsers $clubsilhouetteFactory,
        LoggerInterface $logger,
        RequestInterface $request,
        Redirect $resultRedirect,
        ManagerInterface $messageManager
    ) {
        $this->clubsilhouetteFactory = $clubsilhouetteFactory;
        $this->logger = $logger;
        $this->request = $request;
        $this->resultRedirectFactory = $resultRedirect;
        $this->messageManager = $messageManager;
    }
    
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->request->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->clubsilhouetteFactory;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('User is deleted successfully.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->logger->error($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('User does not exist.'));
        return $resultRedirect->setPath('*/*/');
    }
}
