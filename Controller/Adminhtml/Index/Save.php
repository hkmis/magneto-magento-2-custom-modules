<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magento\Backend\Model\Session;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;

class Save implements HttpPostActionInterface
{
    /**
     * @var \ClubSilhouetteUsers
     */
    public $clubsilhouettemodel;

    /**
     * @var \Session
     */
    public $adminsession;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param ClubSilhouetteUsersFactory $clubsilhouettemodel
     * @param Session $adminsession
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ClubSilhouetteUsersFactory $clubsilhouettemodel,
        Session $adminsession,
        LoggerInterface $logger,
        RequestInterface $request,
        ResultFactory $resultFactory,
        ManagerInterface $messageManager
    ) {
        $this->clubsilhouettemodel = $clubsilhouettemodel;
        $this->logger = $logger;
        $this->adminsession = $adminsession;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
    }
    /**
     * Save blog record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $paymentCycleMonths = '';
        $cardId='';
        $refill1ItemNo='';
        $refill2ItemNo='';
        $shippingId='';

        $data = $this->request->getPostValue();    
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);        
        if ($data) {

            if (isset($data['membership'])) {
                $membershipData = $data['membership'];
                $membershipStatus = $membershipData['membershipStatus'];
                $endDate = $membershipData['endDate'];
                if (isset($membershipData['paymentcycle'])) {
                    $paymentCycleMonths = $membershipData['paymentcycle'];
                }
                if (isset($membershipData['creditcard'])) {
                    $cardId = $membershipData['creditcard'];
                }
                if (isset($membershipData['mat'])) {
                    $refill1ItemNo = $membershipData['mat'];
                }
                if (isset($membershipData['blade'])) {
                    $refill2ItemNo = $membershipData['blade'];
                }
                if (isset($membershipData['shippingId'])) {
                    $shippingId = $membershipData['shippingId'];
                }
            }else{
                $membershipStatus = $data['membershipStatus'];
                $paymentCycleMonths = $data['paymentCycleMonths'];
                $cardId = $data['cardId'];
                $refill1ItemNo = $data['refill1ItemNo'];
                $refill2ItemNo = $data['refill2ItemNo'];
                $shippingId = $data['shippingId'];
            }

            $currentId = $this->request->getParam('id');
            try {
                if ($currentId) {
                    $model = $this->clubsilhouettemodel->create()->load($currentId);
                    if ($model->getId()) {
                        $model->setData('membershipStatus', $membershipStatus);
                        $model->setData('endDate', $endDate);
                        if ($paymentCycleMonths) {
                            $model->setData('paymentCycleMonths', $paymentCycleMonths);
                        }
                        if ($cardId) {
                            $model->setData('cardId', $cardId);
                        }
                        if ($refill1ItemNo) {
                            $model->setData('refill1ItemNo', $refill1ItemNo);
                        }
                        if ($refill2ItemNo) {
                            $model->setData('refill2ItemNo', $refill2ItemNo);
                        }
                        if ($shippingId) {
                            $model->setData('shippingId', $shippingId);
                        }
                        $model->Save();
                    }
                    $this->messageManager->addSuccess(__('The club silhouette user has been saved.'));
                    $this->adminsession->setFormData(false);
                    if ($this->request->getParam('back')) {
                        if ($this->request->getParam('back') == 'add') {
                            return $resultRedirect->setPath('*/*/add');
                        } else {
                            return $resultRedirect->setPath('*/*/edit', ['id' => $this->clubsilhouettemodel->getBlogId(),
                                '_current' => true]);
                        }
                    }
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                $this->messageManager->addException('There is some issue while saving record.Please try again.');
                $this->logger->error($e->getMessage());
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->request->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
