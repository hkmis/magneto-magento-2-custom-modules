<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magento\Backend\Model\Session;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as ClubSilhouetteUsers;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteActionsFactory as ClubUsersActions;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Clubpoints implements HttpPostActionInterface
{

    private const ACTION_RECEIVE_POINTS = 'P';

    /**
     * Constructor
     *
     * @param ClubSilhouetteUsers $clubuserFactory
     * @param Session $adminsession
     * @param Data $helper
     * @param ManagerInterface $messageManager
     * @param LoggerInterface $logger
     * @param ClubUsersActions $clubUsersActions
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        ClubSilhouetteUsers $clubuserFactory,
        Session $adminsession,
        Data $helper,
        ManagerInterface $messageManager,
        LoggerInterface $logger,
        ClubUsersActions $clubUsersActions,
        RequestInterface $request,
        ResultFactory $resultFactory
    ) {
        $this->clubuserFactory = $clubuserFactory;
        $this->adminsession = $adminsession;
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
        $this->clubUsersActions = $clubUsersActions;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
    }
    /**
     * Save educator record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->request->getParams();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $id = $data['id'];
            $points = $data['points'];
            $pointsComm = $data['pointsComm'];
            try {
                if ($id) {
                    // Save Points
                    $model = $this->clubuserFactory->create();
                    $model->load($id, 'id');
                    $currentPoints = $model->getData('points');
                    $userId = $model->getData('userId');
                    $points = $currentPoints + $points;
                    $model->setPoints($points);
                    $model->save();
                    // Save action in silhouette action table
                    $ipAddress = $this->helper->getCustomerIp();
                    $action = self::ACTION_RECEIVE_POINTS;
                    $saveAction = $this->helper->saveClubUsersActions($userId,$action,$pointsComm,$ipAddress,
                        $points);
                    if ($saveAction) {
                        $resultJson->setData(["success" => true, "points" => $points]);
                        return $resultJson;
                    }

                }
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $resultJson->setData(["failure" => true]);
                return $resultJson;
            }
        }
    }
}
