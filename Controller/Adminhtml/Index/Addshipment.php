<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteRefillShipmentsFactory as ClubSilhouetteShipments;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;

class Addshipment implements HttpPostActionInterface
{
    /**
     * Constructor
     *
     * @param ClubSilhouetteShipments $clubSilhouetteShipments
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        ClubSilhouetteShipments $clubSilhouetteShipments,
        Data $helper,
        LoggerInterface $logger,
        RequestInterface $request,
        ResultFactory $resultFactory
    ) {
        $this->clubSilhouetteShipments = $clubSilhouetteShipments;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
    }
    /**
     * Save payment record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->request->getParams();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $action  = $data['action'];
            $userId = $data['id'];
            if ($action == 'shipOn') {
                $shipDate = $data['shipDate'];
                
                $recordData = [];
                $recordData['userId'] = $userId;
                $recordData['scheduledDate'] = $shipDate;
                try {
                        // Save Shipment
                        $model = $this->clubSilhouetteShipments->create();
                        $model->setData($recordData);
                        $model->save();
                        
                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }

            } elseif ($action == 'startOn') {
                $startDate = $data['startDate'];
                $startDate = date('Y-m-d H:i:s', strtotime($startDate) + (60 * 60 * 24));
                $numberShip = (int)$data['numberShip'];

                $dates_arr = $this->helper->annualShipmentDatesArr($startDate, $numberShip);
                try {
                        $model = $this->clubSilhouetteShipments->create();
                    foreach ($dates_arr as $date) {
                        $recordData = [];
                        $recordData['userId'] = $userId;
                        $recordData['scheduledDate'] = $date;
                        $model->setData($recordData);
                        $model->save();
                    }
                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }

            } elseif ($action == 'deleteAll') {
                try {
                        // delete payment
                        $model = $this->clubSilhouetteShipments->create()->getCollection();
                        $records = $model->addFieldToFilter('userId', $userId);
                    ;
                    // Delete the records
                    foreach ($records as $record) {
                        $record->delete();
                    }

                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }
            } else {
                try {
                        // delete payment
                        $deleteId = $data['deleteId'];
                        $model = $this->clubSilhouetteShipments->create();
                        $model->load($deleteId);
                        $model->delete();
    
                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }
            }
            
        }
    }
}
