<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePaymentsFactory as ClubSilhouettePayments;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Addpayment implements HttpPostActionInterface
{

    /**
     * Constructor
     *
     * @param ClubSilhouettePayments $clubSilhouettePayments
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        ClubSilhouettePayments $clubSilhouettePayments,
        Data $helper,
        LoggerInterface $logger,
        RequestInterface $request,
        ResultFactory $resultFactory
    ) {
        $this->clubSilhouettePayments = $clubSilhouettePayments;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
    }
    /**
     * Save payment record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->request->getParams();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $action  = $data['action'];
            if ($action == 'add') {
                $userId = $data['id'];
                $dueDate = $data['dueDate'];
                $amount = $data['amount'];
                
                $recordData = [];
                $recordData['userId'] = $userId;
                $recordData['dueDate'] = $dueDate;
                $recordData['amt'] = $amount;
                try {
                        // Save Payment
                        $model = $this->clubSilhouettePayments->create();
                        $model->setData($recordData);
                        $model->save();
                        
                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }

            } else {
                $id = $data['id'];
                try {
                        // delete payment
                        $model = $this->clubSilhouettePayments->create();
                        $model->load($id);
                        $model->delete();

                        $resultJson->setData(["success" => true]);
                        return $resultJson;
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                    $resultJson->setData(["failure" => true]);
                    return $resultJson;
                }

            }
            
        }
    }
}
