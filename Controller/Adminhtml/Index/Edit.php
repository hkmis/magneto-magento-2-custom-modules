<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

class Edit implements HttpGetActionInterface
{

    /**
     * Constructor
     *
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        ResultFactory $resultFactory
    ) {
        $this->resultFactory = $resultFactory;
    }

    /**
     * Edit Action
     *
     * @param Edit Maintenance Banners
     */
    
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Club Silhouette User'));
        return $resultPage;
    }
}
