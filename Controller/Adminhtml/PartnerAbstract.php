<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml;

use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;
use Magento\Backend\App\Action\Context;

/**
 * PartnerAbstract Abstract Class
 * @package MagentoItSolutions\PartnerProgram\Controller\Adminhtml
 *
 */
abstract class PartnerAbstract extends \Magento\Backend\App\Action
{

    public const ADMIN_RESOURCE = 'Magneto_ClubSilhouetteManager::top_level';

    /**
     * @var Registry $coreRegistry
     */
    protected Registry $coreRegistry;

    /**
     * Constructor Method
     *
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage(Page $resultPage): Page
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Magneto'), __('Magneto'))
            ->addBreadcrumb(__('Bundel'), __('Bundel'));
        return $resultPage;
    }
}

