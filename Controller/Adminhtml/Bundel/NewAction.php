<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Bundel;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magneto\ClubSilhouetteManager\Controller\Adminhtml\BundelAbstract;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Controller\ResultInterface;

/**
 * NewAction class
 *
 * @package Magneto/ClubSilhouetteManager/Controller/Adminhtml/Bundel
 */
class NewAction extends BundelAbstract
{

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * New action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}

