<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Bundel;

use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magneto\ClubSilhouetteManager\Controller\Adminhtml\BundelAbstract;
use Magneto\ClubSilhouetteManager\Model\Bundel;

/**
 * Delete Class
 *
 * @package Magneto/ClubSilhouetteManager/Controller/Adminhtml/Bundel
 */
class Delete extends BundelAbstract
{
    public function __construct(Bundel $bundle
    ) 
    {
        $this->bundle = $bundle;
    }
    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            try {
                // init model and delete
                $model = $this->bundle;
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Bundle.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Bundle to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

