<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Bundel;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magneto\ClubSilhouetteManager\Api\Data\BundelInterface;
use Magneto\ClubSilhouetteManager\Api\BundelRepositoryInterface;

/**
 * Save Class
 * @package MagnetoItSolutions/BundelProgram/Controller/Adminhtml/Bundel
 */
class Save extends Action
{

    /**
     * @var DataPersistorInterface
     */
    protected DataPersistorInterface $dataPersistor;    

    /**
     * @var BundelInterface
     */
    private BundelInterface $BundelInterface;

    /**
     * @var BundelRepositoryInterface
     */
    private BundelRepositoryInterface $BundelRepository;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param BundelInterface $bundelInterface
     * @param BundelRepositoryInterface $bundelRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        BundelInterface $bundelInterface,
        BundelRepositoryInterface $bundelRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->bundelInterface = $bundelInterface;
        $this->bundelRepository = $bundelRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $startTime = microtime(true);
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        if ($postData) {
            if ($id = $this->getRequest()->getParam('id')) {
                $id= (int)$id;
                $bundelData = $this->bundelRepository->get($id);
                if (!$bundelData->getId() && $id) {
                    $this->messageManager->addErrorMessage(__('This Bundle no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $this->bundelInterface->setData($postData);

            try {
                $bundelData = $this->bundelRepository->save($this->bundelInterface);
                $this->messageManager->addSuccessMessage(__('You saved the Bundle.'));
                $this->dataPersistor->clear('_clubSilhouetteExclusiveDesignBundles');

                $endTime = microtime(true);
                $timeElapsed = $endTime - $startTime;                

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $bundelData->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {                
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Bundle.'));
            }

            $this->dataPersistor->set('_clubSilhouetteExclusiveDesignBundles', $postData);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
