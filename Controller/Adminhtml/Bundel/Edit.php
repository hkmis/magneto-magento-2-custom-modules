<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Controller\Adminhtml\Bundel;

use Magneto\ClubSilhouetteManager\Controller\Adminhtml\BundelAbstract;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Backend\Model\View\Result\Page;
use Magneto\ClubSilhouetteManager\Model\Bundel;
/**
 * Edit class
 *
 * @package Magneto/ClubSilhouetteManager/Controller/Adminhtml/Bundel
 */
class Edit extends BundelAbstract
{

    protected PageFactory $resultPageFactory;

    /**
     * Constructor Class
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        Bundel $bundle
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->bundle = $bundle;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->bundle;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Bundle no longer exists.'));
                /**
                 * @var Redirect $resultRedirect
                 * @suppressWarning
                 */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->coreRegistry->register('_clubSilhouetteExclusiveDesignBundles', $model);

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Bundle') : __('New Bundle'),
            $id ? __('Edit Bundle') : __('New Bundle')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Exclusive Bundles'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Bundle %1', $model->getBundleName()) : __('New Bundle'));
        return $resultPage;
    }
}

