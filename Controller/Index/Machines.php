<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magneto\SerialNumberLogger\Model\MachineTypeFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use DateTime;
use DateInterval;

class Machines implements HttpGetActionInterface
{

    private const REFILL_SHIPMENTS_PER_YEAR = 3;
    
    /**
     * Constructor
     *
     * @param MachineTypeFactory $machineType
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        MachineTypeFactory $machineType,
        ResultFactory $resultFactory
    ) {
        $this->machineType = $machineType;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute Method
     *
     * @return array
     */
    public function execute()
    {
        $collection = $this->machineType->create()->getCollection()->getData();
        $html = "";
        foreach ($collection as $machine) {
            $machineName = $machine['machine_name'];
            $machineType = $machine['product_type'];

            $html .= '<option value="'.$machineType.'">'.$machineName.'</option>';
        }

        // Shipment Dates
        $payment_date = new DateTime();
        $payment_date->add(DateInterval::createFromDateString(30 . ' days'));
        $shipmentDates = $this->shipmentDatesArray($payment_date->format('Y-m-d H:i:s'));
        $shipmentDatesHtml = '';
        foreach ($shipmentDates as $date) {
            $shipmentDatesHtml .= '<span class="shipment-date" style="font-weight:bold">'.$date.'</span>';
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["success" => true, 'machines' => $html, 'shipmentDates' => $shipmentDatesHtml]);
        return $resultJson;
    }

    /**
     * Get Annual Shipment Dates
     *
     * @param date $start_date
     * @param string $number_of_shipments
     * @return array
     */
    private function shipmentDatesArray(
        $start_date,
        int $number_of_shipments = self::REFILL_SHIPMENTS_PER_YEAR
    ): ?array {
        $date = new DateTime($start_date);
        $dates_arr = [];
        $num_days_to_add = (int) (365 / self::REFILL_SHIPMENTS_PER_YEAR);

        //loop and add them, avoiding weekends
        for ($i = 0; $i < $number_of_shipments; $i++):

            //is it a weekend?
            switch (date('w', $date->getTimestamp())):

                //saturday?
                case 6:
                    $date->add(DateInterval::createFromDateString('2 days'));
                    break;

                //sunday?
                case 7:
                    $date->add(DateInterval::createFromDateString('1 day'));
                    break;
            endswitch;

            //add it to the array
            $dates_arr[] = $date->format('F d,Y');

            //update the date
            $date->add(DateInterval::createFromDateString($num_days_to_add . ' days'));
        endfor;

        return $dates_arr;
    }
}
