<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magento\Checkout\Model\SessionFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Checkout\Model\Cart;

class Addtocart implements HttpPostActionInterface
{

    /**
     * Construction
     *
     * @param Context $context
     * @param Session $session
     * @param SessionFactory $checkoutSession
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepositoryInterface $productRepository
     * @param Json $json
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Session $session,
        SessionFactory $checkoutSession,
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        Json $json,
        ResultFactory $resultFactory,
        Data $helper,
        Cart $cart
    ) {
        $this->context = $context;
        $this->session =  $session;
        $this->checkoutSession = $checkoutSession;
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;        
        $this->json = $json;
        $this->resultFactory = $resultFactory;
        $this->helper = $helper;
        $this->cart = $cart;
    }

    /**
     * Execute Method
     *
     * @return array
     */
    public function execute()
    {        
        $data = $this->context->getRequest()->getParams();
        $sesionData = $this->session;
        if (isset($data)) {
            $shippingId = $data['shippingaddressId'];
            if ($sesionData) {
                $membershipProd = $sesionData->getMembershipSku();
                $membershipProdId = $this->helper->getProductId($membershipProd);
                $matProd = $sesionData->getMatProduct();
                $bladeProd = $sesionData->getBladeProduct();

                $product = $this->productRepository->get($membershipProd);
                $product->addCustomOption('additional_options', $this->json->serialize([
                    'custom_option_1' => [
                        'label' => __("Mat Product Sku"),
                        'value' => $matProd
                    ],
                    'custom_option_2' => [
                        'label' => __("Blade Product Sku"),
                        'value' => $bladeProd
                    ]
                ]));
                $qty = '1';

                $session = $this->checkoutSession->create();
                $quote = $session->getQuote();
                $quote->addProduct($product, $qty);

                $this->cartRepository->save($quote);
                $session->replaceQuote($quote)->unsLastRealOrderId();
                            
                // add custom price in cart                
                $cart = $this->cart; 
                $cartId = $cart->getQuote()->getId();                

                $quote = $this->cartRepository->getActive($cartId);
                $cartitems = $cart->getQuote()->getAllItems();
                $quoteItems = [];                 
                foreach($cartitems as $item)
                {                    
                    if($item->getSku() == $membershipProd)
                    {                    
                        $item->setCustomPrice(0);
                        $item->setOriginalCustomPrice(0);
                        $item->getProduct()->setIsSuperMode(true);
                        $item->save();

                    }

                }
                $quoteItems[] = $cartitems;
                $quote->setItems($cartitems);
                $this->cartRepository->save($quote);
                $quote->collectTotals();
            }
        }
        
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["success" => true]);
        return $resultJson;
    }
}
