<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory;

class ShippingAdress implements HttpPostActionInterface
{

    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $helper
     * @param AddressFactory $addressFactory
     * @param AddressRepositoryInterface $addressRepository
     * @param Session $session
     * @param ManagerInterface $messageManager
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Data $helper,
        AddressFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        Session $session,
        ManagerInterface $messageManager,
        ResultFactory $resultFactory,
        ClubSilhouetteUsersFactory $clubSilhouetteUsersFactory
    ) {
        $this->context = $context;
        $this->helper =  $helper;
        $this->addressFactory = $addressFactory;
        $this->addressRepository = $addressRepository;
        $this->session =  $session;
        $this->messageManager = $messageManager;
        $this->resultFactory = $resultFactory;
        $this->clubSilhouetteUsersFactory = $clubSilhouetteUsersFactory;
    }

    /**
     * Execute Method
     *
     * @return array
     */
    public function execute()
    {
        $data = $this->context->getRequest()->getParams();
        $html = '';$regionId = '';$region = '';

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (isset($data)) {
            $action = $data['action'];
            $firstName = $data['firstname'];
            $lastName = $data['lastname'];
            $street = [$data['street_1']];
            $city = $data['city'];
            if (isset($data['region_id'])) {
                $regionId = $data['region_id'];
            }
            if (isset($data['region'])) {
                $region = $data['region_id'];
            }
            $postcode = $data['postcode'];
            $country = $data['country'];
            $telephone = $data['telephone'];
            if (isset($regionId)) {
                $region = $regionId;
            }
            $customeId = $this->helper->getLoginCustomerId();

            if ($action == 'updateAdd') {
                try {
                    $addressId = $this->session->getCurrShippingId();
                    $address = $this->addressFactory->create()->load($addressId);
                    $address->setCustomerId($customeId)
                            ->setFirstname($firstName)
                            ->setLastname($lastName)
                            ->setCountryId($country)
                            ->setPostcode($postcode)
                            ->setCity($city)
                            ->setTelephone($telephone)
                            ->setStreet($street)
                            ->setRegion($region);
                    $address->save();
                    $lastAddressId = $address->getId();
                    $customerId = $this->helper->getLoginCustomerId();
                    $clubSilhouetteUsersFactory = $this->clubSilhouetteUsersFactory->create();
                    $clubSilhouetteUsersFactory = $clubSilhouetteUsersFactory->addFieldToFilter('userId', $customerId);
                    $clubuserId = $clubSilhouetteUsersFactory->getId();
                    if($clubuserId){
                        try
                        {    
                            $clubSilhouetteUsersFactory->setData('shippingId', $lastAddressId);
                            $clubSilhouetteUsersFactory->save();
                        }catch(\Exception $e)
                        {
                            $this->messageManager->addErrorMessage('Please try again later! There is some issue while saving your shipping address.');
                        }
                    }                    
                    $this->messageManager->addSuccessMessage('Your Shipping address has been updated!');
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage('Please try again later! There is some issue while saving your shipping address.');
                }
                
            } else {
                if ($customeId) {
                    $address = $this->addressFactory->create();

                    $address->setCustomerId($customeId)
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setCountryId($country)
                        ->setPostcode($postcode)
                        ->setCity($city)
                        ->setTelephone($telephone)
                        ->setStreet($street)
                        ->setRegion($region)
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');

                    $address->save();
                    $lastAddressId = $address->getId();
                    
                    $customerAddress = $this->helper->getCustomerAddress();
                    
                    foreach ($customerAddress as $customerAddres) {
                        $addressId = $customerAddres['entity_id'];
                        $name =  $customerAddres['firstname'].' '.$customerAddres['lastname'];
                        $city = $customerAddres['city'];
                        $showAddress = $name.','.$city;

                        if ($addressId == $lastAddressId) {
                            $html .= '<option value="'.$addressId.'" selected>'.$showAddress.'</option>';
                        } else {
                            $html .= '<option value="'.$addressId.'">'.$showAddress.'</option>';
                        }
                    }
                    $html .= '<option value="newaddress">add new</option>';
                }                
                $resultJson->setData(["success" => true, "shippingHtml" => $html]);                

            }
            
        } else {
            $resultJson->setData(["failure" => true]);            
        }
        return $resultJson;
    }
}
