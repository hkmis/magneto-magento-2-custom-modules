<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magneto\SerialNumberLogger\Model\ResourceModel\MachineType\CollectionFactory as MachineTypeFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

class RefillProducts implements HttpGetActionInterface
{

    private const REFILL_PRODUCT_MAT = 'clubsilhouette/general/refillitem_mat';
    
    private const REFILL_PRODUCT_BLADE = 'clubsilhouette/general/refillitem_blade';
    
    /**
     * Constructor
     *
     * @param Context $context
     * @param CollectionFactory $productFactory
     * @param Config $config
     * @param StoreManagerInterface $storemanager
     * @param MachineTypeFactory $machineType
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $productFactory,
        Config $config,
        StoreManagerInterface $storemanager,
        MachineTypeFactory $machineType,
        ResultFactory $resultFactory
    ) {
        $this->productFactory = $productFactory;
        $this->config = $config;
        $this->_storeManager =  $storemanager;
        $this->context = $context;
        $this->machineType = $machineType;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute Method
     *
     * @return mixed
     */
    public function execute()
    {
        $machinesSku = '';
        $matArray = '';
        $bladeArray = '';
        $bladeItems = '';
        $matItems = '';        
        $data = $this->context->getRequest()->getParams();
        
        if ($data) {
            if (isset($data['machinesSku'])) {
                $machinesSku = $data['machinesSku'];
                $machineCollection = $this->machineType->create();
                $machineCollection = $machineCollection->addFieldToFilter(
                    'product_type',
                    ['in' => $machinesSku]
                );
                foreach ($machineCollection as $collection) {
                    $matArray = $matArray.','.$collection->getRefillitemMat();
                    $bladeArray = $bladeArray.','.$collection->getRefillitemBlade();
                }
                $matArray = trim($matArray, ',');
                $bladeArray = trim($bladeArray, ',');

                // Convert into array
                $matArray =explode(',', $matArray);
                $bladeArray =explode(',', $bladeArray);
            }
        }
        
        $refillMat = $this->config->getWebsiteConfig(self::REFILL_PRODUCT_MAT);
        $refillBlade = $this->config->getWebsiteConfig(self::REFILL_PRODUCT_BLADE);

        $refillMatSkus = explode(',', $refillMat);
        $refillBladeSkus = explode(',', $refillBlade);
        $productSkus = array_merge($refillMatSkus, $refillBladeSkus);

        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter('sku', ['in' => $productSkus]);
        
        $store = $this->_storeManager->getStore();
                        
        foreach ($collection as $product) {
            $isActiveMatClass = 0;
            $isActiveBladeClass = 0;
            $productSku = $product->getSku();
            $productImageUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
            . 'catalog/product' .$product->getImage();
            $productName = $product->getName();
            $productDesc = $product->getShortDescription();
            
            // For Mat Products
            if (in_array($productSku, $refillMatSkus)) {

                if ($matArray) {
                    if (in_array($productSku, $matArray)) {   
                        $isActiveMatClass = 1;
                        $matItems .= $this->getMatHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveMatClass);
                    } else {
                        $matItems .= $this->getMatHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveMatClass);                        
                    }
                } else {
                    $matItems .= $this->getMatHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveMatClass);
                }
                
            }
            
            // For Blade Products
            if (in_array($productSku, $refillBladeSkus)) {
                if ($bladeArray) {
                    if (in_array($productSku, $bladeArray)) {
                        $isActiveBladeClass = 1;
                        $bladeItems .= $this->getBladeHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveBladeClass);
                    } else {
                        $bladeItems .= $this->getBladeHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveBladeClass);
                    }
                } else {
                    $bladeItems .= $this->getBladeHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveBladeClass);
                }
            }            
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(["success" => true, 'bladeItems' => $bladeItems, 'matItems' => $matItems]);
        return $resultJson;
    }

    public function getMatHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveClass)
    {
        $matItems = '';
        if($isActiveClass == 1)
        {
            $matItems .= '<div class="upper-item"><div class="refillmat active" style="display: inline-block" 
                        data-sku="'.$productSku.'">';
        }else{
            $matItems .= '<div class="upper-item"><div class="refillmat" style="display: inline-block" 
                        data-sku="'.$productSku.'">';
        }        
        $matItems .= '<img src='.$productImageUrl.' class="refillMatImg" id="refillMatImg" 
                    width="120" height="120"/>';
        $matItems .= '<h3 class="refillMatName" id="refillMatName">'.$productName.'</h3>';
        $matItems .= '<span class="refillMatDesc" id="refillMatDesc">'.$productDesc.'</span>';
        $matItems .= '</div></div>';

        return $matItems;

    }

    public function getBladeHtml($productSku, $productImageUrl, $productName, $productDesc, $isActiveClass)
    {
        $bladeItems = '';
        if($isActiveClass == 1)
        {
            $bladeItems .= '<div class="upper-item"><div class="refillblade active" style="display: inline-block" 
                        data-sku="'.$productSku.'">';
        }else{
            $bladeItems .= '<div class="upper-item"><div class="refillblade" style="display: inline-block" 
                        data-sku="'.$productSku.'">';
        }        
        
        $bladeItems .= '<img src='.$productImageUrl.' class="refillBladeImg" id="refillBladeImg" 
                    width="120" height="120"/>';
        $bladeItems .= '<h3 class="refillBladeName" id="refillBladeName">'.$productName.'</h3>';
        $bladeItems .= '<span class="refillBladeDesc" id="refillBladeName">'.$productDesc.'</span>';
        $bladeItems .= '</div></div>';

        return $bladeItems;

    }
}
