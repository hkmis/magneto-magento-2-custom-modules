<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\HttpPostActionInterface;

class SetSession implements HttpPostActionInterface
{

    /**
     * Constructor
     *
     * @param Context $context
     * @param Session $session
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Session $session,
        ResultFactory $resultFactory
    ) {
        $this->context = $context;
        $this->session =  $session;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute Method
     *
     * @return array
     */
    public function execute()
    {
        $data = $this->context->getRequest()->getParams();
        
        if (isset($data)) {
            if (isset($data['membershipSku'])) {
                $membershipSku = $data['membershipSku'];
                $this->session->setMembershipSku($membershipSku);
            }
            if (isset($data['refillitem1'])) {
                $refillitem1 = $data['refillitem1'];
                $this->session->setMatProduct($refillitem1);
            }
            if (isset($data['refillitem2'])) {
                $refillitem2 = $data['refillitem2'];
                $this->session->setBladeProduct($refillitem2);
            }
            if (isset($data['shippingaddressId'])) {
                $shippingaddressId = $data['shippingaddressId'];
                $this->session->setShippingAddressId($shippingaddressId);
            }
            if (isset($data['currShippingId'])) {
                $currShippingId = $data['currShippingId'];
                $this->session->setCurrShippingId($currShippingId);
            }
            $sesionData = $this->session->getData();
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(["success" => true, 'sessionData' => $sesionData]);
            return $resultJson;
        } else {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(["failure" => true]);
            return $resultJson;
        }
    }
}
