<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as ClubuserFactory;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class ClubsilhouetteTab implements HttpPostActionInterface
{

    /**
     * Undocumented function
     *
     * @param Context $context
     * @param Session $session
     * @param ClubuserFactory $clubUserFactory
     * @param Data $helper
     * @param ManagerInterface $messageManager
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Session $session,
        ClubuserFactory $clubUserFactory,
        Data $helper,
        ManagerInterface $messageManager,
        ResultFactory $resultFactory
    ) {
        $this->context = $context;
        $this->session =  $session;
        $this->clubUserFactory = $clubUserFactory;
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute Method
     *
     * @return array
     */
    public function execute()
    {
        $data = $this->context->getRequest()->getParams();
        if (isset($data)) {
            $action = $data['action'];
            $customerId = $this->helper->getLoginCustomerId();
            if ($action == 'updateRefill') {
                $matProduct = $data['matProduct'];
                $bladeProduct = $data['bladeProduct'];
                $membershipProduct = $data['membershipProduct'];
                if ($membershipProduct == 'Monthly') {
                    $membershipProduct = 1;
                } else {
                    $membershipProduct = 12;
                }
                if ($customerId) {
                    $clubUserFactory = $this->clubUserFactory->create();
                    $clubUserFactory->load($customerId, 'userId');
                    $clubUserFactory->setData('paymentCycleMonths', $membershipProduct);
                    $clubUserFactory->setData('refill1ItemNo', $matProduct);
                    $clubUserFactory->setData('refill2ItemNo', $bladeProduct);
                    $clubUserFactory->save();
                    
                    $this->messageManager->addSuccessMessage('Club Silhouette Record has been updated!');
                } else {
                    $this->messageManager->addSuccessMessage('There is some problem while saving your details.Please try again later.');
                }
            } elseif ($action == 'cancelMembership') {
                if ($customerId) {
                    $clubUserFactory = $this->clubUserFactory->create();
                    $clubUserFactory->load($customerId, 'userId');
                    $clubUserFactory->setData('membershipStatus', 'X');
                    $clubUserFactory->save();
                    
                    $this->messageManager->addSuccessMessage('Club Silhouette Membership has been cancel');
                } else {
                    $this->messageManager->addSuccessMessage('There is some problem while saving your details.Please try again later.');
                }
            }
        } else {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(["failure" => true]);
            return $resultJson;
        }
    }
}
