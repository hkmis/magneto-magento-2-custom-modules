<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magneto\ClubSilhouetteManager\Helper\OrderCreate;
use Magento\Inventory\Model\ResourceModel\Source\CollectionFactory as InvenotryCollection;
use Magneto\ClubSilhouetteManager\Helper\Config;
use DateTime;
use DateInterval;

class Test implements HttpGetActionInterface 
{
    private const CRON_FREQUENCY_HOURS = 24;
    /**
     * @var object
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        PageFactory $resultPageFactory,
        RedirectFactory $resultRedirectFactory,
        Data $helper,
        OrderCreate $orderCreateHelper,
        InvenotryCollection $invenotryCollection,
        Config $config
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->helper = $helper;
        $this->orderCreateHelper = $orderCreateHelper;
        $this->invenotryCollection = $invenotryCollection;
        $this->config = $config;
    }
    /**
     * Club Silhouette Users
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {         
        $end_date = date('Y-m-d H:i:s', time() + (60 * 60 * self::CRON_FREQUENCY_HOURS));
        echo $end_date;
        $enter_loop = true;
        $end_date_plus_month = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', strtotime($end_date)) . ' +1 month'));
        $mod = 1;
        $pending_payments_arr = $this->helper->pendingPaymentsArr($end_date);
        echo '<pre>';
        print_r($pending_payments_arr->getData());die;
        $remainder = 0;
        
        foreach ($pending_payments_arr as $pmt){
            $response = $this->helper->CSProcessRenewal($pmt);
            echo '<pre>';
            print_r($response);
            die;
        }
        die('Nothing Happen!');
        

        // send reminder to review address to all users who will get a shipment in a week
        $review_start_date = new DateTime('today');
        $review_start_date->add(DateInterval::createFromDateString('7 days'));
        $review_end_date = new DateTime($review_start_date->format('Y-m-d 23:59:59'));

        $dateLog = date('Y-m-d H:i:s') . " - Sending reminders to review shipping address for " . $review_start_date->format('Y-m-d H:i:s') . " to " . 
        $review_end_date->format('Y-m-d H:i:s');

        $num_reminders_sent = $this->helper->sendReviewReminderEmails($review_start_date->format('Y-m-d H:i:s'), $review_end_date->format('Y-m-d H:i:s'), 
        'https://www.silhouetteamerica.com/club-silhouette');        

        //get all users who need to have refills created, and the stock status of warehouse items
        $refills_arr = $this->helper->dueShipments();        
        $refills_arr_count = $refills_arr->getData(); 

        //check if there are any refills to send
        if (count($refills_arr_count) > 0):
            foreach ($refills_arr as $row):                  
                    $rowData = $row->getData();
                    $userId = $rowData['userId'];
                    $shippingId = $rowData['shippingId'];
                    $refill1ItemNo = $rowData['refill1ItemNo'];
                    $refill2ItemNo = $rowData['refill2ItemNo'];
                    $refill1ItemNoQty = $this->helper->getProductSalableQty($refill1ItemNo);
                    $refill2ItemNoQty = $this->helper->getProductSalableQty($refill2ItemNo);
                    $refill1ItemNoId = $this->helper->getProductId($refill1ItemNo);
                    $refill2ItemNoId = $this->helper->getProductId($refill2ItemNo);

                    $refill1ItemName = $this->helper->getProductName($refill1ItemNo);
                    $refill2ItemName = $this->helper->getProductName($refill2ItemNo);
                    $productname = array('refill1ItemName' => $refill1ItemName, 'refill2ItemName' => $refill2ItemName);
                    // Order Data
                    $items = array(array('product_id' => $refill1ItemNoId, 'qty' => 1), array('product_id' => $refill2ItemNoId, 'qty' => 1));
                    $shippingAddress = $this->helper->getShippingInfo($shippingId);
                    $shipStreet = '';
                    $currentStreetData = $shippingAddress->getStreet();
                    if ($currentStreetData) {
                        $shipStreet = $currentStreetData[0];
                    }
                    $siteId = 1;$websiteId= 1;$sourceCode = '';
                    $regionId = $shippingAddress->getRegion()->getRegionId();
                    
                    $sourceCode = $this->getSourceCode($regionId);
                    $tempOrder = [
                        'shipping_address' =>[
                            'firstname' => $shippingAddress->getFirstname(),
                            'lastname'=> $shippingAddress->getLastname(),
                            'street' => $shipStreet,
                            'city' => $shippingAddress->getCity(),
                            'country_id' => $shippingAddress->getCountryId(),
                            'region' => $shippingAddress->getRegion()->getRegion(),
                            'postcode' => $shippingAddress->getPostcode(),
                            'telephone' => $shippingAddress->getTelephone(),
              
                        ],
                        'customer_id'  => $userId,
                        'email' => $this->helper->getCustomerEmail($userId),
                        'currency_id'  => 'USD',
                        'items'=> $items,
                        'siteId' => $sourceCode,
                        'websiteId' => $websiteId
                    ];
                    //Check if refill prdoucts has salable qty or not                    
                    if($refill1ItemNoQty > 1 && $refill2ItemNoQty > 1)
                    {                                                   
                        $orderData = $this->orderCreateHelper->createMageOrder($tempOrder);
                        if ($orderData['success'] && $orderData['order_id']!='') {
                            $row->setData('orderCreationDate', date('Y-m-d H:i:s'));
                            $row->setData('orderId', $orderData['order_id']);
                            $row->save();

                            echo 'Saved Data';die;
                        }

                    }else{
                    // if there is an inventory shortage, let the user know we were missing an item or items                    
                    $templateId = $this->config->getClubSilhouetteRefillShipOSEmailTemplate();
                    $refill1ItemName = $this->helper->getProductName($refill1ItemNo);
                    $refill2ItemName = $this->helper->getProductName($refill2ItemNo);
                    $url = 'https://silhouetteamerica.magneto.co.in/sales/order/history';
                    $productname = array('refill1ItemName' => $refill1ItemName, 
                    'refill2ItemName' => $refill2ItemName, 'url' => $url);                    
                    $this->helper->sendMail($this->helper->getCustomerEmail($userId), $templateId, $productname);
                    echo 'sent email';die;
                    }

                    
            endforeach;
        endif;        

        echo 'Finished';die;
    }

    public function getSourceCode($regionId) : ?string
    {
        $sourceCode = 'default';
        $invenotryCollection = $this->invenotryCollection->create();
        $invenotryCollection->addFieldToFilter('region_id', $regionId);                                        
        foreach($invenotryCollection as $inventory)
        {
            $sourceCode = $inventory->getSourceCode();                        
        }

        return $sourceCode;
    }
}
