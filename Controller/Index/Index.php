<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;

class Index implements HttpGetActionInterface 
{
    /**
     * @var object
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        PageFactory $resultPageFactory,
        RedirectFactory $resultRedirectFactory,
        Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->helper = $helper;
    }
    /**
     * Club Silhouette Users
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $customerIsLogin = $this->helper->getLoginCustomerId();
        if (!empty($customerIsLogin)) {
            $isMember = $this->helper->is_member($customerIsLogin);
            if($isMember)
            {
                $resultRedirect->setPath('clubsilhouette/customer/index');
            }else{
                $resultPage = $this->resultPageFactory->create();
                return $resultPage;
            }
            
        } else {            
            $resultRedirect->setPath('customer/account');            
        }

        return $resultRedirect;
    }
}
