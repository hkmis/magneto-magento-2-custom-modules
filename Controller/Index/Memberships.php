<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\ActionInterface;

class Memberships implements ActionInterface
{

    private const ATTRIBUTE_CODE = 'membership_product';
    
    /**
     * Constructor
     *
     * @param Context $context
     * @param CollectionFactory $productFactory
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $productFactory,
        ResultFactory $resultFactory
    ) {
        $this->context = $context;
        $this->productFactory = $productFactory;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute Method
     *
     * @return mixed
     */
    public function execute()
    {
        $membershipCall = '';
        $data = $this->context->getRequest()->getParams();
        if (isset($data)) {
            if (isset($data['membershipCall'])) {
                $membershipCall = $data['membershipCall'];
            }
        }

        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter(self::ATTRIBUTE_CODE, '1');

        $html = "";
        $membershipHtml = "";

        foreach ($collection as $product) {
            $productId = $product->getId();
            $productName = $product->getName();
            $productSku = $product->getSku();
            $productPrice = $product->getFinalPrice();
            $productDesc = $product->getShortDescription();
            if ($membershipCall == 'checkoutpage') {
                $html .= '<div class="plan-container"><div class="inner-planwrap">';
                if ($productSku == 'Monthly') {
                    $html .= ' <span class="membership-plan-title active" 
                    data-sku="'.$productSku.'">'.$productName .'</span>';
                } else {
                    $html .= ' <span class="membership-plan-title" 
                    data-sku="'.$productSku.'">'.$productName .'</span>';
                }
                
                $html .= '</div></div>';
    
                if ($productSku == 'Monthly') {
                    $membershipHtml .= '<div class="membership-plan '.$productSku.'" 
                    id="membership-plan '.$productSku.'">';
                } else {
                    $membershipHtml .= '<div class="membership-plan '.$productSku.'" 
                    id="membership-plan '.$productSku.'" style="display:none">';
                }
                
                $membershipHtml .= '<span>Get first month free, then</span> $'.$productPrice.'/'.$productName;
                $membershipHtml .= '</div>';
            } else {
                $html .= '<div class="plan-container"><div class="inner-planwrap">';
                $html .= ' <h2 class="membership-plan-title">'.$productName .'</h2>';
                $html .= ' <p class="membership-plan-text">'.$productDesc .'</p>';
                $html .= ' <p class="membership-plan-price">$'.$productPrice .'</p>';
                $html .= ' <button class="membership-plan-select" data-title="Select" id="plan-select '.$productId.'" 
                data-id="'.$productId.'" data-sku="'.$productSku.'"><span>Select</span></p>';
                $html .= '</div></div>';
            }
        }
                
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($membershipCall) {
            $resultJson->setData(["success" => true, 'membershipPlan' => $html, 'membershipPrice' => $membershipHtml]);
        } else {
            $resultJson->setData(["success" => true, 'response' => $html]);
        }
        
        return $resultJson;
    }
}
