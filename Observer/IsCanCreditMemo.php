<?php

namespace Magneto\ClubSilhouetteManager\Observer;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magento\Sales\Model\Order;

class IsCanCreditMemo implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magneto\ClubSilhouetteManager\Helper\Config
     */
    private $configProvider;

    public function __construct(\Magneto\ClubSilhouetteManager\Helper\Config $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/IsCanCreditMemo.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        if ($this->configProvider->getModuleStatus()) {
            $logger->info('Enabled');
            $order = $observer->getData('order');
            
            if ($order->canUnhold()) {
                $logger->info('1');
                return;
            }

            if ($order->isCanceled() || $order->getState() === Order::STATE_CLOSED) {
                $logger->info('2');
                return;
            }

            if (($order->getClubsilhouettecreditInvoicedAmount() - $order->getClubsilhouettecreditRefundedAmount()) > 0) {
                $logger->info('3');
                $order->setForcedCanCreditmemo(true);
            } elseif ($order->getClubsilhouettecreditInvoicedAmount()) {
                $logger->info('4');
                $hide = true;
                foreach ($order->getItems() as $item) {
                    $qty = (double)$item->getQtyOrdered() - (double)$item->getQtyRefunded();
                    if ($qty > 0.0001) {
                        $hide = false;
                        $order->setForcedCanCreditmemo(true);
                        break;
                    }
                }

                $logger->info('5');
                if ($hide) {
                    $logger->info('6');
                    $order->setForcedCanCreditmemo(false);
                }
            }

            $logger->info('7');
            if ($order->getClubsilhouettecreditRefundedAmount() !== null
                && $order->getBaseTotalInvoiced() - $order->getClubsilhouettecreditRefundedAmount() <= 0
                && $order->getData(SalesFieldInterface::CS_AMOUNT) === null) {
                $order->setForcedCanCreditmemo(false);
            }
        }
    }
}
