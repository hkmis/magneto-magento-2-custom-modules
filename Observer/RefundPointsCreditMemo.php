<?php
namespace Magneto\ClubSilhouetteManager\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;

class RefundPointsCreditMemo implements ObserverInterface
{    

    /**
     * Constructor
     *
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }
    /**
     * Credit Memo Observer Action
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/creditMemoClubsil.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $enable = $this->config->getModuleStatus();
        $logger->info(print_r($enable, true));
        if ($enable == 1) {
            $creditMemo = $observer->getEvent()->getCreditmemo();
            $order = $creditMemo->getOrder();
            $logger->info(print_r(get_class_methods($order), true));
            
        }
    }
}
