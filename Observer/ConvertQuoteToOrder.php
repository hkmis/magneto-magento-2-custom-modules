<?php
namespace Magneto\ClubSilhouetteManager\Observer;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;

class ConvertQuoteToOrder implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {        
        $order = $observer->getData('order');
        /** @var \Magento\Quote\Model\Quote $quote */

        $quote = $observer->getData('quote');
        if ($quote->getData(SalesFieldInterface::CS_USE)) {
            $order->setClubsilhouettecreditBaseAmount($quote->getClubsilhouettecreditBaseAmount());
            $order->setClubsilhouettecreditAmount($quote->getClubsilhouettecreditAmount());
            $order->setClubsilhouettecreditUsedPoints($quote->getClubsilhouettecreditUsedPoints());
            $order->setData(
                SalesFieldInterface::CS_SHIPPING_AMOUNT,
                $quote->getData(SalesFieldInterface::CS_SHIPPING_AMOUNT)
            );
        }
    }
}
