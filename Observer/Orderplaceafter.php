<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Session;

class Orderplaceafter implements ObserverInterface
{
    protected const PAYMENT_CYCLE_MONTHLY_MONTHS = 1;
    
    protected const PAYMENT_CYCLE_ANNUAL_MONTHS = 12;

    private const POINTS_SUBTOTAL_MULTIPLIER = .1;

    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        Data $helper,
        ClubSilhouetteUsersFactory $clubSilhouetteUsersFactory,
        ProductRepositoryInterface $productRepository,
        Session $session,
        Config $config
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->clubSilhouetteUsersFactory = $clubSilhouetteUsersFactory;
        $this->productRepository = $productRepository;
        $this->session =  $session;
        $this->config = $config;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {        
        $matSku = '';
        $bladeSku = '';
        $membershipProduct= '';        
        $customerId = $this->session->getCustomer()->getId();        
        $isEnable = $this->config->getModuleStatus();
        $isExist = 0;
        
        if ($isEnable == 1) {
            $clubSilhouetteUsersFactory = $this->clubSilhouetteUsersFactory->create();
            $clubSilhouetteUsersFactory->load($customerId, 'userId');
            $clubuserId = $clubSilhouetteUsersFactory->getId();
            if(empty($clubuserId))
            {
                try {                    
                    
                    $order = $observer->getEvent()->getOrder();
                    $items = $order->getAllItems();
                    // Get membership product sku's
                    $membershipProds = $this->helper->getMembershipProducts();
                    $membershipProdSku = [];
                    foreach ($membershipProds as $membershipProd) {
                        $membershipProdSku[] = $membershipProd['sku'];
                    }

                    foreach ($items as $item) {
                        $productSku = $item->getSku();
                        if (in_array($productSku, $membershipProdSku)) {
                            $isExist = 1;
                            $membershipProduct = $productSku;
                            $itemData = $item->getData();
                            if (isset($itemData['product_options']['additional_options'])) {
                                $additionalData = $itemData['product_options']['additional_options'];
                                if ($additionalData) {
                                    $matData = $additionalData->custom_option_1;
                                    $bladeData = $additionalData->custom_option_2;

                                    if ($matData) {
                                        $matSku = $matData->value;
                                    }

                                    if ($bladeData) {
                                        $bladeSku = $bladeData->value;
                                    }                                    

                                }
                            }
                            
                        }
                    }                    
                    if($isExist == 1){
                        $data = [];
                        if($membershipProduct == 'Monthly'){
                            $membershipProduct = self::PAYMENT_CYCLE_MONTHLY_MONTHS;
                        }else{
                            $membershipProduct = self::PAYMENT_CYCLE_ANNUAL_MONTHS;
                        }

                        $data['payment_cycle_months'] = $membershipProduct;
                        $data['userId'] = $customerId;
                        $data['card_id'] = '123456';
                        $data['shipping_id'] = $this->session->getShippingAddressId();
                        $data['refill1_item_no'] = $matSku;
                        $data['refill2_item_no'] = $bladeSku;                        

                        // Save data in the table
                        $response = $this->helper->saveClubSilhouetteUser($customerId, true, $data, false);                        
                        if(!empty($response))
                        {
                            $this->logger->info('Response come');

                            $mssg = $response['msg'];
                            $this->logger->info(print_r($mssg, true));
                            if(!$response['success'])
                            {
                                $this->logger->info('user has not been saved! Observer call.');
                            }
                        }
                    }
                    
                } catch (\Exception $e) {
                    $this->logger->info($e->getMessage());
                }
            }else
            {
                // check membership status                
                $membershipStatus = $clubSilhouetteUsersFactory->getData('membershipStatus');
                $membersPoints = $clubSilhouetteUsersFactory->getData('points');                
                $total = 0.00;

                if($membershipStatus == 'A')
                {
                    try
                    {
                        $order = $observer->getEvent()->getOrder();
                        $orderId = $order->getId();
                        $discount = $order->getDiscountAmount();                        
                        
                        $discount = abs((float) $discount);                        
    
                        foreach($order->getAllItems() as $item)
                        {                                                
                            $total += ($item->getPrice() * $item->getQtyOrdered());
                        }                        

                        //subtract the discount
                        $total -= $discount;                                   
    
                        //calculate the points, now that we have a total to work with
                        $points = (int) ($total * self::POINTS_SUBTOTAL_MULTIPLIER);                        
    
                        //make sure we don't "award" negative points
                        if ($points < 0):
                            $points = 0;
                        endif;
                        if($points > 0)
                        {
                            $points = (int)$membersPoints + (int)$points;
                            $clubSilhouetteUsersFactory->setData('points', $points);
                            $clubSilhouetteUsersFactory->save();   
                            $notes = $points. 'points received for a store purchase (Order #'. $orderId.')';    
                            $ipAddress = $this->helper->getCustomerIp();
                            
                            if($this->helper->saveClubUsersActions($customerId, $this->helper::ACTION_RECEIVE_POINTS, $notes,
                            $ipAddress, $points, $orderId))
                            {
                                $templateId = $this->config->getCSPointsEmailTemplate();
                                $email = $this->helper->getCustomerEmail($customerId);
                                $emailVars = [
                                    'points_to_add' => $points
                                ];
                                $this->helper->sendMail($email, $templateId, $emailVars);
                            }

                        }                              
                        
                    }catch(\Exception $e)
                    {
                        $this->logger->info('There is some error while saving club points.'. $e->getMessage());                
                    }                    

                }

            }
        }
    }
}
