<?php

namespace Magneto\ClubSilhouetteManager\Observer;

use Magneto\ClubSilhouetteManager\Helper\Data;
use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;

class DeductClubPoints implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magneto\ClubSilhouetteManager\Helper\Data
     */
    private $helper;

    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {        
        $order = $observer->getData('order');

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getData('quote');
        if ($quote->getData(SalesFieldInterface::CS_USE)) {
            $notes = $quote->getClubsilhouettecreditUsedPoints().' points received for a 
            store purchase (Order #'.$order->getIncrementId().')';            

            $this->helper->addOrSubtractClubPoints(
                $quote->getCustomerId(),
                $this->helper::ACTION_RECEIVE_POINTS,
                $notes,
                '',
                -$quote->getClubsilhouettecreditUsedPoints(),                
                $order->getIncrementId()
            );
        }
    }
}
