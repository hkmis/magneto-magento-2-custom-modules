<?php
namespace Magneto\ClubSilhouetteManager\Observer;

class Clubuserconditionobserver implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $additional = $observer->getAdditional();
        $conditions = (array) $additional->getConditions();

        $conditions = array_merge_recursive($conditions, [
            $this->getCustomerShippingAreaCondition()
        ]);

        $additional->setConditions($conditions);
        return $this;
    }

    private function getCustomerShippingAreaCondition()
    {
        return [
            'label'=> __('Club user'),
            'value'=> \Magneto\ClubSilhouetteManager\Model\Rule\Condition\CsuserCondition::class
        ];
    }
}
