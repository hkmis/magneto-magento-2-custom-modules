<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Api\Data;

/**
 * BundelSearchResultsInterface Class
 * @package Magneto/ClubSilhouetteManager/Api/Data
 */
interface BundelSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Bundel list.
     * @return BundelInterface[]
     */
    public function getItems(): array;

    /**
     * Set bundle list.
     * @param BundelInterface[] $items
     * @return BundelSearchResultsInterface
     */
    public function setItems(array $items): BundelSearchResultsInterface;
}

