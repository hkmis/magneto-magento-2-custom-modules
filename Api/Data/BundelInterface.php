<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Api\Data;


/**
 * BundelInterface cass
 * @package Magneto\ClubSilhouetteManager\Api\Data
 */
interface BundelInterface
{

    public const DATE_AVAILABLE = 'dateAvailable';
    public const BUNDLE_ID = 'bundleId';
    public const BUNDLE_NAME = 'bundleName';
    public const ID = 'id';
    public const SENT = 'sent';

    /**
     * Get id
     * @return string|null
     */
    public function getId():string|null;

    /**
     * Set id
     * @param string $id
     * @return BundelInterface
     */
    public function setId(string $id):BundelInterface;

    /**
     * Get bundleId
     * @return string|null
     */
    public function getBundelId():string|null;

    /**
     * Set bundleId
     * @param string $bundelId
     * @return BundelInterface
     */
    public function setBundelId(string $bundelId):BundelInterface;

    /**
     * Get dateAvailable
     * @return string|null
     */
    public function getDateAvailable(): string|null;

    /**
     * Set dateAvailable
     * @param string $dateAvailable
     * @return BundelInterface
     */
    public function setDateAvailable(string $dateAvailable): BundelInterface;

    /**
     * Get bundleName
     * @return string|null
     */
    public function getBundleName(): string|null;

    /**
     * Set bundleName
     * @param string $bundleName
     * @return BundelInterface
     */
    public function setBundleName(string $bundleName): BundelInterface;

    /**
     * Get Sent
     * @return string|null
     */
    public function getSent(): string|null;

    /**
     * Set Sent
     * @param string $sent
     * @return BundelInterface
     */
    public function setSent(string $sent): BundelInterface;
    
}

