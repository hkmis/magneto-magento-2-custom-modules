<?php

namespace Magneto\ClubSilhouetteManager\Api\Data;

interface SalesFieldInterface
{
    public const CS_USE = 'clubsilhouettecredit_use';
    public const CS_USED_POINTS = 'clubsilhouettecredit_used_points';
    public const CS_BASE_AMOUNT = 'clubsilhouettecredit_base_amount';
    public const CS_AMOUNT = 'clubsilhouettecredit_amount';
    public const CS_SHIPPING_AMOUNT = 'clubsilhouettecredit_shipping_amount';
    public const CS_SHIPPING_AMOUNT_INVOICED = 'clubsilhouettecredit_shipping_amount_invoiced';
    public const CS_SHIPPING_AMOUNT_REFUNDED = 'clubsilhouettecredit_shipping_amount_refunded';
    public const CS_INVOICED_BASE_AMOUNT = 'clubsilhouettecredit_invoiced_base_amount';
    public const CS_INVOICED_AMOUNT = 'clubsilhouettecredit_invoiced_amount';
    public const CS_REFUNDED_BASE_AMOUNT = 'clubsilhouettecredit_refunded_base_amount';
    public const CS_REFUNDED_AMOUNT = 'clubsilhouettecredit_refunded_amount';
}
