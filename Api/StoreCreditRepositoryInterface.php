<?php

namespace Magneto\ClubSilhouetteManager\Api;

interface StoreCreditRepositoryInterface
{
    /**
     * @param int $customerId
     *
     * @return \Magneto\ClubSilhouetteManager\Api\Data\StoreCreditInterface
     */
    public function getByCustomerId($customerId);
}
