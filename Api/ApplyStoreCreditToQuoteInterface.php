<?php
namespace Magneto\ClubSilhouetteManager\Api;

interface ApplyStoreCreditToQuoteInterface
{
    /**
     * @param int $cartId
     * @param float $amount
     * @param float $usedPoints
     * @return float
     */
    public function apply($cartId, $amount, $usedPoints);

    /**
     * @param int $cartId
     * @return float
     */
    public function cancel($cartId);
}
