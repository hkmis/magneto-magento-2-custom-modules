<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magneto\ClubSilhouetteManager\Api\Data\BundelInterface;
use Magneto\ClubSilhouetteManager\Api\Data\BundelSearchResultsInterface;

/**
 * BundelRepositoryInterface Class
 */
interface BundelRepositoryInterface
{

    /**
     * Save Bundel
     * @param BundelInterface $bundel
     * @return BundelInterface
     * @throws LocalizedException
     */
    public function save(
        BundelInterface $bundel
    ): BundelInterface;

    /**
     * Retrieve Bundel
     * @param int $id
     * @return BundelInterface
     * @throws LocalizedException
     */
    public function get(int $id):BundelInterface;

    /**
     * Retrieve Bundel matching the specified criteria.
     * @param SearchCriteriaInterface $searchCriteria
     * @return BundelSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    ):BundelSearchResultsInterface;

    /**
     * Delete Bundel
     * @param BundelInterface $bundel
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        BundelInterface $bundel
    ):bool;

    /**
     * Delete Bundel by ID
     * @param int $id
     *
     * @return bool true on success
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById(int $id):bool;
}
