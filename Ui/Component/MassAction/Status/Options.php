<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Ui\Component\MassAction\Status;

use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;

class Options implements \JsonSerializable
{
    /**
     * options variable
     *
     * @var array $options
     */
    protected $options;

    /**
     * data variable
     *
     * @var array $data
     */
    protected $data;

    /**
     * For UrlBuilder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * urlPath variable
     *
     * @var $urlPath
     */
    protected $urlPath;

    /**
     * paramName variable
     *
     * @var $paramName
     */
    protected $paramName;

    /**
     * additionalData variable
     *
     * @var array $additionalData
     */
    protected $additionalData = [];

    /**
     * Constructor Action
     *
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->data = $data;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Action jsonSerializes
     *
     * @param jsonSerialize
     */
    public function jsonSerialize() : mixed
    {
        if ($this->options === null) {
            $options = [[
                'value' => '0',
                'label' => 'Enable '
            ], [
                'value' => '1',
                'label' => 'Disable'
            ]];
            $this->prepareData();
            foreach ($options as $optionCode) {
                $this->options[$optionCode['value']] = [
                    'type' => 'change_status_' . $optionCode['value'],
                    'label' => __($optionCode['label']),
                    '__disableTmpl' => true
                ];

                if ($this->urlPath && $this->paramName) {
                    $this->options[$optionCode['value']]['url'] = $this->urlBuilder->getUrl(
                        $this->urlPath,
                        [$this->paramName => $optionCode['value']]
                    );
                }

                $this->options[$optionCode['value']] = array_merge_recursive(
                    $this->options[$optionCode['value']],
                    $this->additionalData
                );
            }

            $this->options = array_values($this->options);
        }

        return $this->options;
    }

    /**
     * Action prepareData
     *
     * @param PrepareData
     */
    protected function prepareData()
    {
        foreach ($this->data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->urlPath = $value;
                    break;
                case 'paramName':
                    $this->paramName = $value;
                    break;
                case 'confirm':
                    foreach ($value as $messageName => $message) {
                        $this->additionalData[$key][$messageName] = (string)new Phrase($message);
                    }
                    break;
                default:
                    $this->additionalData[$key] = $value;
                    break;
            }
        }
    }
}
