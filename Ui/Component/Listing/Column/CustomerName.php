<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Ui\Component\Listing\Column;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as Model;

class CustomerName extends Column
{
    /**
     * @var object
     */
    protected $customerRepository;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CustomerRepositoryInterface $customerRepository,
        Model $model,
        array $components = [],
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->clubUsersModel = $model;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * DataSource
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $customerId = $item['userId'];
                $id = $item['id'];
                try {
                    $customer = $this->customerRepository->getById($customerId);
                    $customerName = $customer->getFirstname();
                    $item[$this->getData('name')] = $customerName;
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $model = $this->clubUsersModel->create();
                    $model->load($id);
                    $model->delete();
                }
            }
        }

        return $dataSource;
    }
}
