<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Date extends Column
{

    /**
     * For TimezoneInterface
     *
     * @var \TimezoneInterface
     */
    protected $_timezoneInterface;

    /**
     * @param ContextInterface $contextInterface
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
     * @param UiComponentFactory $componentFactory
     * @param array $components
     * @param array $data
     */

    public function __construct(
        ContextInterface $contextInterface,
        TimezoneInterface $timezoneInterface,
        UiComponentFactory $componentFactory,
        array $components = [],
        array $data = []
    ) {
         $this->_timezoneInterface = $timezoneInterface;
        parent::__construct($contextInterface, $componentFactory, $components, $data);
    }

    /**
     * Date Action Datasource
     *
     * @param array $dataSource
     */

    public function prepareDataSource(array $dataSource): array
    {
        $dateFormat = "d-M-Y";
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $date = '';
                if ($item[$this->getData('name')] != '' && $item[$this->getData('name')] != '0000-00-00 00:00:00') {
                    $date = $this->_timezoneInterface
                                        ->date(new \DateTime($item[$this->getData('name')]))
                                        ->format($dateFormat);
                }
                $item[$this->getData('name')] = $item[$this->getData('name')];
            }
        }
        return $dataSource;
    }
}
