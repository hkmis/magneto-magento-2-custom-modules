<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Store for core config data path. Keep the configuration path in one place for settings.
 */
class Config extends AbstractHelper
{
    public const MODULE_STATUS = 'clubsilhouette/general/enable';
    public const BUNDLEDOWNKEY = 'bundledownloader';
    public const LABELKEY = 'label';
    public const BUNDLEDOWNLOADERSETLABEL ='Promo Bundles';
    public const SETKEY = 'set';
    public const BUNDLEDOWNLOADERSET  = "downloader/general/selectedset";
    public const PRODUCTTYPE = 'virtual';
    public const NEWPRODUCTPATH = 'catalog/product/new';
    public const DEFAULTSETID = 23;

    public const FROM_EMAIL = 'trans_email/ident_general/email';
    public const FROM_NAME = 'trans_email/ident_general/name';

    public const EXCBUNDLE_TEMPLATE = 'clubsilhouette/cs_excbundle/cs_excbundle_email_succ_template';
    public const EXCBUNDLE_EMAIL = 'clubsilhouette/cs_excbundle/cs_excbundle_email';
    public const CSUSER_EMAIL_TEMPLATE = 'clubsilhouette/general/cs_user_email_template';
    public const CSUSER_REFILLSHIP_EMAIL_TEMPLATE = 'clubsilhouette/cs_refillship/cs_refillship_email_template';
    public const CSUSER_REFILLSHIP_OS_EMAIL_TEMPLATE = 'clubsilhouette/cs_refillship/cs_refillship_outofstock_email_template';
    public const CS_REPORT_EMAILS = 'clubsilhouette/general/cs_report_email';
    public const CS_REPORT_EMAILS_TEMPLATE = 'clubsilhouette/general/cs_report_email_template';
    public const CS_PAYMENT_REMAINDER_TEMPLATE = 'clubsilhouette/general/cs_payment_rem_template';
    public const CS_RENEWAL_MC_TEMPLATE = 'clubsilhouette/cs_renewal/cs_renewal_email_mc_template';
    public const CS_POINTS_TEMPLATE = 'clubsilhouette/general/cs_points';

   
    /**
     * Config constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Fetch website configuration from db.
     *
     * @param string $path
     * @param int $website
     * @param string $scope
     * @return int|string|boolean|float|null
     */
    public function getWebsiteConfig($path, $website = 0, $scope = ScopeInterface::SCOPE_WEBSITE): int|string|bool|float|null
    {
        return $this->scopeConfig->getValue(
            $path,
            $scope,
            $website
        );
    }

    /**
     * Get Module Status
     *
     * @return bool|string|null
     */
    public function getModuleStatus(): string|bool|null
    {
        return $this->getWebsiteConfig(self::MODULE_STATUS);
    }

    /**
     * GetSystemConf
     *
     * @param string $config
     * @return string
     */
    public function getSystemConf($config): ?string
    {
        return $this->scopeConfig->getValue(
            $config,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get General Email
     *
     * @return string
     */
    public function getFromEmail(): ?string
    {
        $email = $this->getWebsiteConfig(self::FROM_EMAIL);
        return $email;
    }

    /**
     * Get General Name
     *
     * @return string
     */
    public function getFromName(): ?string
    {
        $name = $this->getWebsiteConfig(self::FROM_NAME);
        return $name;
    }

    /**
     * Get Exclusive Bundle Email Template
     *
     * @return string|null
     */
    public function getExclusiveBundleEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::EXCBUNDLE_TEMPLATE);
        
        if(empty($templateId))
        {
            $templateId = 'cs_exc_bundle_succ';
        }
        
       return $templateId;
    }

    /**
     * Get Exclusive Bundle Email
     *
     * @return string|null
     */
    public function getExclusiveBundleEmail(): ?string
    {
       return $this->getWebsiteConfig(self::EXCBUNDLE_EMAIL);
    }


    /**
     * Get ClubSilhouette Email Template
     *
     * @return string|null
     */
    public function getClubSilhouetteEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CSUSER_EMAIL_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_free_trial';
        }
        
        return $templateId;
    }

    /**
     * Get ClubSilhouette Refill Shipment Email Template
     *
     * @return string|null
     */
    public function getClubSilhouetteRefillShipEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CSUSER_REFILLSHIP_EMAIL_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_user_refillship';
        }
        
        return $templateId;
    }

    /**
     * Get ClubSilhouette Refill Shipment Out Of Stock Product Email Template
     *
     * @return string|null
     */
    public function getClubSilhouetteRefillShipOSEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CSUSER_REFILLSHIP_OS_EMAIL_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_user_refillship_os';
        }
        
        return $templateId;
    }

     /**
     * Get Club-silhouette Report Emails
     *
     * @return string|null
     */
    public function getCsReportsEmail(): ?string
    {
       return $this->getWebsiteConfig(self::CS_REPORT_EMAILS);
    }

    /**
     * Get ClubSilhouette Reports Email Template
     *
     * @return string|null
     */
    public function getCSReportEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CS_REPORT_EMAILS_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_users_report';
        }
        
        return $templateId;
    }

    /**
     * Get ClubSilhouette Reports Email Template
     *
     * @return string|null
     */
    public function getCSPaymentReminderEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CS_PAYMENT_REMAINDER_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_users_payment_reminder';
        }
        
        return $templateId;
    }

    /**
     * Get ClubSilhouette Renewal Missing Creditcard Email Template
     *
     * @return string|null
     */
    public function getCSRenewalMcEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CS_RENEWAL_MC_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_renewal_mc';
        }
        
        return $templateId;
    }

    /**
     * Get ClubSilhouette Points Email Template
     *
     * @return string|null
     */
    public function getCSPointsEmailTemplate(): ?string
    {
        $templateId = $this->getWebsiteConfig(self::CS_POINTS_TEMPLATE);
        if(empty($templateId))
        {
            $templateId = 'cs_points';
        }
        
        return $templateId;
    }


}
