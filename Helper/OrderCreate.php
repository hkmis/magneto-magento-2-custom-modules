<?php
namespace Magneto\ClubSilhouetteManager\Helper;

use \Magento\Directory\Model\CountryFactory;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\App\Helper\Context;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Catalog\Model\Product;
use \Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Quote\Api\CartManagementInterface;
use \Magento\Customer\Model\CustomerFactory;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Sales\Model\Order;
use \Magento\Sales\Model\Service\InvoiceService;
use \Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use \Magento\Framework\DB\Transaction;
use \Magento\Sales\Model\Convert\Order as CovertOrder;
use Psr\Log\LoggerInterface;
use \Magento\Store\Model\App\Emulation;

class OrderCreate extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var Config
     */
    protected $configHelper;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var Product
     */
    protected $_product;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepositoryInterface;
    /**
     * @var CartManagementInterface
     */
    protected $cartManagementInterface;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var Order
     */
    protected $order;
     /**
      * @var CountryFactory
      */
    protected $_countryFactory;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;
    /**
     * @var InvoiceService
     */
    protected $_invoiceService;
    /**
     * @var Transaction
     */
    protected $_transaction;

    /**
     * @var CovertOrder
     */
    protected $_convertOrder;
    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

   /**
    * @param Context                     $context
    * @param StoreManagerInterface       $storeManager
    * @param Product                     $product
    * @param CartRepositoryInterface     $cartRepositoryInterface
    * @param CartManagementInterface     $cartManagementInterface
    * @param CustomerFactory             $customerFactory
    * @param CustomerRepositoryInterface $customerRepository
    * @param Order                       $order
    * @param OrderRepositoryInterface    $orderRepository
    * @param InvoiceService              $invoiceService
    * @param InvoiceSender               $invoiceSender
    * @param Transaction                 $transaction
    * @param CountryFactory              $countryFactory
    * @param CovertOrder                 $convertOrder
    * @param Logger                      $logger
    */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Product $product,
        CartRepositoryInterface $cartRepositoryInterface,
        CartManagementInterface $cartManagementInterface,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        Order $order,
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        Transaction $transaction,
        CountryFactory $countryFactory,
        CovertOrder $convertOrder,
        LoggerInterface $logger,
        Emulation $emulation
    ) {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->invoiceSender = $invoiceSender;
        $this->_transaction = $transaction;
        $this->_countryFactory = $countryFactory;
        $this->_convertOrder = $convertOrder;    
        $this->logger = $logger;    
        $this->emulation = $emulation;
        parent::__construct($context);
    }
    /**
     * Create Order On Your Store
     *
     * @param array $orderData
     * @return array|null
     */
    public function createMageOrder($orderData, $paymentMethod = null): ?array
    {

        $result = [
            'order_id' =>'',
            'success' => false,
            'msg' =>''
        ];
        try {
            //starting the store emulation with area defined for admin
            $websiteId = $orderData['websiteId'];
            $this->emulation->startEnvironmentEmulation($websiteId, 'frontend');
            //$store=$this->_storeManager->getStore();
            
            $customer=$this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($orderData['email']);// load customet by email address
                        
            $cartId = $this->cartManagementInterface->createEmptyCart(); //Create empty cart
            $quote = $this->cartRepositoryInterface->get($cartId); // load empty cart quote
            //$quote->setStore($store);
            
            // if you have allready buyer id then you can load customer directly
            $customer= $this->customerRepository->getById($customer->getEntityId());
            $quote->setCurrency();
            $quote->assignCustomer($customer); //Assign quote to customer

            //add items in quote
            foreach ($orderData['items'] as $item) {
                $this->logger->info("Inside Quote loop");
                $product=$this->_product->load($item['product_id']);
                $product->setPrice(0);
                $quote->addProduct($product, (int)$item['qty']);
            }
            
            //Set Address to quote
            $quote->getBillingAddress()->addData($orderData['shipping_address']);
            $quote->getShippingAddress()->addData($orderData['shipping_address']);
            
            // Collect Rates and Set Shipping & Payment Method
            $shippingAddress= $quote->getShippingAddress();
            $shippingAddress->setCollectShippingRates(true)
                            ->collectShippingRates()
                            ->setShippingMethod('freeshipping_freeshipping'); //shipping method
            $quote->setPaymentMethod('free'); //payment method
            $quote->setInventoryProcessed(false); //not effetc inventory
            
            // Set Sales Order Payment
            $quote->getPayment()->importData(['method' => 'free']);
            $quote->setCustomerIsGuest(false);
            $quote->save(); //Now Save quote and your quote is ready
            
            // Collect Totals
            $quote->collectTotals();
            
            // Create Order From Quote
            $quote = $this->cartRepositoryInterface->get($quote->getId());                                   
            $orderId = $this->cartManagementInterface->placeOrder($quote->getId());            
            $order = $this->order->load($orderId);
            $order->setEmailSent(1);

            // discard the emulated environment after doing your work
            $this->emulation->stopEnvironmentEmulation();
            $increment_id = $order->getRealOrderId();
           
            if ($order->getEntityId()) {
                if ($order->canInvoice()) {
                    $this->createInvoice($orderId);
                }
                if ($order->canShip()) {
                    $this->createShipments($orderId, $orderData['siteId']);
                }

                $result = [
                    'order_id' =>$orderId,
                    'success' => true,
                    'msg' =>'Successfully Created'
                ];
                $this->logger->info("16");
            } else {
                
                $result = [
                    'order_id' =>'',
                    'success' => false,
                    'msg' =>'Something went wrong'
                ];
            }
        } catch (\Exception $e) {
        
            $result = [
                'order_id' =>'',
                'success' => false,
                'msg' =>$e->getMessage()
            ];                        
            
            $this->logger->info("Service Order Helper Data Order->".$e->getMessage());
        }
        
        return $result;
    }
    /**
     * Create Invoice
     *
     * @param int $orderId
     * @return void
     */
    public function createInvoice($orderId)
    {
        try {
            $order = $this->_orderRepository->get($orderId);
            if ($order->canInvoice()) {
                $invoice = $this->_invoiceService->prepareInvoice($order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->_transaction->addObject(
                    $invoice
                )->addObject(
                    $invoice->getOrder()
                );
                $transactionSave->save();
                $this->invoiceSender->send($invoice);
                //send notification code
                $order->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                )
                ->setIsCustomerNotified(true)
                ->save();
            }
        } catch (\Exception $e) {
            $this->logger->info("Service Order Helper Data Invoice->".$e->getMessage());
        }
    }

     /**
      * To get Country Name
      *
      * @param string $countryCode
      * @return string
      */
    public function getCountryname($countryCode): ?string
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
    /**
     * Get customer email
     *
     * @param int $customerId
     * @return string
     */
    public function getCustomerEmail($customerId): ?string
    {
        return $this->customerFactory->create()->load($customerId)->getEmail();
    }
    /**
     * Get order items
     *
     * @param int $orderId
     * @return mixed
     */
    public function getOrderItemsById($orderId): mixed
    {
        $order = $this->_orderRepository->get($orderId);
        return $order->getAllItems();
    }
    /**
     * Create Shipments
     *
     * @param int $orderId
     * @return void
     */
    public function createShipments($orderId, $source)
    {
        $order = $this->_orderRepository->get($orderId);
        // to check order can ship or not
        if (!$order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("You can't create the Shipment of this order.")
            );
        }
        $orderShipment = $this->_convertOrder->toShipment($order);
        foreach ($order->getAllItems() as $orderItem) {
            // Check virtual item and item Quantity
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $qty = $orderItem->getQtyToShip();
            $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qty);
            $orderShipment->addItem($shipmentItem);
        }
        $orderShipment->register();
        $orderShipment->getOrder()->setIsInProcess(true);
        try {
            // Save created Order Shipment
            $orderShipment->getExtensionAttributes()->setSourceCode($source);
            $orderShipment->save();
            $orderShipment->getOrder()->save();
            // Send Shipment Email
            $orderShipment->save();
        } catch (\Exception $e) {
            $this->logger->info("Service Order Helper Data Shipments->".$e->getMessage());
        }
    }
}
