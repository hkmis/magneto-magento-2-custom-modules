<?php
/**
 * @author Magneto Team
 * @copyright Copyright (c) 2022 Magneto (https://www.magneto.com)
 */
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Http\Context as httpContext;
use Magento\Authorization\Model\UserContextInterface; 
use Magento\Customer\Model\Customer;
use Magento\Checkout\Model\Session as checkoutSession;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as ClubSilhouetteUsersFactory;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteRefillShipmentsFactory as ClubSilhouetteRefillShipmentsFactory;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePaymentsFactory as ClubUsersPayments;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteActionsFactory as ClubUsersActions;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel\CollectionFactory as CsBundleCollection;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Stdlib\DateTime\DateTime as TimezoneInterface;
use Magneto\GenerateCodes\Model\SOSProductDownloadcard;
use Magneto\NotificationManager\Helper\Data as NotificationHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Catalog\Model\Product as ProductCollection;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Inventory\Model\ResourceModel\Source\CollectionFactory as InvenotryCollection;
use Magneto\ClubSilhouetteManager\Helper\OrderCreate;
use Psr\Log\LoggerInterface;
use DateTime;
use DateInterval;

class Data extends AbstractHelper
{

    public const REFILL_PRODUCT_MAT = 'clubsilhouette/general/refillitem_mat';
    
    public const REFILL_PRODUCT_BLADE = 'clubsilhouette/general/refillitem_blade';

    public const ATTRIBUTE_CODE = 'membership_product';

    public const TRIAL_LENGTH_DAYS = 30;
    
    public const MAX_DAYS_WITHOUT_PAYING = 90;

    public const PAYMENT_CYCLE_ANNUAL_MONTHS = 12;
    
    public const PAYMENT_CYCLE_MONTHLY_MONTHS = 1;

    public const MEMBERSHIP_STATUS_ACTIVE = 'A';
    public const MEMBERSHIP_STATUS_CANCELED = 'X';
    public const MEMBERSHIP_STATUS_ERROR = 'E';
    public const MEMBERSHIP_STATUS_EXPIRED = 'I';
    public const MEMBERSHIP_STATUS_POTENTIAL = 'P';

    public const ACTION_SIGNUP = 'S';
    public const ACTION_RECEIVE_POINTS = 'P';
    public const ACTION_UPDATE = 'U';
    public const ACTION_AGREE_TERMS = 'T';
    public const ACTION_CANCEL = 'X';
    public const ACTION_AGREE_TERMS_MSSG = 'User agreed to terms for trial membership';
    public const ACTION_SIGNUP_MSSG = 'User signed-up for trial membership';
    public const ACTION_RENEWAL = 'R';

    public const BUNDLE_NOTIFICATION_ID = 7;
    private const REMINDER_EMAIL_NUM_DAYS = 7;

    private const REFILL_SHIPMENTS_PER_YEAR = 3;

    public static $ALLOWED_PAYMENT_CYCLES = [self::PAYMENT_CYCLE_ANNUAL_MONTHS,
    self::PAYMENT_CYCLE_MONTHLY_MONTHS];

    /**
     * Constructor
     *
     * @param Context $context
     * @param Config $config
     * @param CollectionFactory $productFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param httpContext $session
     * @param Customer $customer
     * @param checkoutSession $checkoutSession
     * @param ClubSilhouetteUsersFactory $clubSilhouetteUsersFactory
     * @param ClubSilhouetteRefillShipmentsFactory $clubSilhouetteRefillShipmentsFactory
     * @param ClubUsersActions $clubUsersActions
     */
    public function __construct(
        Context $context,
        Config $config,
        CollectionFactory $productFactory,
        CustomerRepositoryInterface $customerRepository,
        AddressRepositoryInterface $addressRepository,
        httpContext $session,
        Customer $customer,
        checkoutSession $checkoutSession,
        ClubSilhouetteUsersFactory $clubSilhouetteUsersFactory,
        ClubSilhouetteRefillShipmentsFactory $clubSilhouetteRefillShipmentsFactory,
        ClubUsersActions $clubUsersActions,
        ClubUsersPayments $clubUsersPayments,
        CsBundleCollection $csBundleCollection,
        RemoteAddress $remoteAddress,
        TimezoneInterface $timezoneInterface,
        LoggerInterface $logger,
        SOSProductDownloadcard $sosProductDownloadcard,
        NotificationHelper $notificationHelper,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        StateInterface $state,
        StockResolverInterface $stockResolverInterface,
        GetProductSalableQtyInterface $salableQtyMsi,
        ProductCollection $productCollection,
        InvenotryCollection $invenotryCollection,
        OrderCreate $orderCreateHelper,
        UserContextInterface $userContextInterface
    ) {
        $this->config = $config;
        $this->productFactory = $productFactory;
        $this->customerRepository = $customerRepository;
        $this->addressRepository = $addressRepository;
        $this->customerSession = $session;
        $this->customer = $customer;
        $this->checkoutSession = $checkoutSession;
        $this->clubSilhouetteUsersFactory = $clubSilhouetteUsersFactory;
        $this->clubSilhouetteRefillShipmentsFactory = $clubSilhouetteRefillShipmentsFactory;
        $this->clubUsersActions = $clubUsersActions;
        $this->remoteAddress = $remoteAddress;
        $this->clubUsersPayments = $clubUsersPayments;
        $this->timezoneInterface = $timezoneInterface;  
        $this->logger = $logger;
        $this->csBundleCollection = $csBundleCollection;
        $this->sosProductDownloadcard = $sosProductDownloadcard;
        $this->notificationHelper = $notificationHelper;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $state;
        $this->stockResolverInterface = $stockResolverInterface;
        $this->salableQtyMsi = $salableQtyMsi;
        $this->productCollection = $productCollection;
        $this->invenotryCollection = $invenotryCollection;
        $this->orderCreateHelper = $orderCreateHelper;
        $this->userContextInterface = $userContextInterface;
        parent::__construct($context);
    }
    
    /**
     * Mat Products
     *
     * @return array
     */
    public function getRefillProducts($refillProd): ?array
    {
        $matProducts = [];
        $refillMat = $this->config->getWebsiteConfig($refillProd);
        $refillMatSkus = explode(',', $refillMat);
        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter('sku', ['in' => $refillMatSkus]);
        foreach ($collection as $product) {
            $productSku = $product->getSku();
            $productName = $product->getName();
            $matData = ['sku' => $productSku,'name' => $productName];
            $matProducts[] = $matData;
        }
        return $matProducts;
    }   

    /**
     * Get Product Salable Qty
     *
     * @param string $sku
     * @return int|string|float
     */
    public function getProductSalableQty($sku): string|int|null|float
    {
        $productQty = 0;
        if($sku)
        {
            $websiteCode = self::getStoreWebsiteCode(1);
            $stock = $this->stockResolverInterface->execute('website', $websiteCode);		
            try {
                $salableQty = $this->salableQtyMsi->execute($sku, $stock->getStockId());
                if ($salableQty) {
                    $productQty = $salableQty;
                }
            } catch (\Exception $e) {
            }
        }   
        return $productQty;        
    }

    /**
     * Get Product Id
     *
     * @param string $sku
     * @return int|string
     */    
    public function getProductId($sku)
    {        
        $productId = $this->productCollection->getIdBySku($sku);

        return $productId;
    }

    /**
     * Get Product Name
     *
     * @param string $sku
     * @return string
     */
    public function getProductName($sku): ?string
    {
        $product = $this->productCollection->loadByAttribute('sku', $sku);
        $productName = $product->getName();

        return $productName;
    }

    /**
     * Get Website Code
     *
     * @param int $websiteId
     * @return mixed
     */
    public function getStoreWebsiteCode($websiteId) : mixed
    {
		return $this->storeManager->getWebsite($websiteId)->getCode();
	}

    /**
     * Get Membership Products
     *
     * @return array|null
     */
    public function getMembershipProducts(): ?array
    {
        $membershipProds = [];
        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter(self::ATTRIBUTE_CODE, '1');
        foreach ($collection as $product) {
            $membership = ['name' => $product->getName(), 'sku' => $product->getSku()];
            $membershipProds[] = $membership;
        }
        return $membershipProds;
    }

    /**
     * Get ClubUserMat Product
     *
     * @param int $userId
     * @return array|null
     */
    public function getClubUserMatProduct($userId) : ?array
    {
        $matSku = '';
        $matData = [];

        $clubUser = $this->getClubUser($userId);        
        if($clubUser){
            $clubUserData = $clubUser->getData();
            foreach($clubUserData as $userData)
            {
                $matSku = $userData['refill1ItemNo'];
            }            
        }        
        
        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter('sku', ['in' => $matSku]);
        foreach ($collection as $product) {
            $productSku = $product->getSku();
            $productName = $product->getName();
            $matData = ['sku' => $productSku,'name' => $productName];            
        }

        return $matData;
    }

    /**
     * Get ClubUserBlade Product
     *
     * @param int $userId
     * @return array|null
     */
    public function getClubUserBladeProduct($userId) : ?array
    {
        $bladeSku = '';
        $bladeData = [];

        $clubUser = $this->getClubUser($userId);        
        if($clubUser){
            $clubUserData = $clubUser->getData();
            foreach($clubUserData as $userData)
            {
                $bladeSku = $userData['refill2ItemNo'];
            }            
        }        
        
        $collection = $this->productFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter(
            'status',
            \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
        );
        $collection->addAttributeToFilter('sku', ['in' => $bladeSku]);
        foreach ($collection as $product) {
            $productSku = $product->getSku();
            $productName = $product->getName();
            $bladeData = ['sku' => $productSku,'name' => $productName];            
        }

        return $bladeData;
    }
    
    /**
     * Get Default Shipping Address
     *
     * @return void
     */
    public function getShippingInfo($shippingId)
    {
        if ($shippingId) {
            $shippingAddress = $this->addressRepository->getById($shippingId);
            return $shippingAddress;        
        }
    }
    
    /**
     * Get CS User Shipping Id
     *
     * @return string|null
     */
    public function getClubUserShippingId($userId): ?string
    {
        $shippingId = '';

        $clubUser = $this->getClubUser($userId);        
        foreach($clubUser as $user)
        {
            $userData = $user->getData();
            $shippingId = $userData['shippingId'];
        }
        return $shippingId;
    }

    /**
     * Get Login Customer Id
     *
     * @return mixed
     */
    public function getLoginCustomerId() : mixed
    {
        $customerId = NULL;
        if ($this->customerSession->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
            $customerId = $this->userContextInterface->getUserId();
            return $customerId;
        } 
        return $customerId;
    }

    /**
     * Get Customer Address
     *
     * @return array
     */
    public function getCustomerAddress(): ?array
    {
        $customerId = $this->getLoginCustomerId();
        $customerObj = $this->customer->load($customerId);
        $customerAddress = [];

        foreach ($customerObj->getAddresses() as $address) {
            $customerAddress[] = $address->toArray();
        }

        return $customerAddress;
    }

    /**
     * Get Customer Cart
     *
     * @return array
     */
    public function getCustomerCart() : ?array
    {
        $cart =  $this->checkoutSession->getQuote();
        $result = $cart->getAllVisibleItems();
        return $result;
    }

    /**
     * Get Club Silhouette User points
     *
     * @param int $userId
     * @return string
     */
    public function getSilhouetteUserPoints($Id = null, $userId = null): ?string
    {
        $points = '';
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        if($Id)
        {
            $collection->addFieldToFilter('id', $Id);
        }
        if($userId)
        {
            $collection->addFieldToFilter('userId', $userId);
        }
                
        foreach ($collection as $data) {
            $points = $data->getPoints();
        }
        
        return $points;
    }

    /**
     * Get Shipping Date
     *
     * @return string
     */
    public function getJoinDate($Id) : ?string
    {
        $createdDate = '';
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('id', $Id);

        foreach ($collection as $data) {
            $dateCreated = $data->getData();
            if ($dateCreated) {
                $createdDate = $dateCreated['dateCreated'];
            }
        }
        return $createdDate;
    }

    /**
     * Get CS User Status 
     *
     * @return string
     */
    public function getCSUserStatus($userId) : ?string
    {
        $status = '';
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);

        foreach ($collection as $data) {
            $statusData = $data->getData();
            if ($statusData) {
                $status = $statusData['membershipStatus'];
            }
        }
        
        return $status;
    }    
    
    /**
     * Check if it's CS member
     *
     * @param int $user_id
     * @return boolean
     */
    public function is_member($user_id): bool
    {
        $is_member = false;
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $user_id);

        if ($collection->getData()) {
            $is_member = true;
        }

        return $is_member;
    }

    /**
     * Get Payment Cycle Month
     *
     * @param int $user_id
     * @return string
     */
    public function getPaymentCycleMonth($user_id): ?string
    {
        $paymentMonths = '';
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $user_id);

        foreach ($collection as $data) {
            $dateCreated = $data->getData();
            if ($dateCreated) {
                $paymentMonths = $dateCreated['paymentCycleMonths'];
            }
        }

        return $paymentMonths;
    }

    /**
     * Get Club User
     *
     * @param int $user_id
     * @return object|null
     */
    public function getClubUser($user_id = null, $Id = null): ?object
    {
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        if($user_id){
            $collection->addFieldToFilter('userId', $user_id);
        }
        if($Id){
            $collection->addFieldToFilter('id', $Id);
        }        

        return $collection;
    }    

    /**
     * Get Customer email using customer id
     *
     * @param int $id
     * @return string|null
     */
    public function getCustomerEmail($id): ?string
    {
        $email = '';
        $customer = $this->customerRepository->getById($id);
        $email = $customer->getEmail();

        return $email;
    }

    /**
     * Get Customer Ip Address
     *
     * @return string
     */
    public function getCustomerIp() : ?string
    {
        $userIp = $this->remoteAddress->getRemoteAddress();        
        return $userIp;
    }


    public function saveClubSilhouetteUser($user_id, bool $is_trial, $post, $is_free = false, DateTime $end_date = null)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/clubsilhouetRegisrationHelper.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $existing_member  = '';
        $previous_payment_cycle_months = '';
        $currentUserShipingId ='';
        $currentUserRefill1 = '';
        $currentUserRefill2='';
        $currentUserCardId = '';
        $cs_user = $this->getClubUser($user_id)->getData();
        if ($cs_user) {
            foreach ($cs_user as $user) {
                $existing_member  = $user['userId'];
                if ($existing_member) {
                    $existing_member =  true;
                } else {
                    $existing_member = false;
                }

                $previous_payment_cycle_months = $user['paymentCycleMonths'];
                $currentUserShipingId = $user['shippingId'];
                $currentUserRefill1 = $user['refill1ItemNo'];
                $currentUserRefill2 = $user['refill2ItemNo'];
                $currentUserCardId = $user['cardId'];
            }
        }
                           
        $return_arr = [
            'msg' => 'Unspecified error occurred',
            'success' => false
        ];

        //determine the payment start date
        $payment_date = new DateTime();
        if ($is_trial):
            $payment_date->add(DateInterval::createFromDateString(self::TRIAL_LENGTH_DAYS . ' days'));
        endif;
    
        //make sure the payment cycle is in an acceptable range    
        if (in_array($post['payment_cycle_months'], self::$ALLOWED_PAYMENT_CYCLES)):
            //make sure the mat is a valid SKU
            if (self::isValidMatItem($post['refill1_item_no'])):

                //make sure the blade is a valid SKU
                if (self::isValidBladeItem($post['refill2_item_no'])):
                                
                    $address = $this->getShippingInfo($currentUserShipingId);
                    $currentStreet = '';$region = '';$currPostCode ='';
                    $currFirstname ='';$currLastname = '';$currCity = '';
                    if ($address) {
                        $currentStreetData = $address->getStreet();
                        if ($currentStreetData) {
                            $currentStreet = $currentStreetData[0];
                        }
                            $regionData = $address->getRegion();
                        if ($regionData) {
                                $region = $regionData->getRegion();
                        }
                        $currFirstname = $address->getFirstname();
                        $currLastname = $address->getLastName();
                        $currCity = $address->getCity();
                        $currPostCode = $address->getPostcode();
                    }
                        $cs_preferences = (
                            '<i>' . 'Old Membership Settings' . '</i>' . '<br />' .
                            'Cycle: ' . $previous_payment_cycle_months . ' Month(s)' . '<br />'.
                            'Refill #1: ' . $currentUserRefill1 . '<br />'.
                            'Refill #2: ' . $currentUserRefill2 . '<br />'.
                            'Billing ID: ' . $currentUserCardId . '<br />' .
                            'Shipping Address: '. '<br />'.
                            '<addresss>' . $currFirstname.' '.$currLastname . '<br />' .
                            $currentStreet . '<br />' .
                            $currCity . ', ' . $region . ' ' . $currPostCode . '<br />' .
                            '</addresss><br />'
                        );
                                
                        $clubuserSaveData = [];
                        $clubuserSaveData['autoRenew'] = true;
                        $clubuserSaveData['cardId'] = $post['card_id'];

                    if (!$existing_member):
                        $clubuserSaveData['endDate'] = $payment_date->format('Y-m-d H:i:s');
                        $clubuserSaveData['membershipStatus'] = self::MEMBERSHIP_STATUS_ACTIVE;
                    elseif (isset($post['membership_status']) && in_array($post['membership_status'], self::validMembershipStatuses())):
                            $clubuserSaveData['membershipStatus'] = $post['membership_status'];
                    endif;

                    // if an end date has been set explicitly, use that
                    if (!empty($end_date)) {
                        $clubuserSaveData['endDate'] = $payment_date->format('Y-m-d H:i:s');
                    }

                    $clubuserSaveData['paymentCycleMonths'] = $post['payment_cycle_months'];
                    $clubuserSaveData['refill1ItemNo'] = $post['refill1_item_no'];
                    $clubuserSaveData['refill2ItemNo'] = $post['refill2_item_no'];
                    $clubuserSaveData['shippingId'] = $post['shipping_id'];
                    $clubuserSaveData['userId'] = $user_id;
                    if ($this->saveUsertoDb($clubuserSaveData)):

                        $logger->info('Data Saved');
                        //schedule deliveries, if necessary
                        if (!self::hasFutureShipments($user_id)):
                            $logger->info('Inside Shipments');
                            $start_date = $payment_date->format('Y-m-d H:i:s');
                            $logger->info('before get last shipment date');
                            $last_shipment_date = $this->getLastShipmentDate($user_id);

                            $logger->info('Above if last shipemtn date');
                            if (!empty($last_shipment_date)):
                                $logger->info('Inside last shipment cond');
                                if ((strtotime(date('Y-m-d H:i:s')) - strtotime($last_shipment_date)) < 60*60*24*4*30):
                                    $start_date = date('Y-m-d H:i:s', strtotime($last_shipment_date . ' +4 months'));
                                endif;
                            endif;
                            $logger->info('Before scheduleDefaultAnualShipments');
                            $this->scheduleDefaultAnualShipments($start_date, $user_id);
                            $logger->info('after scheduleDefaultAnualShipments');
                        endif;

                        // get last updated user data
                        $lastUpdatedUser = $this->getLastUpdatedUser($user_id);
                        $paymentCycleMonths = $lastUpdatedUser['paymentCycleMonths'];
                        $lastUserShipingId = $lastUpdatedUser['shippingId'];
                        $lastUserRefill1 = $lastUpdatedUser['refill1ItemNo'];
                        $lastUserRefill2 = $lastUpdatedUser['refill2ItemNo'];
                        $lastUserCardId = $lastUpdatedUser['cardId'];
                        $lastEndDate = $lastUpdatedUser['endDate'];
            
                        //delete future payments, so we can reschedule them
                        $logger->info('delete future payments, so we can reschedule them');
                        if ($existing_member && $previous_payment_cycle_months != $paymentCycleMonths && !$is_free) {
                            $this->deleteUnpaidPayments($user_id);
                        }

                        //schedule payments
                        $logger->info('schedule payments');
                        if (!$this->hasFuturePayments($user_id)):
                            $logger->info('schedule payments inside if');
                            $this->scheduleFuturePaymentsNewuser($user_id, $paymentCycleMonths, $lastEndDate, $is_free);
                            $logger->info('schedule payments after inside if');
                        endif;

                        //save actions, based on whether it's a sign-up or an update
                        if ($existing_member):

                            //if the user has overdue payments, attempt to bill them
                            if ($this->hasOverduePayments($user_id)):
                                //$cs_user->process_overdue_payments(); 
                                // Need to discuss this point with dharmesh how we gonna manage recurring payments
                            endif;

                            $address = $this->getShippingInfo($lastUserShipingId);
                            $lastStreet = ''; $region = '';

                            if ($address) {
                                    $lastStreetData = $address->getStreet();
                                if ($lastStreetData) {
                                    $lastStreet = $lastStreetData[0];
                                }
                                    $regionData = $address->getRegion();
                                if ($regionData) {
                                        $region = $regionData->getRegion();
                                }
                            }

                            $cs_preferences .= (
                                '<i>' . 'New Membership Settings' . '</i>' . '<br />' .
                                'Cycle: ' . $paymentCycleMonths . ' Month(s)' . '<br />'.
                                'Refill #1: ' . $lastUserRefill1 . '<br />'.
                                'Refill #2: ' . $lastUserRefill2 . '<br />'.
                                'Billing ID: ' . $lastUserCardId . '<br />' .
                                'Shipping Address: '. '<br />'.
                                '<addresss>' . $address->getFirstname().' '.$address->getLastName() . '<br />' .
                                $lastStreet . '<br />' .
                                $address->getCity() . ', ' . $region . ' ' . $address->getPostcode() . '<br />' .
                                '</addresss><br />'
                            );

                            //save update action
                            $ipAddress = $this->getCustomerIp();
                            $this->saveClubUsersActions($user_id, self::ACTION_UPDATE, $cs_preferences, $ipAddress);

                            //send an email to the customer, informing her that her membership has been updated
                            //#TODO: Complete this email STUB
                        else:

                            //save sign-up action
                            $logger->info('not existing user');
                            $ipAddress = $this->getCustomerIp();
                            $signupDate = date('Y-m-d H:i:s');
                            $this->saveClubUsersActions($user_id, self::ACTION_SIGNUP, self::ACTION_SIGNUP_MSSG, $ipAddress, null, null, $signupDate);

                            //save agree-to-terms action
                            $this->saveClubUsersActions($user_id, self::ACTION_AGREE_TERMS, self::ACTION_AGREE_TERMS_MSSG, $ipAddress, null, null, $signupDate);

                            //send an email to the customer, informing her that his membership has been created
                            $templateId = $this->config->getClubSilhouetteEmailTemplate();
                            $emailVars = [
                                'trialLength' => self::TRIAL_LENGTH_DAYS
                            ];
                            $userEmail = $this->getCustomerEmail($user_id);
                            $this->sendMail($userEmail,$templateId, $emailVars);

                            //check if there are any bundles that need to be sent from the current calendar month
                            /*$bundles_arr = ClubSilhouetteExclusiveDesignBundle::bundles_arr(date('Y') . '-' . date('m') . '-01', date('Y-m-d H:i:s'), $db, $timeout, true);
                            foreach ($bundles_arr as $b):
                                try {
                                    $b->send_to_user($user_id);
                                } catch (Exception $e) {
                                    General::write_log(self::LOG, date('Y-m-d H:i:s') . " - Error issuing Club bundle to user ID {$user_id}. EXCEPTION: {$e->getMessage()}\n");
                                }
                            endforeach;*/
                        endif;

                        //increase the user's Cloud storage limit
                         //self::check_cloud_storage_for_member($cs_user);

                        $return_arr['success'] = true;
                        $return_arr['msg'] = 'success';
                    else:
                            $return_arr['msg'] = 'There is some issue while saving your user.Please try again later!';
                    endif;
                            
                else:
                        $return_arr['msg'] = 'It appears you have chosen a SKU for the blade that is not valid.';
                endif;
            else:
                        $return_arr['msg'] = 'It appears you have chosen a SKU for the mat that is not valid.';
            endif;

        else:
                $return_arr['msg'] = 'Oops! It appears you need to select a payment cycle.';
        endif;
    
        return $return_arr;
    }

    /**
     * Check Mat Sku
     *
     * @param string $matSku
     * @return boolean
     */
    public function isValidMatItem($matSku): ?bool
    {
        $refillMat = $this->config->getWebsiteConfig(self::REFILL_PRODUCT_MAT);
        if ($refillMat) {
            $refillMatSkus = explode(',', $refillMat);

            if (in_array($matSku, $refillMatSkus)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Check Blade Sku
     *
     * @param string $bladeSku
     * @return boolean
     */
    public function isValidBladeItem($bladeSku): ?bool
    {
        $refillBlade = $this->config->getWebsiteConfig(self::REFILL_PRODUCT_BLADE);
        if ($refillBlade) {
            $refillBladeSkus = explode(',', $refillBlade);

            if (in_array($bladeSku, $refillBladeSkus)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /**
     * Valid Membership Status
     *
     * @return array
     */
    public function validMembershipStatuses() : ?array
    {
        return [
            self::MEMBERSHIP_STATUS_ACTIVE,
            self::MEMBERSHIP_STATUS_CANCELED,
            self::MEMBERSHIP_STATUS_ERROR,
            self::MEMBERSHIP_STATUS_EXPIRED,
            self::MEMBERSHIP_STATUS_POTENTIAL
        ];
    }

    /**
     * Save User in Db
     *
     * @return bool
     */
    public function saveUsertoDb($data): ?bool
    {
        $endDate = '';
        $membershipStatus = '';
        if ($data) {
            try {
                $userId = $data['userId'];
                $renew = $data['autoRenew'];
                $cardId = $data['cardId'];
                if (isset($data['endDate'])) {
                    $endDate = $data['endDate'];
                }
                $paymentCycleMonths = $data['paymentCycleMonths'];
                $matProduct = $data['refill1ItemNo'];
                $bladeProduct = $data['refill2ItemNo'];
                $shippingId = $data['shippingId'];
                if (isset($data['membershipStatus'])) {
                    $membershipStatus = $data['membershipStatus'];
                }
                $model = $this->clubSilhouetteUsersFactory->create();
                $model->load($userId, 'userId');
                if ($model->getId()) {
                    if ($membershipStatus) {
                        $model->setMembershipStatus($membershipStatus);
                    }
                    if ($endDate) {
                        $model->setEndDate($endDate);
                    }
                    $model->setPaymentCycleMonths($paymentCycleMonths);
                    $model->setCardId($cardId);
                    $model->setAutoRenew($renew);
                    $model->setShippingId($shippingId);
                    $model->setRefill1ItemNo($matProduct);
                    $model->setRefill2ItemNo($bladeProduct);
                    $model->save();
                } else {
                    $model->setData($data)->save();
                }
                return true;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                return false;
            }
            
        }
    }

    /**
     * Check has future shipments
     *
     * @return boolean
     */
    public function hasFutureShipments($user_id): ?bool
    {
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $user_id);
        $collection->addFieldToFilter('scheduledDate', ['gt' => new \Zend_Db_Expr('NOW()')]);
        $row = $collection->count();

        if ($row) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get Last Shipment Date
     *
     * @return string
     */
    public function getLastShipmentDate($user_id): ?string
    {
        $last_shipment_date = '';
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $user_id);
        $collection->setOrder('scheduledDate', 'desc');
        $data = $collection->getData();
        if ($data) {
            $last_shipment_date = $data[0]['scheduledDate'];
        }

        return $last_shipment_date;
    }

    /**
     * Schedule Annual Shipments
     *
     * @param date $start_date
     * @return void
     */
    public function scheduleDefaultAnualShipments($start_date, $user_id)
    {
        //add a day to the start date, so we can make sure they make their first payment
        $start_date = date('Y-m-d H:i:s', strtotime($start_date) + (60 * 60 * 24));

        //vars
        $dates_arr = $this->annualShipmentDatesArr($start_date);
        $no_errors = true;

        $model = $this->clubSilhouetteRefillShipmentsFactory->create();
        //loop and add them, avoiding weekends
        $data = [];
        foreach ($dates_arr as $date):

            //create a refill shipment
            $data['scheduledDate'] = $date;
            $data['userId'] = $user_id;
            try {
                $model->setData($data);
                $model->save();
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $no_errors = false;
                $this->message = 'While the user data is still consistent, there was an error saving a refill shipment: ' . $e->get_message();
            }
        endforeach;

        return $no_errors;
    }

    /**
     * Annual Shipment Dates
     *
     * @param date $start_date
     * @param int $number_of_shipments
     * @return void
     */
    public static function annualShipmentDatesArr($start_date, int $number_of_shipments = 3)
    {
        $date = new DateTime($start_date);
        $dates_arr = [];
        $num_days_to_add = (int) (365 / 3);

        //loop and add them, avoiding weekends
        for ($i = 0; $i < $number_of_shipments; $i++):

            //is it a weekend?
            switch (date('w', $date->getTimestamp())):

                //saturday?
                case 6:
                    $date->add(DateInterval::createFromDateString('2 days'));
                    break;

                //sunday?
                case 7:
                    $date->add(DateInterval::createFromDateString('1 day'));
                    break;
            endswitch;

            //add it to the array
            $dates_arr[] = $date->format('Y-m-d H:i:s');

            //update the date
            $date->add(DateInterval::createFromDateString($num_days_to_add . ' days'));
        endfor;

        return $dates_arr;
    }

    /**
     * Get Updated User's Data
     *
     * @return array
     */
    public function getLastUpdatedUser($userId): ?array
    {   
        $updatedUser = array();
        $userData = $this->getClubUser($userId)->getData();
        foreach($userData as $user)
        {            
            $updatedUser['paymentCycleMonths'] = $user['paymentCycleMonths'];
            $updatedUser['shippingId'] = $user['shippingId'];
            $updatedUser['refill1ItemNo'] = $user['refill1ItemNo'];
            $updatedUser['refill2ItemNo'] = $user['refill2ItemNo'];
            $updatedUser['cardId'] = $user['cardId'];
            $updatedUser['endDate'] = $user['endDate'];
        }

        return $updatedUser;
    }

    /**
     * Delete Unpaid Payments
     *
     * @return bool
     */
    public function deleteUnpaidPayments($userId, $nonFreeOnly = false): ?bool
    {
        $collection = $this->clubUsersPayments->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        
        if ($nonFreeOnly) {
            $collection->addFieldToFilter('amt', ['gt' => 0.00]);
        }
        
        $collection->addFieldToFilter('paymentDate', ['null' => true]);

        $connection = $collection->getConnection();
        $connection->beginTransaction();
        
        try {
            foreach ($collection as $item) {
                $item->delete();
            }
            $connection->commit();
            return true;
        } catch (\Exception $e) {
            $connection->rollBack();
            return false;
        }
    }
     
    public function hasFuturePayments($userId)
    {        
        try
        {
            $currentDate = $this->timezoneInterface->gmtDate();
            $collection = $this->clubUsersPayments->create()->getCollection();
            $collection->addFieldToFilter('userId', $userId);
            $collection->addFieldToFilter('paymentDate', ['null' => true]);
            $collection->addFieldToFilter('dueDate', ['gt' => $currentDate]);

            if($collection->Count())
            {
                return true;
            }
        }catch(\Exception $e){
            return false;
        }        

        return false;
    }

    public function scheduleFuturePaymentsNewuser($userId, $paymentCycleMonths, $lastEndDate, $is_free)
    {        
        //set price to zero if free
        if ($is_free){
            $annual_sku_price = 0.00;
            $monthly_sku_price = 0.00;
        }else{
            $annual_sku_price = 99.99;
            $monthly_sku_price = 9.99;
        }

        //is this a monthly deal, or a yearly deal?
        switch($paymentCycleMonths):
            case self::PAYMENT_CYCLE_MONTHLY_MONTHS:

                //figure out the first payment of 12
                $date = new DateTime($lastEndDate);
                if ($date->getTimestamp() < time()):
                    $date = new DateTime('now');
                endif;
                for ($i = 0; $i < 12; $i++):
                    $this->createUserPayment($userId, $monthly_sku_price, $date);

                    //add a month to the date
                    $date->add(DateInterval::createFromDateString('1 month'));
                endfor;

                break;
            case self::PAYMENT_CYCLE_ANNUAL_MONTHS:
                $this->createUserPayment($userId, $annual_sku_price, new DateTime($lastEndDate));
            default:
        endswitch;

        return true;
    }

    public static function addSubscriptionMonthsToDate($date, $num_of_months){
        $date_obj_1 = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', strtotime($date)));

        $year = $date_obj_1->format('Y');
        $month = $date_obj_1->format('n');
        $day = (int) $date_obj_1->format('d');

        $year += floor($num_of_months/12);
        $num_of_months = $num_of_months%12;
        $month += $num_of_months;
        if($month > 12) {
            $year ++;
            $month = $month % 12;
            if($month === 0)
                $month = 12;
        }

        if(!checkdate($month, $day, $year)) {
            $date_obj_2 = DateTime::createFromFormat('Y-n-j', $year.'-'.$month.'-1');
            $date_obj_2->modify('last day of');
        }else {
            $date_obj_2 = DateTime::createFromFormat('Y-n-d', $year.'-'.$month.'-'.$day);
        }
        $date_obj_2->setTime((int) $date_obj_1->format('H'), (int) $date_obj_1->format('i'), (int) $date_obj_1->format('s'));
        return $date_obj_2->format('Y-m-d H:i:s');
    }

    public function createUserPayment($user_id, $price, DateTime $date, $num_reminders_sent = 0)
    {
        $data = array();
        
        $data['amt'] = $price;
        $data['userId'] = $user_id;
        $data['dueDate'] = $date;
        $data['numRemindersSent'] = $num_reminders_sent;
        
        try{
            $pmt = $this->clubUsersPayments->create();
            $pmt->setData($data);
            $pmt->save();

            return true;

        }catch(\Exception $e)
        {
            return false;
        }
        
        return false;
    }

    /**
     * Save Users Action Logs
     *
     * @return bool
     */
    public function saveClubUsersActions($userId, $action = null, $notes = null, $ipAddress = null, $points = 0, $orderId = null, $date = null): ?bool
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/saveClubUsersActions.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $data = [];
        $clubActionModel = $this->clubUsersActions->create();

        $data['userId'] = $userId;
        $data['action'] = $action;
        $data['notes'] = $notes;
        $data['ipAddress'] = $ipAddress;
        $data['points'] = $points;
        $data['orderId'] = $orderId;

        $logger->info(print_r($data, true)); 
        if (empty($date)) {
            $date = date('Y-m-d H:i:s');
        }
        $data['date'] = $date;
        $logger->info(print_r($data, true)); 
        
        try 
        {
            $logger->info('inside try'); 
            $clubActionModel->setData($data);
            $clubActionModel->save();

            return true;
        }catch (\Exception $e) 
        {
            return false;
        }
    }

    /**
     * Get User's overdue payments
     *
     * @return bool
     */
    public function hasOverduePayments($userId): bool
    {
        $has_overdue_payments = false;
        $collection = $this->clubUsersPayments->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collection->addFieldToFilter('dueDate', ['lt' => new DateTime('-1 hour')]);
        $collection->addFieldToFilter('orderId', ['null' => true]);
        $row = $collection->count();

        if ($row) {
            $has_overdue_payments = true;
        }
        
        return $has_overdue_payments;
    }

    /**
     * Get User's payments
     *
     * @return object
     */
    public function userPayments($userId, $startDate, $endDate): ?object
    {
        $collection = $this->clubUsersPayments->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collection->addFieldToFilter('dueDate', ['from' => $startDate, 'to' => $endDate]);
        $collection->setOrder('dueDate', 'asc');
        $collection = $collection->getData();

        return $collection;
    }

    /**
     * Get User's payments
     *
     * @return void
     */
    public function processOverduePayments($userId)
    {
        $payments_arr = $this->userPayments($userId, date('Y-m-d H:i:s', time() - (60 * 60 * 24 * 365 * 5)), date('Y-m-d H:i:s'));

        $num_failed_payments = 0;
        foreach ($payments_arr as $pmt):

            //if it's overdue, attempt to process it
            if (empty($pmt['orderId'])):
                if (!$pmt->process()):
                    $num_failed_payments++;
                    $this->message = 'Unable to process payment ID ' . $pmt->get_id() . ": " . $pmt->get_message() . "\n";
                    General::write_log(ClubSilhouettePayment::LOG, date('Y-m-d H:i:s') . " - " . $this->message . "\n");
                endif;
            endif;
        endforeach;
    }

    /**
     * Get User's Past Shipped Shipments Data
     *
     * @return array
     */
    public function getShippedShipments($userId, $startDate, $endDate): ?array
    {
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();

        $collection->addFieldToFilter('userId', $userId);
        $collection->addFieldToFilter('scheduledDate', ['gteq' => $startDate]);
        $collection->addFieldToFilter('scheduledDate', ['lteq' => $endDate]);
        $collection->addFieldToFilter('orderId', ['notnull' => true]);
        $collection->setOrder('scheduledDate', 'desc');
        $collection = $collection->getData();

        return $collection;
    }

    /**
     * Get User's Past Unshipped Shipments Data
     *
     * @return array
     */
    public function getUnshippedShipments($userId, $startDate, $endDate): ?array
    {
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        
        $collection->addFieldToFilter('userId', $userId);
        $collection->addFieldToFilter('scheduledDate', ['gteq' => $startDate]);
        $collection->addFieldToFilter('scheduledDate', ['lteq' => $endDate]);
        $collection->addFieldToFilter('orderId', ['null' => true]);
        $collection->setOrder('scheduledDate', 'desc');
        $collection = $collection->getData();

        return $collection;
    }

    /**
     * Get User's Shipments Data
     *
     * @return array
     */
    public function getUserShipments($userId, $startDate, $endDate): ?array
    {
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        
        $collection->addFieldToFilter('userId', $userId);
        $collection->addFieldToFilter('scheduledDate', ['gteq' => $startDate]);
        $collection->addFieldToFilter('scheduledDate', ['lteq' => $endDate]);
        $collection->setOrder('scheduledDate', 'ASC');
        $collection = $collection->getData();

        return $collection;
    }

    /**
     * Add or Substract Club Points
     *
     * @return bool
     */
    public function addOrSubtractClubPoints($userId, $action = null, $notes = null, $ipAddress = null, $points = null, $orderId = null, $date = null): ?bool
    {             
        if($userId){

            $model = $this->clubSilhouetteUsersFactory->create();
            $model->load($userId, 'userId');
            
            if ($model->getId()) {
                try
                {
                    // Save points for current user
                    $currentPoints = $this->getSilhouetteUserPoints('',$userId);   
                    $deductedPoints = $points;                 
                    $points = $currentPoints + (float)$deductedPoints;

                    $model->setPoints($points);
                    $model->save();

                    // save action for points 
                    $ipAddress = $this->getCustomerIp();
                    $this->saveClubUsersActions($userId, $action, $notes, $ipAddress, $deductedPoints, $orderId);

                    return true;
                }catch(\Exception $e){                    
                    return false;
                }                

            }
        }
        return false;
    }

    /**
     * DeactivateExpired Members
     *
     * @return int
     */
	public function deactivateExpiredMembers(): ?int
    {        
		//vars
		$count = 0;		
       
		//deactivate members whose membership has expired
        $membershipStatus = array(self::MEMBERSHIP_STATUS_CANCELED, self::MEMBERSHIP_STATUS_EXPIRED, self::MEMBERSHIP_STATUS_POTENTIAL);
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('endDate', ['lt' => new DateTime('-2 days')]);
        $collection->addFieldToFilter('membershipStatus', array('nin' => $membershipStatus));    
		
        $newValue = self::MEMBERSHIP_STATUS_EXPIRED;     
		foreach ($collection as $item) {
            $item->setData('membershipStatus', $newValue);
            $item->save();            
            $count ++;
        }

		return $count;
	}
    
    /**
     * DeactivateExpired Members
     *
     * @return int
     */
	public function cancelLongExpiredMembers(): ?int
    {               
        //vars
		$count = 0;		
       
		//deactivate members whose membership has expired        
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $days = '-'.self::MAX_DAYS_WITHOUT_PAYING.' days';
                
        $collection->addFieldToFilter('endDate', ['lt' => new DateTime($days)]);
        $collection->addFieldToFilter('membershipStatus', self::MEMBERSHIP_STATUS_EXPIRED);    	        

		foreach ($collection as $item) {  
            $itemData = $item->getData();
            $userId = $itemData['userId'];           
            if ($this->cancel($userId, 'SYSTEM_LONG_EXPIRED')):
                $count++;
            endif;
        }        
		return $count;
	}

    /**
     * Cancel User Subscription
     *
     * @param string $canceler
     * @return string|bool|null
     */
    public function cancel($userId, $canceler = 'SYSTEM'): string|bool|null
    {                
        //remove unpaid payments
		if ($this->userRemoveUnpaidPayments($userId)){                
				//remove unshipped shipments
				if ($this->userRemoveUnshippedShipments($userId)){

					//update user member status and end date
					$membershipStatus = self::MEMBERSHIP_STATUS_CANCELED;
					$endDate = date('Y-m-d H:i:s');

                    $model = $this->clubSilhouetteUsersFactory->create();
                    $model->load($userId, 'userId');
                    if ($model->getId()) 
                    {          
                        $model->setData('membershipStatus', $membershipStatus);
                        $model->setData('endDate', $endDate);
                        $saved = $model->save();
                        if($saved)
                        {                            
                            $notes = 'Membership cancellation by user #'.$canceler;
                            if(!$this->saveClubUsersActions($userId, self::ACTION_CANCEL, $notes)){
                                return false;
                            }
                        }
                    }					
				}else{
					return false;
                }
        }else{
			return false;			
        }

        return true;
    }

    /**
     * Remove Unpaide payments
     *
     * @param int $userId     
     * @return bool
     */
    public function userRemoveUnpaidPayments($userId): ?bool
    {        
        $collection = '';
        $collection = $this->clubUsersPayments->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);                
        $collection->addFieldToFilter('orderId', ['null' => true]);

        try {
            if($collection->getData())
            {
                foreach ($collection as $item) {
                    $id = $item->getId();                    
                    $item->delete();
                }                        
                return true;
            }else{
                return false;
            }
            
        } catch (\Exception $e) {            
            return false;
        }    
    }

    /**
     * Remove Unshipped Shipments of user
     *
     * @param int $userId
     * @return bool
     */
    public function userRemoveUnshippedShipments($userId): ?bool
    {        
        $collection = '';
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);                
        $collection->addFieldToFilter('orderId', ['null' => true]);        
            
        try {
            if($collection->getData())
            {
                foreach ($collection as $item) {
                    $item->delete();
                }
                return true;
            }else{
                return false;
            }
            
        } catch (\Exception $e) {            
            return false;
        }    
    }

    /**
     * Reactivate Inactive Members
     *
     * @return void
     */
    public function reactivateInactiveMembers()
    {        
        //vars
        $count = 0;
        $users_needing_reactivation = $this->needsReactivationUsers();
        foreach ($users_needing_reactivation as $user):
            $userId = $user->getData('userId');
            $user->setData('membershipStatus', self::MEMBERSHIP_STATUS_ACTIVE);
            $user->setData('endDate', date('Y-m-d H:i:s', time() + (60 * 60 * 24)));    
            if ($user->save()):
                if (!$user->hasFutureShipments($userId)):
                    $this->scheduleDefaultAnualShipments(date('Y-m-d H:i:s'), $userId);
                endif;
                $count++;
            else:
                $this->logger->info('Clubsilhouette user can not be activated.');
            endif;
        endforeach;

        return $count;
    }

    /**
     * Get Reactivation Users
     *
     * @return object
     */
    public function needsReactivationUsers()
    {        
        $collection = '';
        $statusInactive = self::MEMBERSHIP_STATUS_EXPIRED; 

        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();                
        $collection->getSelect()->joinLeft(
            ['p' => '_clubSilhouettePayments'],
            'main_table.userId = p.userId AND p.orderId IS NULL AND p.dueDate < NOW()',
            []
        )->where('main_table.membershipStatus = ?', $statusInactive)
        ->group('main_table.userId');                                                

        return $collection;
                
    }

    /**
     * Get Available Bundles
     *
     * @return mixed
     */
    public function availableBundlesArr(): mixed
    {
        $date = date('Y-m-d H:i:s');
        $collection = $this->csBundleCollection->create();
        $collection->addFieldToFilter('dateAvailable', ['lt' => $date]);
        $collection->addFieldToFilter('sent', '0');
                
        return $collection;
    }

    /**
     * Get Active Members
     *
     * @return array
     */
    public function activeMemberUserIdArr(): ?array
    {        
        $userIds = array();
        $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();
        $collection->addFieldToFilter('membershipStatus', self::MEMBERSHIP_STATUS_ACTIVE);        
        
        $collection = $collection->getData();
        foreach($collection as $user){
            $userIds[] = $user['userId'];
        }

        return $userIds;
    }

    /**
     * Send Bundle To Users
     *
     * @return boolean|null
     */
    public function sendBundleToUser($userId, $bundleId): ?bool
    {           
        //set up the main notification
		$notification_id = self::BUNDLE_NOTIFICATION_ID;    

        //create 16-digit code
		$random_id = $this->sosProductDownloadcard->generateCardId($this->sosProductDownloadcard::CARD_ID_LENGTH);        
        $card_number = $this->sosProductDownloadcard->generate16digitCode($this->sosProductDownloadcard::CARD_TYPE_BUNDLE);        
        $card_id = $this->sosProductDownloadcard::CARD_TYPE_BUNDLE . $random_id;        

        // attempt to insert
        $card = $this->sosProductDownloadcard->createDownloadCard($card_id, $card_number, 0.00, 'USD', 
        $this->sosProductDownloadcard::CARD_TYPE_BUNDLE, null, $userId);
        
        if (!empty($card)):

            //try to create the bundle itself
                if ($this->sosProductDownloadcard->createDownloadCardBundle($card_id, $bundleId)):                    
                        if($this->sendNotificationUsers($userId, $notification_id))
                        {
                            echo 'Successfully scheduled notification.';
                        }else{
                            echo 'Error creating exclusive design notification for';
                        }                    

                    // try applying the code

                    $userId = (int)$userId;
                    $return_arr = $this->sosProductDownloadcard->applyCardBundle($userId, 'USD', $card);
                    if ($return_arr['success']):
                        
                        //send email Need to write send email code                         
                        $templateId = $this->config->getExclusiveBundleEmailTemplate();
                        echo 'templateId'.$templateId;echo '\n';
                        $email = $this->config->getExclusiveBundleEmail();
                        echo 'email'.$email;echo '\n';
                        $this->sendMail($email, $templateId);
                        
                        echo 'email sent';
                        return true;
                    else:
                        echo 'email not sent';
                        return false;
                    endif;
                else:
                    echo 'Error attaching bundle to exclusive download code for'.$userId;
                    return false;
                endif;

            else:
                echo 'Error creating exclusive design download code for'.$userId;
                return false;
        endif;
    }   

    /**
     * Send Notification To Users
     *
     * @param int $userId
     * @return bool
     */
    public function sendNotificationUsers($userId, $notificationId): ?bool
    {
        try
        {
            $this->notificationHelper->sendNotification($userId, null, $type = 'recurring', $notificationId);
            return true;

        }catch(\Exception $e)
        {
            return false;
        }        

    }


    /**
     * Send Mail
     *
     * @param array $data
     * @return boolean
     */
    public function sendMail($email, $templateId, $templateVars = null): bool
    {              
        $toEmail = $email;                         
        $fromEmail= $this->config->getFromEmail();
        $fromName = $this->config->getFromName();

        try {
                if(empty($templateVars))
                {
                    $templateVars = [];
                }                
     
                $storeId = $this->storeManager->getStore()->getId();

                $from = ['email' => $fromEmail, 'name' => $fromName];
                $this->inlineTranslation->suspend();
     
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $templateOptions = [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $storeId
                ];
                $transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($toEmail)
                    ->getTransport();
                $transport->sendMessage();

                $this->inlineTranslation->resume();
                return true;
        } catch (\Exception $e) {
            $this->logger->error("Clubsilhouette Email Error");
            $this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * sendReviewReminderEmails
     *
     * @return void
     */
    public function sendReviewReminderEmails(string $shipStartDate, string $shipEndDate, string $clubSettingsUrl)
    {
        $i = 0;
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        $collection->addFieldToFilter('scheduledDate' , ['gteq' => $shipStartDate]);
        $collection->addFieldToFilter('scheduledDate' , ['lteq' => $shipEndDate]);

        foreach($collection as $shipment){                       
            $shipmentData = $shipment->getData();
            $userId = $shipmentData['userId'];            
            $shippingId = $this->getClubUserShippingId($userId);
            $address = $this->getShippingInfo($shippingId);
            $matProduct = $this->getClubUserMatProduct($userId);
            $bladeProduct = $this->getClubUserBladeProduct($userId);

            //get Shipping Data
            $street = '';$region = '';$currPostCode ='';$currCity = '';$country='';
            if ($address) {
                $streetData = $address->getStreet();
                if ($streetData) {
                    $street = $streetData[0];
                }
                $regionData = $address->getRegion();
                if ($regionData) {
                        $region = $regionData->getRegion();
                }                
                $currCity = $address->getCity();
                $currPostCode = $address->getPostcode();
                $countryId = $address->getCountryId();                                                
            }            

            try {
                //get email address
    			$userEmail = $this->getCustomerEmail($userId);
                $templateId = $this->config->getClubSilhouetteRefillShipEmailTemplate();                
                $emailVars = 
                    [
                        'street' => $street,
                        'region' => $region,
                        'city' => $currCity,
                        'address_country' => $countryId,
                        'postCode' => $currPostCode,
                        'club_settings_url' => $clubSettingsUrl,
                        'item_1_item_number' => $matProduct['sku'],
                        'item_1_name' => $matProduct['name'],
                        'item_2_item_number' => $bladeProduct['sku'],
                        'item_2_name' => $bladeProduct['name']
                    ];                    
                if ($this->sendMail($userEmail, $templateId, $emailVars)):
                    $i++;
                endif;
            } catch (Exception $e){
                $this->logger('Error While sending email');
            }

        }

        return $i;
    }

    /**
     * Get Due Shipment
     *
     * @return void
     */
    public function dueShipments()
    {   
        $status = "'".self::MEMBERSHIP_STATUS_ACTIVE."'";
        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();                
        $collection->getSelect()->join(
            ['u' => '_clubSilhouetteUsers'],
            'main_table.userId = u.userId',
            ['shippingId' => 'u.shippingId', 'refill1ItemNo' => 'u.refill1ItemNo', 'refill2ItemNo' => 'u.refill2ItemNo']
        )->where('main_table.scheduledDate < NOW() AND main_table.orderCreationDate IS NULL AND u.membershipStatus ='. $status);  
                
        return $collection;

    }

    /**
     * Remove Stacked Payments
     *
     * @param boolean $print_updates
     * @param integer $mod
     * @param integer $remainder
     * @return void
     */
    public function usersRemoveStackedPayments(bool $print_updates = false, int $mod = 1, int $remainder = 0)
    {
        $errors_count = 0;
        if ($print_updates):
            $this->logger->info('Querying users with stacked payments.');
        endif;
        $users_with_stacked_payments = $this->usersWithStackedPaymentsArr($mod, $remainder);
        if ($print_updates):
            $this->logger->info('Found'. count($users_with_stacked_payments) . 'users with stacked payments. Looping users.');            
        endif;

        foreach ($users_with_stacked_payments as $user_id):
            if ($user_id % $mod == $remainder):
                if (self::deleteUnpaidPayments($user_id)):
                    if ($print_updates):                        
                        $this->logger->info('Removed stacked payments for'. $user_id);
                    endif;
                else:
                    $errors_count++;
                endif;
            endif;
        endforeach;

        if ($print_updates):
            $this->logger->info('JOB COMPLETE!' . $errors_count. 'errors removing stacked payments. Check log file for any specific error messages');            
        endif;
    }

    /**
     * Get User Stacked Payments Array
     *
     * @return array        
     */
    public function usersWithStackedPaymentsArr(int $mod, int $remainder): ?array
    {
        $user_ids = array();

        $collection = $this->clubUsersPayments->create()->getCollection();                
        $collection->getSelect()->from(['p' => '_clubSilhouettePayments'], ['total' => 'COUNT(*)', 'user_id' => 'p.userId'])
        ->where('p.dueDate < DATE_SUB(NOW(), INTERVAL 1 DAY)')
        ->where('p.paymentDate IS NULL')
        ->where("p.userId MOD $mod = $remainder")
        ->group('p.userId')
        ->having('COUNT(*) > 1');  

        foreach($collection as $user)
        {
            $user_ids[] = $user->getUserId();
        }
                      
        return $user_ids;
    }

    /**
     * schedule Future payments for Users
     *
     * @param sring $end_date
     * @param integer $mod
     * @param integer $remainder
     * @return bool
     */
    public function scheduleFuturePayments($end_date, int $mod = 1, int $remainder = 0): ?bool
    {
        $annual_sku_price = 99.99;
        $monthly_sku_price = 9.99;

        try{        
            $collection = $this->clubSilhouetteUsersFactory->create()->getCollection();                
            $collection->getSelect()->from(['csu' => '_clubSilhouetteUsers'], ['user_id' => 'csu.userId', 'end_date' => 'csu.endDate', 'payment_cycle_months' => 'csu.paymentCycleMonths'])
            ->joinInner(['u' => 'customer_entity'], 'u.entity_id = csu.userId')
            ->joinLeft(
                ['p' => '_clubSilhouettePayments'],
                'csu.userId = p.userId AND p.orderId IS NULL',
                []
            )
            ->where('csu.endDate < ?', $end_date)
            ->where('csu.autoRenew = ?', 1)
            ->where('p.id IS NULL')
            ->where('csu.membershipStatus IN (?, ?)', [self::MEMBERSHIP_STATUS_ACTIVE, self::MEMBERSHIP_STATUS_EXPIRED])
            ->where('csu.autoRenew = ?', 1)
            ->where('u.is_active = ?', 1)
            ->group('csu.userId'); 
                    
            foreach($collection as $user)
            {               
                $userData = $user->getData();                        
                //is this a monthly deal, or a yearly deal?
                if($userData['paymentCycleMonths'])
                {                      
                    if((int)$userData['paymentCycleMonths'] == self::PAYMENT_CYCLE_MONTHLY_MONTHS)
                    {                    
                        //figure out the first payment of 12
                        $date = new DateTime($userData['endDate']);
                        if ($date->getTimestamp() < time()):
                            $date = new DateTime('now');
                        endif;
                        for ($i = 0; $i < 12; $i++):
                            $this->createUserPayment($userData['userId'], $monthly_sku_price, $date);

                            //add a month to the date
                            $date->add(DateInterval::createFromDateString('1 month'));
                        endfor;

                    }else{                    
                        $this->createUserPayment($userData['userId'], $annual_sku_price, new DateTime($userData['endDate']));
                    }
                }
                            
            }        
            
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    
    }

    /**
     * Send Payment Reminder Emails
     *
     * @return bool
     */
    public function sendPaymentReminderEmails(): ?bool
    {
        $collection = $this->clubUsersPayments->create()->getCollection();                
        $collection->getSelect()->from(['p' => '_clubSilhouettePayments'])
        ->columns([
            'amt',
            'due_date' => 'dueDate',
            'id',
            'user_id' => 'userId',
            'email' => 'u.email',
            'firstname' => 'u.firstname',
            'lastname' => 'u.lastname',
            'payment_cycle_months' => 'cu.paymentCycleMonths'
        ])
        ->join(
            ['u' => 'customer_entity'],
            'p.userId = u.entity_id',
            []
        )
        ->join(
            ['cu' => '_clubSilhouetteUsers'],
            'u.entity_id = cu.userId',
            []
        )
        ->where('cu.membershipStatus NOT IN(?)', [self::MEMBERSHIP_STATUS_CANCELED])
        ->where('p.orderId IS NULL')
        ->where('p.numRemindersSent = 0')
        ->where('p.dueDate < DATE_ADD(NOW(), INTERVAL ? DAY)', [self::REMINDER_EMAIL_NUM_DAYS]);


        foreach($collection->getData() as $userPayment)
        {
            $payment_cycle_string = 'annual';
			switch($userPayment['payment_cycle_months']):
				case self::PAYMENT_CYCLE_ANNUAL_MONTHS:
					$payment_cycle_string = 'annual';
					break;
				case self::PAYMENT_CYCLE_MONTHLY_MONTHS:
					$payment_cycle_string = 'monthly';
					break;
			endswitch;

            $email = $this->getCustomerEmail($userPayment['userId']);
            $emailVars = [
                'user_name' => $userPayment['firstname'].' '.$userPayment['lastname'],
                'payment_cycle_string' => $payment_cycle_string,
                'formatted_date' => $userPayment['due_date']
            ];
            $templateId = $this->config->getCSPaymentReminderEmailTemplate();

            $this->sendMail($email, $templateId, $emailVars);

            $model = $this->clubUsersPayments->create();
            $model->load($userPayment['id'], 'id');

            if($model->getId())
            {
                try
                {
                    $model->setData('numRemindersSent',1);
                    $model->save();
                }catch(\Exception $e){
                    $this->logger->info('There is some problem while updating the user payment');
                    return false;
                }
                
            }

        }

        return true;
    }

    /**
     * Get Pending Payment Collection
     *
     * @return bool
     */
    public function pendingPaymentsArr($endDate)
    {
        $paymentCollection = $this->clubUsersPayments->create()->getCollection();                
        $paymentCollection->getSelect()
        ->join(
            ['u' => 'customer_entity'],
            'main_table.userId = u.entity_id',
            []
        )
        ->join(
            ['csu' => '_clubSilhouetteUsers'],
            'csu.userId = main_table.userId',
            []
        )
        ->where('csu.membershipStatus NOT IN(?)', [self::MEMBERSHIP_STATUS_CANCELED])
        ->where('main_table.orderId IS NULL')
        ->where('main_table.dueDate < ?', $endDate)
        ->where('u.is_active = ?', 1)       
        ->order('main_table.dueDate ASC');
        
        return $paymentCollection;
    }

    /**
     * Process Renewal
     *
     * @param object $collection
     * @return void
     */
    public function CSProcessRenewal($collection)
    {      
        $response = [
            'success' => true,
            'mssg' => 'sucess'
        ];  
        $userId = $collection->getData('userId');        
        $id = $collection->getData('id');
        $clubUser = $this->getClubUser($userId);        
        $clubUserData = $clubUser->getData();
            if(isset($clubUserData))
            {
                $status = $clubUserData[0]['membershipStatus'];
                if($status != self::MEMBERSHIP_STATUS_CANCELED)
                {
                    if(isset($clubUserData[0]['cardId']))
                    {
                        $shippingId = $clubUserData[0]['shippingId'];
                        $csUserEndDate = $clubUserData[0]['endDate'];                        
                        $paymentCycleMonths = $clubUserData[0]['paymentCycleMonths'];
                        $membershipProdSku = 'Monthly';
                        if($paymentCycleMonths == 12)
                        {
                            $membershipProdSku = 'Annually';
                        }
                        $membershipProdId = $this->getProductId($membershipProdSku);
                        $cardId = $clubUserData[0]['cardId'];

                        $items = array(array('product_id' => $membershipProdId, 'qty' => 1));
                        $shippingAddress = $this->getShippingInfo($shippingId);
                        $shipStreet = '';
                        $currentStreetData = $shippingAddress->getStreet();
                        if ($currentStreetData) {
                            $shipStreet = $currentStreetData[0];
                        }
                        $websiteId= 1;$sourceCode = '';
                        $regionId = $shippingAddress->getRegion()->getRegionId();
                            
                        $sourceCode = $this->getSourceCode($regionId);

                        $tempOrder = [
                            'shipping_address' =>[
                                'firstname' => $shippingAddress->getFirstname(),
                                'lastname'=> $shippingAddress->getLastname(),
                                'street' => $shipStreet,
                                'city' => $shippingAddress->getCity(),
                                'country_id' => $shippingAddress->getCountryId(),
                                'region' => $shippingAddress->getRegion()->getRegion(),
                                'postcode' => $shippingAddress->getPostcode(),
                                'telephone' => $shippingAddress->getTelephone(),                      
                            ],
                                'customer_id'  => $userId,
                                'email' => $this->getCustomerEmail($userId),
                                'currency_id'  => 'USD',
                                'items'=> $items,
                                'siteId' => $sourceCode,
                                'websiteId' => $websiteId
                        ];
                        
                        $orderData = $this->orderCreateHelper->createMageOrder($tempOrder);
                        if ($orderData['success'] && $orderData['order_id']!='') {
                            
                            $orderId = $orderData['order_id'];
                            $paymentDate = date('Y-m-d H:i:s');
                            
                            // Update Payment Record with orderid & payment date
                            $model = $this->clubUsersPayments->create();
                            $model->load($id, 'id');
                            if($model->getId())
                            {
                                try
                                {
                                    $model->setData('orderId', $orderId);
                                    $model->setData('paymentDate', $paymentDate);
                                    $model->save();

                                    // Update CS User enddate and membership status
                                    if($this->updateEndDate($userId, $csUserEndDate, $paymentCycleMonths))
                                    {
                                        //does the user need new shipments scheduled? (they do if they have no future shipments scheduled)
                                        if($this->hasFutureShipments($userId))
                                        {
                                            //schedule shipments
                                            $shipment_date = $this->nextShipmentDate($userId);
                                            if(!self::scheduleDefaultAnualShipments($shipment_date, $userId)){
                                                $this->logger->info('Could not schedule future shipments for user');
                                            }

                                        }                                        
                                        
                                        $renewalDate = date('Y-m-d H:i:s');
                                        if(!$this->saveClubUsersActions($userId, self::ACTION_RENEWAL, 'automatic membership renewal', 
                                        null, null, null, $renewalDate)){
                                            $this->logger->info('ERROR - Error saving renewal action to DB');
                                        }                                        

                                    }
                                    
                                    //Need to Do Code For GP Order Sync
                                    //Need to do payment usig nuvei payment method                                    
                                    
                                    $response = [
                                        'success' => true,
                                        'mssg' => 'User Renewal process has been completed'
                                    ];
                                    return $response;

                                }catch(\Exception $e){
                                    $this->logger->info('There is some problem while updating the user payment');
                                    $response = [
                                        'success' => false,
                                        'mssg' => 'ERROR - Unable to save Club Silhouette user '.$userId.
                                        ' but could not save CS payment object to DB'
                                    ];
                                    return $response;
                                }
                                
                            }else{
                                $response = [
                                    'success' => false,
                                    'mssg' => 'ERROR - Billed Club Membership successfully for user '.$userId.
                                    ' but could not save CS payment object to DB'
                                ];
                                return $response;
                            }

                        }else{
                            $response = [
                                'success' => false,
                                'mssg' => 'ERROR - Error while creating the customer order'.$userId
                            ];
                            return $response;
                        }

                    }else{
                        // Credit card is not saved
                        $email = $this->getCustomerEmail($userId);                        
                        $templateId = $this->config->getCSRenewalMcEmailTemplate();
                        $this->sendMail($email, $templateId);

                        $this->changeCSUserStatus($userId);

                        $response = [
                            'success' => false,
                            'mssg' => 'ERROR - Error billing Silhouette user: no card found'.$userId
                        ];
                        return $response;
                    }

                }else{
                    $response = [
                        'success' => false,
                        'mssg' => 'ERROR - User is not in a consistent state'
                    ];
                    return $response;
                }

            }

        $response = [
            'success' => false,
            'mssg' => 'ERROR - User data not found'
        ];        
        return $response;
    }

    /**
     * Get Source Code
     *
     * @return string|null
     */
    public function getSourceCode($regionId) : ?string
    {
        $sourceCode = 'default';
        if($regionId)
        {
            $invenotryCollection = $this->invenotryCollection->create();
            $invenotryCollection->addFieldToFilter('region_id', $regionId);
            foreach($invenotryCollection as $inventory)
            {
                $sourceCode = $inventory->getSourceCode();                        
            }
        }
        
        return $sourceCode;
    }

    /**
     * Update EndDate Of CS user
     *
     * @return bool
     */
    public function updateEndDate($userId, $endDate, $paymentCycleMonths)
    {
        $model = $this->clubSilhouetteUsersFactory->create();
        $model->load($userId, 'userId');
        if($model->getId())
        {
            try
            {
                $user_end_date = new DateTime($endDate);
  			    $user_end_date->add(DateInterval::createFromDateString($paymentCycleMonths . ' months'));
                $model->setData('endDate', $user_end_date);
                $model->setData('membershipStatus', self::MEMBERSHIP_STATUS_ACTIVE);
                $model->save();

                return true;
            }catch(\Exception $e){
                $this->logger->info('There is some problem while updating the user payment');
                return false;
            }        
        }
        return false;
    }


    public function nextShipmentDate($userId)
    {
        $shipmentDate = '';
        $shipmentDate = new DateTime('now');
		$num_days_to_add = (int) (365 / 3);

        $collection = $this->clubSilhouetteRefillShipmentsFactory->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId)
        ->setOrder('scheduledDate', 'DESC')
        ->setPageSize(1)
        ->setCurPage(1);

        foreach($collection as $shipment)
        {            
            $scheduledDate = $shipment->getData('scheduledDate');
            if (!empty($scheduledDate)):
                $shipmentDate =  new DateTime(date('Y-m-d H:i:s', strtotime($scheduledDate)));

                // if the date retrieved is way in the past, we don't want to schedule past shipments, so we'll make it $num_days_to_add days ago
                if ($shipmentDate->getTimestamp() < (time() - (60 * 60 * 24 * $num_days_to_add))):
                    $shipmentDate = new DateTime(date('Y-m-d H:i:s', time() - (60 * 60 * 24 * $num_days_to_add)));
                endif;
            endif;
            
        }
        $shipmentDate->add(DateInterval::createFromDateString($num_days_to_add . ' days'));
        
        return $shipmentDate->format('Y-m-d H:i:s');

    }

    /**
     * Change CS User Status
     *
     * @param int $userId
     * @return bool
     */
    public function changeCSUserStatus($userId): ?bool
    {
        $model = $this->clubSilhouetteUsersFactory->create();
        $model->load($userId, 'userId');
        if($model->getId())
        {
            try
            {                                
                $model->setData('membershipStatus', self::MEMBERSHIP_STATUS_EXPIRED);
                $model->save();

                return true;
            }catch(\Exception $e)
            {
                $this->logger->info('There is some problem while updating the user payment');
                return false;
            }        
        }
        return false;
    }
}
