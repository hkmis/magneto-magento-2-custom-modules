<?php

declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Setup\Patch\Schema;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Setup\SalesSetup;
use Magento\Sales\Setup\SalesSetupFactory;

class AddCreditUsed implements SchemaPatchInterface
{
    /**
     * @var SalesSetup
     */
    private $salesSetup;

    /**
     * @var QuoteSetup
     */
    private $quoteSetup;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var array<string, string>
     */
    private $flatEntityTables = [
        Order::ENTITY       => 'sales_order',
        'order_item'        => 'sales_order_item',
        'quote'             => 'quote',
        'quote_item'        => 'quote_item',
        'invoice'           => 'sales_invoice',
        'invoice_item'      => 'sales_invoice_item',
        'creditmemo'        => 'sales_creditmemo',
        'creditmemo_item'   => 'sales_creditmemo_item'
    ];

    public function __construct(
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        ResourceConnection $resourceConnection
    ) {
        $this->salesSetup = $salesSetupFactory->create();
        $this->quoteSetup = $quoteSetupFactory->create();
        $this->resourceConnection = $resourceConnection;
    }

    public function apply()
    {
        $this->addOrderAttributes();
        $this->addQuoteAttributes();
        $this->addInvoiceAttributes();
        $this->addCreditmemoAttributes();

        return $this;
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }

    private function addOrderAttributes(): void
    {                
        $this->addAttributeIfNotExists(
            Order::ENTITY,
            SalesFieldInterface::CS_USED_POINTS,
            ['type' => 'decimal']
        );

        $this->addAttributeIfNotExists(
            'order_item',
            SalesFieldInterface::CS_USED_POINTS,
            [
                'type'      => Table::TYPE_DECIMAL,
                'visible'   => false,
                'nullable'  => true
            ]
        );
    }

    private function addQuoteAttributes(): void
    {        
        $this->addAttributeIfNotExists('quote', SalesFieldInterface::CS_USED_POINTS, ['type' => 'decimal']);     
        
        $this->addAttributeIfNotExists(
            'quote_item',
            SalesFieldInterface::CS_USED_POINTS,
            [
                'type'      => Table::TYPE_DECIMAL,
                'visible'   => false,
                'nullable'  => true
            ]
        );
    }

    private function addInvoiceAttributes(): void
    {        
        $this->addAttributeIfNotExists('invoice', SalesFieldInterface::CS_USED_POINTS, ['type' => 'decimal']);        
        $this->addAttributeIfNotExists(
            'invoice_item',
            SalesFieldInterface::CS_USED_POINTS,
            [
                'type'      => Table::TYPE_DECIMAL,
                'visible'   => false,
                'nullable'  => true
            ]
        );
    }

    private function addCreditmemoAttributes(): void
    {        
        $this->addAttributeIfNotExists('creditmemo', SalesFieldInterface::CS_USED_POINTS, ['type' => 'decimal']);        
        $this->addAttributeIfNotExists(
            'creditmemo_item',
            SalesFieldInterface::CS_USED_POINTS,
            [
                'type'      => Table::TYPE_DECIMAL,
                'visible'   => false,
                'nullable'  => true
            ]
        );
    }

    /**
     * @param string $entityTypeCode
     * @param string $attributeCode
     * @param array $params
     * @return void
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    private function addAttributeIfNotExists(
        string $entityTypeCode,
        string $attributeCode,
        array $params
    ): void {
        $tableName = $this->resourceConnection->getTableName($this->flatEntityTables[$entityTypeCode]);
        $connection = $this->resourceConnection->getConnection();

        if (!$connection->tableColumnExists($tableName, $attributeCode)) {
            if ($entityTypeCode === 'quote' || $entityTypeCode === 'quote_item') {
                $this->quoteSetup->addAttribute($entityTypeCode, $attributeCode, $params);
            } else {
                $this->salesSetup->addAttribute($entityTypeCode, $attributeCode, $params);
            }
        }
    }
}
