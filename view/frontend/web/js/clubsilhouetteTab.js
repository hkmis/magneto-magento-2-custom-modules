define([

    'jquery',

    'mage/url',

    'Magento_Ui/js/modal/modal',

    'domReady!',

], function ($, urlBuilder,modal,  message) {

    "use strict";

  

    return function (config) {

       

        // Update Shipping address

        $('body').on('click', '#new-add', function() {                

            var options = {

                        type: 'popup',

                        modalClass: "clubs-addresspopup",

                        title: 'Shipping Address',

                        responsive: true,

                        innerScroll: true,                

                        buttons: []

                    };



            var popup = modal(options, $('#popup-content'));    



            // Store Membership Sku In Session   

            var currShippingId = $('#new-add').attr('data-id');                             

            $.ajax({

                    url: config.sessionUrl,

                    type: 'POST',

                    dataType: 'json',

                    data: {

                        currShippingId: currShippingId

                    },

                    success: function(response) {

                        if(response.sessionData){

                            console.log(response.sessionData);  

                        }

                    },

                    error: function (xhr, status, errorThrown) {

                        console.log('Error happens. Try again.');

                    }

            });

            $('#popup-content').modal('openModal');  

            $('#my-form').trigger("reset");                    

            });



            // Update Refill Items

            $('body').on('click', '#update-refillitems', function() {  

                var matProduct = $('#matitem').val();

                var bladeProduct = $('#bladeitem').val();

                var membershipProduct = $('#membershiptype').val();

                                

                var action = 'updateRefill';



                $.ajax({

                        url: config.Url,

                        type: 'POST',

                        dataType: 'json',

                        data: {

                            matProduct: matProduct,

                            bladeProduct : bladeProduct,

                            membershipProduct : membershipProduct,

                            action : action

                        },

                        complete: function(data) {

                            setTimeout(location.reload(), 50000);

                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                });



             });



            // Cancel Membership        

            var action = 'cancelMembership';



             $('body').on('click', '#cancel-btn', function() { 

                $.ajax({

                        url: config.Url,

                        type: 'POST',

                        dataType: 'json',

                        data: {                            

                            action : action

                        },

                        complete: function(data) {

                            setTimeout(location.reload(), 50000);

                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                });



        });    



    };

  });

  