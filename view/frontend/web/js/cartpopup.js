define([
    'jquery',
    'mage/url',
    'Magento_Ui/js/modal/modal',
    'domReady!',
], function ($, urlBuilder, modal,  message) {
    "use strict";
  
    return function (config) {
       
        $(document).ready(function () {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                modalClass: "club-silhouette-popup"
            };

            // Get Membership Products            
            var membershipCall = 'checkoutpage';
            $.ajax({
                url: config.membershipsUrl,
                type: 'POST',
                dataType: 'json',
                data: {
                    membershipCall: membershipCall
                },
                success: function (response) {
                    if (response.membershipPlan) {
                        $('#membership-wrapper').append(response.membershipPlan);
                    }
                    if (response.membershipPrice) {
                        $('#pricing').append(response.membershipPrice);
                    }
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });

            var popup = modal(options, $('#modal-content'));
            $('body').on('click', '#club-popup', function () {
                $('#modal-content').modal('openModal');
            });
        });

        // Display Price Section
        $('body').on('click', '.membership-plan-title', function () {
            var sku = $(this).attr('data-sku');
            $('.membership-plan').hide();
            var className = 'membership-plan.' + sku;
            $('.' + className).show();
            $('.membership-plan-title').toggleClass('active'); 
        });

    };
  });
  