define([

    'jquery',

    'mage/url',

    'domReady!',

    'fastselectJs',

], function ($, urlBuilder, message, fastselect) {

    "use strict";

  

    return function (config) {



        $(document).ready(function() {            

            

            // MultiSelect Library Call

            $('#machines-select').fastselect();



            // Get Membership Products Data            

            $.ajax({

                url: config.membershipsUrl,

                type: 'GET',

                dataType: 'json',

            success: function(response) {             

                    if(response.response){

                        $('#membership-plans').append(response.response);

                    }

                },

                error: function (xhr, status, errorThrown) {

                    console.log('Error happens. Try again.');

                }

            });



                $('body').on('click', '.membership-plan-select', function() { 
                    
                    $('#refill-items-main').show();

                    $('#membership-main').hide();

                    $('.back-btn').attr('href','javascript:void(0)');

                    // Get RefillItems Data                    
                    $('body').trigger('processStart');
                    $.ajax({

                        url: config.refillproductsUrl,

                        type: 'GET',

                        dataType: 'json',

                    success: function(response) {                                                                  

                            if(response.matItems){                                

                                $('#second-sec-content').append(response.matItems);

                            }

                            if(response.bladeItems){                                

                                $('#third-sec-content').append(response.bladeItems);

                            }
                            $('body').trigger('processStop');                            

                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                    });



                    // Get Machines Data                

                    $.ajax({

                        url: config.machinesUrl,

                        type: 'GET',

                        dataType: 'json',

                    success: function(response) {

                            if(response.machines){                                

                                $('#machines-select').append(response.machines);

                            }

                            if(response.shipmentDates){                                

                                $('#shipment-dates').append(response.shipmentDates);

                            }                            

                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                    });                                    



                    // Store Membership Sku In Session                                        

                    var membershipSku = $(this).attr('data-sku');

                    $.ajax({

                        url: config.sessionUrl,

                        type: 'POST',

                        dataType: 'json',

                        data: {

                            membershipSku: membershipSku

                        },

                        success: function(response) {

                            console.log('Success');

                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                    });                   

                }); 



            // Get RefillItems Updated Data

            $('#machines-select').on('change', function(){    
                
                var machinesSku = $(this).val();                
                $('body').trigger('processStart');            
                    $.ajax({

                        url: config.refillproductsUrl,

                        type: 'GET',

                        dataType: 'json',

                        data: {

                            machinesSku : machinesSku

                        },

                        success: function(response) {                                                                  

                            if(response.matItems){                                

                                $('#second-sec-content').empty();

                                $('#second-sec-content').append(response.matItems);

                            }

                            if(response.bladeItems){             

                                $('#third-sec-content').empty();                   

                                $('#third-sec-content').append(response.bladeItems);

                            }                            
                            $('body').trigger('processStop');
                        },

                        error: function (xhr, status, errorThrown) {

                            console.log('Error happens. Try again.');

                        }

                    });

            });



            // Add Fill Class in Refill Mat Products                             

            $('body').on('click', '.refillmat', function() {   
                $('body').trigger('processStart');
                    if($(this).hasClass('active')){

                        $(this).toggleClass('fill');

                        // Store Membership Sku In Session                                            

                        var refillitem1 = $(this).attr('data-sku');

                        $.ajax({

                            url: config.sessionUrl,

                            type: 'POST',

                            dataType: 'json',

                            data: {

                                refillitem1: refillitem1

                            },

                            success: function(response) {

                            $('body').trigger('processStop');
                            console.log('sucess');  

                            },

                            error: function (xhr, status, errorThrown) {

                                console.log('Error happens. Try again.');

                            }

                        });

                    }                       

            });



            // Add Fill Class in Refill Blade Products                             

            $('body').on('click', '.refillblade', function() {   

                    if($(this).hasClass('active')){

                        $(this).toggleClass('fill');

                        // Store Membership Sku In Session                                            

                        var refillitem2 = $(this).attr('data-sku');
                        $('body').trigger('processStart');

                        $.ajax({

                            url: config.sessionUrl,

                            type: 'POST',

                            dataType: 'json',

                            data: {

                                refillitem2: refillitem2

                            },

                            success: function(response) {

                            $('body').trigger('processStop');
                            console.log('sucess');  

                            },

                            error: function (xhr, status, errorThrown) {

                                console.log('Error happens. Try again.');

                            }

                        });   

                    }                    

            });



            // Add product to cart

            $('body').on('click', '#continue-btn', function() {                       

                    var shippingaddressId = $('#shippingaddress').val();

                    // Store Membership Sku In Session                                        
                    $('body').trigger('processStart');
                        $.ajax({

                            url: config.sessionUrl,

                            type: 'POST',

                            dataType: 'json',

                            data: {

                                shippingaddressId: shippingaddressId

                            },

                            success: function(response) {

                                if(response.sessionData){

                                    console.log(response.sessionData);  

                                }                                
                                $('body').trigger('processStop');
                            },

                            error: function (xhr, status, errorThrown) {

                                console.log('Error happens. Try again.');

                            }

                        });

                    

                    // add product to cart ajax                    

                        $.ajax({

                            url: config.addtocartUrl,

                            type: 'POST',

                            dataType: 'json',

                            data: {

                                shippingaddressId: shippingaddressId

                            },

                            success: function(response) {

                                window.location.href = config.checkoutUrl;

                            },

                            error: function (xhr, status, errorThrown) {

                                console.log('Error happens. Try again.');

                            }

                        });   

            });



            //Back Button Check

            $('body').on('click', '.back-btn', function(e) { 

                var href = $(this).attr('href');

                if(href == 'javascript:void(0)'){

                    $('#membership-main').show();

                    $('#refill-items-main').hide();

                    $(this).attr('href','club-silhouette');

                    e.preventDefault();

                }                

            });

            

            //Continue button enable

            setInterval(function () {

                if($('.refillmat').hasClass('fill') && $('.refillblade').hasClass('fill') 

                && $('#terms').prop('checked'))

                { 

                    jQuery('#continue-btn').removeAttr("disabled");

                }else{

                    jQuery('#continue-btn').prop('disabled', true);

                }

            }, 2000);

        });

    };

  });

  