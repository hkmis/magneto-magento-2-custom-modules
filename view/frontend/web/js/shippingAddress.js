define([

    'jquery',

    'mage/url',

    'Magento_Ui/js/modal/modal',

    'domReady!',

], function ($, urlBuilder, modal,  message) {

    "use strict";

  

    return function (config) {

       

        $(document).ready(function() { 



            $('body').on('click', '#shippingaddress', function() {

                    var shipping = $(this).val();

                    var options = {

                                type: 'popup',

                                modalClass: "clubs-addresspopup",

                                title: 'Shipping Address',

                                responsive: true,

                                innerScroll: true,                

                                buttons: []

                            };



                    var popup = modal(options, $('#popup-content'));

                    if(shipping == 'newaddress'){                            

                        $('#popup-content').modal('openModal');  

                        $('#my-form').trigger("reset");

                    }



             });



        });



    };

  });

  