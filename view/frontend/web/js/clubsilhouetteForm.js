define([
    'jquery',
    'mage/url',
    'domReady!',
], function ($, urlBuilder,  message) {
    "use strict";
  
    return function (config) {
       
        $(document).ready(function() {            
            var allowCountry = config.allowCountry;       
            var baseUrl = config.baseUrl;
            var isMemberRedirect = config.isMemberRedirect;
            var isMember = config.isMember;
            
            if(allowCountry == 0)
            {
                alert('Club Silhouette is not available in your area. We apologize for any inconvenience.');
                window.location.href = baseUrl;
            }

            if(isMember)
            {
                window.location.href = isMemberRedirect;
            }
        });

    };
  });
  