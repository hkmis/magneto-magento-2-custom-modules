define([
    'jquery',
    'mage/url',    
    'domReady!',
], function ($, urlBuilder,  message) {
    "use strict";
  
    return function (config) {
       
        $('body').on('click', '#submit-btn', function(e) {                
            if ($('#my-form').valid()) {
                e.preventDefault();                    
                // Store Shippingaddress
                
                var firstname = $('#firstname').val();
                var street_1 = $('#street_1').val();
                var lastname = $('#lastname').val();
                var street_2 = $('#street_2').val();
                var region_id = $('#region_id').val();
                var region = $('#region').val();
                var country = $('#country').val();
                var city = $('#city').val();
                var postcode = $('#postcode').val();
                var company = $('#company').val();
                var telephone = $('#telephone').val();
                var action = 'addAdd';
                
                $.ajax({
                        url: config.shippingUrl,
                        type: 'POST',
                        dataType: 'json',                            
                        data: {   
                            firstname : firstname, lastname : lastname, street_1 :  street_1,
                            street_2 : street_2, region_id : region_id,  region : region, country : country,
                            city : city, postcode : postcode, company : company,  telephone : telephone,
                            action : action
                            },
                        success: function(response) {                                
                                if(response.shippingHtml){
                                    $("#popup-content").modal("closeModal");
                                    $('#shippingaddress').empty();
                                    $('#shippingaddress').append(response.shippingHtml);
                                }
                        },
                        error: function (xhr, status, errorThrown) {
                            console.log('Error happens. Try again.');
                        }
                });
            }

        });

    };
  });
  