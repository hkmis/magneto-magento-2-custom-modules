<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Cron;

use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;

class Clubuser 
{
    protected $logger;
    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger,
    Config $config,
    Data $helper
    ) 
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->helper = $helper;
    }
    
    /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {
        if($this->config->getModuleStatus())
        {
            $this->logger->info('Cron Works1');

            //deactivate expired members
            $count = $this->helper->deactivateExpiredMembers();
            $this->logger->info('Members expired.');
            $this->logger->info(print_r($count, true));

            //cancel long-expired memberships
	        $count = $this->helper->cancelLongExpiredMembers();
            $this->logger->info('Long-expired memberships canceled');
            $this->logger->info(print_r($count, true));

            //reactivate inactive members
            $count = $this->helper->reactivateInactiveMembers();
            $this->logger->info('Members reactivated');
            $this->logger->info(print_r($count, true));	        

        }
        
    }
}