<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Cron;

use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;
use DateTime;
use DateInterval;

class ClubuserRenewals 
{
    private const CRON_FREQUENCY_HOURS = 24;

    protected $logger;
    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger,
    Config $config,
    Data $helper
    ) 
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->helper = $helper;
    }
    
    /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {
        if($this->config->getModuleStatus())
        {
            $this->logger->info('Renewal Cron Start');

            $end_date = date('Y-m-d H:i:s', time() + (60 * 60 * self::CRON_FREQUENCY_HOURS));            
            $end_date_plus_month = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', strtotime($end_date)) . ' +1 month'));
            $mod = 1;
            $pending_payments_arr = array();
            $remainder = 0;            

            //remove stacked payments for users, since they were inactive in previous months
	        $this->helper->usersRemoveStackedPayments(true, $mod, $remainder);
            $this->logger->info('Removed Stacked Payments for user.');

            //schedule payments for members whose membership is ending tomorrow, and who don't already have future payments scheduled
            if ($this->helper->scheduleFuturePayments($end_date_plus_month, $mod, $remainder)):                
                $this->logger->info('Scheduled future payments successfully.');
            else:
                $this->logger->info('Club Silhouette Job "Process Renewals": Error scheduling future payments');                
            endif;
            
            $this->logger->info('sending reminder emails.');

            //send reminder emails
            $this->helper->sendPaymentReminderEmails();
            $this->logger->info('payment reminder emails sent');

            //get all un-paid payments that fall within the frequency of the cron job
            $this->logger->info('retrieving pending payments.');
            $pending_payments_arr = $this->helper->pendingPaymentsArr($end_date);            
            $this->logger->info('pending payments retrieved');
            
            //loop through them
            foreach ($pending_payments_arr as $pmt):

                $this->logger->info('User'. $pmt->getUserId());                
                $response = $this->helper->CSProcessRenewal($pmt);
                if ($response['success']):
                    $this->logger->info('Payment successfully processed.');
                else:       
                    $mssg = $response['mssg'];
                    $this->logger->info('Error processing payment from cron.');
                    $this->logger->info(print_r($mssg, true));
                endif;
                $this->logger->info('***************************************');
            endforeach;

            $this->logger->info('cron finished:');
        }
        
    }
}