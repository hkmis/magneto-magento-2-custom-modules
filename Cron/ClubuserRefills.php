<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Cron;

use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magneto\ClubSilhouetteManager\Helper\OrderCreate;
use DateTime;
use DateInterval;

class ClubuserRefills 
{
    protected const CLUBURL = 'https://www.silhouetteamerica.com/club-silhouette';
    protected const ORDERHISTORY_URL = 'https://silhouetteamerica.magneto.co.in/sales/order/history';
    protected $logger;
    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger,
    Config $config,
    Data $helper,
    OrderCreate $orderCreateHelper
    ) 
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->helper = $helper;
        $this->orderCreateHelper = $orderCreateHelper;
    }
    
    /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {        
        $count = 0;
        if($this->config->getModuleStatus())
        {
            $this->logger->info('ClubuserRefills Cron Start');

            // send reminder to review address to all users who will get a shipment in a week
            $review_start_date = new DateTime('today');
            $review_start_date->add(DateInterval::createFromDateString('7 days'));
            $review_end_date = new DateTime($review_start_date->format('Y-m-d 23:59:59'));

            $dateLog = date('Y-m-d H:i:s') . " - Sending reminders to review shipping address for " . $review_start_date->format('Y-m-d H:i:s') . " to " . 
            $review_end_date->format('Y-m-d H:i:s');
            $this->logger->info(print_r($dateLog, true));


            $num_reminders_sent = $this->helper->sendReviewReminderEmails($review_start_date->format('Y-m-d H:i:s'), $review_end_date->format('Y-m-d H:i:s'), 
            self::CLUBURL);

            $this->logger->info('Number of Reminder Sent');
            $this->logger->info(print_r($num_reminders_sent, true));

            //get all users who need to have refills created, and the stock status of warehouse items
            $refills_arr = $this->helper->dueShipments();
            $refills_arr_count = $refills_arr->getData(); 

            //check if there are any refills to send
        if (count($refills_arr_count) > 0):
            foreach ($refills_arr as $row):                  
                    $rowData = $row->getData();
                    $userId = $rowData['userId'];
                    $shippingId = $rowData['shippingId'];
                    $refill1ItemNo = $rowData['refill1ItemNo'];
                    $refill2ItemNo = $rowData['refill2ItemNo'];
                    $refill1ItemNoQty = $this->helper->getProductSalableQty($refill1ItemNo);
                    $refill2ItemNoQty = $this->helper->getProductSalableQty($refill2ItemNo);
                    $refill1ItemNoId = $this->helper->getProductId($refill1ItemNo);
                    $refill2ItemNoId = $this->helper->getProductId($refill2ItemNo);

                    $refill1ItemName = $this->helper->getProductName($refill1ItemNo);
                    $refill2ItemName = $this->helper->getProductName($refill2ItemNo);
                    $productname = array('refill1ItemName' => $refill1ItemName, 'refill2ItemName' => $refill2ItemName);
                    // Order Data
                    $items = array(array('product_id' => $refill1ItemNoId, 'qty' => 1), array('product_id' => $refill2ItemNoId, 'qty' => 1));
                    $shippingAddress = $this->helper->getShippingInfo($shippingId);
                    $shipStreet = '';
                    $currentStreetData = $shippingAddress->getStreet();
                    if ($currentStreetData) {
                        $shipStreet = $currentStreetData[0];
                    }
                    $websiteId= 1;$sourceCode = '';
                    $regionId = $shippingAddress->getRegion()->getRegionId();
                    
                    $sourceCode = $this->helper->getSourceCode($regionId);
                    $tempOrder = [
                        'shipping_address' =>[
                            'firstname' => $shippingAddress->getFirstname(),
                            'lastname'=> $shippingAddress->getLastname(),
                            'street' => $shipStreet,
                            'city' => $shippingAddress->getCity(),
                            'country_id' => $shippingAddress->getCountryId(),
                            'region' => $shippingAddress->getRegion()->getRegion(),
                            'postcode' => $shippingAddress->getPostcode(),
                            'telephone' => $shippingAddress->getTelephone(),
              
                        ],
                        'customer_id'  => $userId,
                        'email' => $this->helper->getCustomerEmail($userId),
                        'currency_id'  => 'USD',
                        'items'=> $items,
                        'siteId' => $sourceCode,
                        'websiteId' => $websiteId
                    ];
                    //Check if refill prdoucts has salable qty or not                      
                    if($refill1ItemNoQty > 1 && $refill2ItemNoQty > 1)
                    {                           
                        $orderData = $this->orderCreateHelper->createMageOrder($tempOrder);
                        if ($orderData['success'] && $orderData['order_id']!='') {
                            $row->setData('orderCreationDate', date('Y-m-d H:i:s'));
                            $row->setData('orderId', $orderData['order_id']);
                            $row->save();   
                            $count ++;                         
                        }

                    }else{
                        // if there is an inventory shortage, let the user know we were missing an item or items                    
                        $templateId = $this->config->getClubSilhouetteRefillShipOSEmailTemplate();
                        $refill1ItemName = $this->helper->getProductName($refill1ItemNo);
                        $refill2ItemName = $this->helper->getProductName($refill2ItemNo);
                        $url = self::ORDERHISTORY_URL;
                        $productname = array('refill1ItemName' => $refill1ItemName, 
                        'refill2ItemName' => $refill2ItemName, 'url' => $url);                        
                        $this->helper->sendMail($this->helper->getCustomerEmail($userId), $templateId, $productname);
                        $this->logger->info('Stock Quanity not available for '.$refill1ItemName. ' or '. $refill1ItemName);
                    }
                    
            endforeach;
            $this->logger->info('Refill Shipments Generated For '. $count. 'Users.');
        endif; 
        }
        
    }    
}