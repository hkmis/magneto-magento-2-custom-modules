<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Cron;

use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;
use DateTime;
use DateInterval;

class ClubuserExcBundles 
{
    private const CRON_FREQUENCY_HOURS = 24;

    protected $logger;
    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger,
    Config $config,
    Data $helper
    ) 
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->helper = $helper;
    }
    
    /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {
        if($this->config->getModuleStatus())
        {
            $this->logger->info('Renewal Cron Start');

            //get the available bundles
            $bundles_arr = $this->helper->availableBundlesArr();

            //see if there are any to send out
            if (count($bundles_arr) > 0):
                //get all active users
    	        $active_user_ids_arr = $this->helper->activeMemberUserIdArr();
    	        $active_users_count = count($active_user_ids_arr);

                //loop through any bundles that are scheduled
	            foreach ($bundles_arr as $bundle):

                    //loop through users
    		        foreach ($active_user_ids_arr as $user_id):

                        try {
                            $bundleId = $bundle->getBundelId();
                            if ($this->helper->sendBundleToUser($user_id, $bundleId)):
                                $this->logger->info('SUCCESS: ');
                            else:
                                $this->logger->info('FAILURE: ');
                            endif;
                        } catch (Exception $e){
                            $this->logger->info('EXCEPTION: ');
                            $this->logger->info(print_r($e->getMessage(), true));
                        }   

                    endforeach;

                    //flag this batch as complete
                    $bundle->setSent(1);
                    if ($bundle->save()):
                        $this->logger->info('Finished with bundle');
                        $this->logger->info(print_r($bundle->getId(), true));                        
                    else:
                        /*$msg = "Error flagging bundle ID " . $bundle->getBundelId();
                        General::write_log(ClubSilhouetteExclusiveDesignBundle::LOG, "{$msg}\n");
                        mail(EMAIL_WEBMASTER, $msg, "see title");
                        echo "{$msg}\n";*/

                        $this->logger->info('Error flagging bundle ID');
                        $this->logger->info(print_r($bundle->getId(), true));                        

                    endif;
                
                endforeach;
            endif;

            $this->logger->info('Renewal Cron End');

        }
        
    }
}