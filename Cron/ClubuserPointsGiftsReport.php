<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Cron;

use Psr\Log\LoggerInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magneto\ClubSilhouetteManager\Helper\Data;
use DateTime;
use DateInterval;

class ClubuserPointsGiftsReport 
{
    private const CRON_FREQUENCY_HOURS = 24;
    private const INCEPTION_DATE = '2009-09-30';
    private const MATRIX_PAGE = 'club-silhouette-report';
    private const BASE_URL = 'https://silhouetteamerica.magneto.co.in';

    protected $logger;
    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger,
    Config $config,
    Data $helper
    ) 
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->helper = $helper;
    }
    
    /**
    * Write to system.log
    *
    * @return void
    */
    public function execute() 
    {
        if($this->config->getModuleStatus())
        {
            $this->logger->info('Renewal Cron Start');

            $end_date = new DateTime('first day of this month');
            $end_date->add(DateInterval::createFromDateString('-1 second'));
            $start_date = new DateTime('first day of last month');

            $emailVar = [
                'start_date' => $start_date->format('Y-m-d'),
                'end_date' => $end_date->format('Y-m-d'),
                'gift_cards_used_all_time_link' => $this->getPageLink(self::MATRIX_PAGE, ['audit_report_end_date' => $end_date->format('Y-m-d H:i:s'), 'audit_report_start_date' => self::INCEPTION_DATE,'download_audit_reports' => 1, 'report_type' => 'gift-cards-used']),
                'gift_cards_used_link' => $this->getPageLink(self::MATRIX_PAGE, ['audit_report_end_date' => $end_date->format('Y-m-d H:i:s'), 'audit_report_start_date' => $start_date->format('Y-m-d H:i:s'), 'download_audit_reports' => 1, 'report_type' => 'gift-cards-used']),
                'points_by_user_link' => $this->getPageLink(self::MATRIX_PAGE, ['audit_report_end_date' => $end_date->format('Y-m-d H:i:s'), 'audit_report_start_date' => $start_date->format('Y-m-d H:i:s'), 'download_audit_reports' => 1, 'report_type' => 'points-by-user']),
                'gift_cards_purchased_link' => $this->getPageLink(self::MATRIX_PAGE, ['audit_report_end_date' => $end_date->format('Y-m-d H:i:s'), 'audit_report_start_date' => $start_date->format('Y-m-d H:i:s'), 'download_audit_reports' => 1, 'report_type' => 'gift-cards-purchased']),
            ];

            $emails = $this->config->getCsReportsEmail();
            $emails = explode(',', $emails);
            $templateId = $this->config->getCSReportEmailTemplate();
            $count = 0;
            foreach($emails as $email)
            {
                $this->helper->sendMail($email, $templateId, $emailVar);
                $count ++;
            }
            $this->logger->info('Report Mail Send to'.$count.' users.');
            $this->logger->info('Renewal Cron End');


        }
        
    }

    /**
    * Create Page Link
    *
    * @return string
    */
    public function getPageLink($page = null, $query_string_arr = null): ?string
    {   
        $query_string_arr['page'] = $page;
        $query_string = http_build_query($query_string_arr);

        $page_link = self::BASE_URL . '/?' . $query_string;
        return $page_link;

    }
}