<?php
namespace Magneto\ClubSilhouetteManager\Plugin;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Http\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ActionInterface;

class CustomerDataContext
{
    protected $_customerSession;
	protected $_httpContext;

    /**
 	* @param Session $customerSession
 	* @param Context $httpContext
 	*/
    public function __construct(
    	Session $customerSession,
    	Context $httpContext
    ) {
    	$this->_customerSession= $customerSession;
    	$this->_httpContext = $httpContext;
    }

    public function aroundDispatch(
    	ActionInterface $subject,
    	\Closure $proceed,
    	RequestInterface $request
    ) {
    	  $this->_httpContext->setValue(
        	  'customer_id',
        	  $this->_customerSession->getCustomerId(),
        	  false
    	  );

	  $this->_httpContext->setValue(
        	  'customer_email',
        	  $this->_customerSession->getCustomer()->getEmail(),
        	   false
    	   );
    	   return $proceed($request);
    }
}