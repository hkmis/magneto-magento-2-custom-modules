<?php

namespace Magneto\ClubSilhouetteManager\Plugin;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;

class RefundToStoreCredit
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $currency;

    /**
     * @var \Magneto\ClubSilhouetteManager\Api\ManageCustomerStoreCreditInterface
     */
    private $manageCustomerStoreCredit;

    /**
     * @var \Magneto\ClubSilhouetteManager\Model\ConfigProvider
     */
    private $configProvider;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    private $orderRepository;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Pricing\PriceCurrencyInterface $currency,
        \Magneto\ClubSilhouetteManager\Helper\Data $manageCustomerStoreCredit,
        \Magneto\ClubSilhouetteManager\Helper\Config $configProvider,
        \Magento\Sales\Model\OrderRepository $orderRepository
    ) {
        $this->request = $request;
        $this->currency = $currency;
        $this->manageCustomerStoreCredit = $manageCustomerStoreCredit;
        $this->configProvider = $configProvider;
        $this->orderRepository = $orderRepository;
    }

    public function beforeRefund(
        \Magento\Sales\Model\Service\CreditmemoService $subject,
        \Magento\Sales\Api\Data\CreditmemoInterface $creditmemo,
        $offlineRequested = false
    ) {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/beforeRefund.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');
        if ($this->configProvider->getModuleStatus()) {
            $logger->info('Enabled');
            if ($amount = $creditmemo->getClubsilhouettecreditAmount()) {
                $logger->info('1');
                $order = $creditmemo->getOrder();
                $order->setClubsilhouettecreditRefundedAmount($order->getClubsilhouettecreditRefundedAmount() + $amount);
                $order->setClubsilhouettecreditRefundedBaseAmount(
                    $order->getClubsilhouettecreditRefundedBaseAmount() + $creditmemo->getClubsilhouettecreditBaseAmount()
                );
            }            
        }
        $logger->info('2');
        return [$creditmemo, $offlineRequested];
    }

    public function afterRefund(
        \Magento\Sales\Model\Service\CreditmemoService $subject,
        \Magento\Sales\Model\Order\Creditmemo $result
    ) {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/afterRefund.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');
        if ($this->configProvider->getModuleStatus()) {
            $logger->info('Enabled');
            if ($amount = $result->getClubsilhouettecreditBaseAmount()) {
                $logger->info('1');
                $notes = $result->getClubsilhouettecreditUsedPoints().' points refunded for a 
            store purchase (Order #'.$this->orderRepository->get($result->getOrderId())->getIncrementId().')';
            $logger->info('2');
                $this->manageCustomerStoreCredit->addOrSubtractClubPoints(
                    $result->getCustomerId(),
                    $this->manageCustomerStoreCredit::ACTION_RECEIVE_POINTS,
                    $notes,
                    NULL,
                    $amount,                    
                    [$this->orderRepository->get($result->getOrderId())->getIncrementId()]
                );
                $logger->info('3');
            }
        }
        $logger->info('4');
        return $result;
    }
}
