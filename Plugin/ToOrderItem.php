<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Plugin;

use Magento\Quote\Model\Quote\Item\ToOrderItem as QuoteToOrderItem;
use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;

class ToOrderItem
{
    /**
     * AroundConvert
     *
     * @param QuoteToOrderItem $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param array $data
     *
     * @return \Magento\Sales\Model\Order\Item
     */
    public function aroundConvert(
        QuoteToOrderItem $subject,
        \Closure $proceed,
        $item,
        $data = []
    ) {
        // Get Order Item
        $orderItem = $proceed($item, $data);
        // Get Quote Item's additional Options
        $additionalOptions = $item->getOptionByCode('additional_options');
        // Check if there is any additional options in Quote Item
        if ($additionalOptions) {
            if ($additionalOptions->getValue()) {
                // Get Order Item's other options
                $options = $orderItem->getProductOptions();
                // Set additional options to Order Item
                $options['additional_options'] = json_decode($additionalOptions->getValue());
                $orderItem->setProductOptions($options);
            }
        }        
        $storeCredit = $item->getData(SalesFieldInterface::CS_AMOUNT);
        $clubCreditUsed = $item->getData(SalesFieldInterface::CS_USED_POINTS);   

        if ($storeCredit) {
            $orderItem->setData(SalesFieldInterface::CS_AMOUNT, $storeCredit);
        }

        if ($clubCreditUsed) {
            $orderItem->setData(SalesFieldInterface::CS_USED_POINTS, $clubCreditUsed);
        }

        return $orderItem;
    }
    
}
