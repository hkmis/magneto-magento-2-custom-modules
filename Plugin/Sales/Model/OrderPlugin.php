<?php
namespace Magneto\ClubSilhouetteManager\Plugin\Sales\Model;

use Magento\Sales\Model\Order;

/**
 * Consider Store Credit Amount For Refund
 */
class OrderPlugin
{
    public const EPSILON = 0.00001;

    public function afterCanCreditmemo(Order $subject, $result)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/afterCanCreditmemo.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');
        if (!$result || $subject->getClubsilhouettcreditRefundedBaseAmount() === null) {
            $logger->info('First return');
            return $result;
        }
        $logger->info('1');
        $totalInvoiced = $subject->getBaseTotalInvoiced()
            + $subject->getBaseRwrdCrrncyAmtInvoiced()
            + $subject->getBaseCustomerBalanceInvoiced()
            + $subject->getBaseGiftCardsInvoiced()
            + $subject->getClubsilhouettcreditInvoicedBaseAmount();
        $totalRefunded = $subject->getBaseTotalRefunded()
            + $subject->getBaseRwrdCrrncyAmntRefnded()
            + ($subject->getBaseCustomerBalanceInvoiced() ? $subject->getBaseCustomerBalanceRefunded() : 0)
            + $subject->getBaseGiftCardsRefunded()
            + $subject->getClubsilhouettcreditRefundedBaseAmount();

        $logger->info('2');
        if ($this->isGreater($totalInvoiced, $totalRefunded)) {
            return true;
        }
        $logger->info('3');
        $itemsQtyRefunded = 0;
        foreach ($subject->getAllItems() as $item) {
            $itemsQtyRefunded += $item->getQtyRefunded();
        }
        $logger->info('4');
        if ($this->isGreaterThanOrEqual($totalRefunded, (float) $subject->getBaseTotalPaid())
            && $subject->getTotalQtyOrdered() <= $itemsQtyRefunded
        ) {
            return false;
        }
        $logger->info('5');
        return $result;
    }

    /**
     * Compares two float digits.
     *
     * @param float $a
     * @param float $b
     *
     * @return bool
     * @since 101.0.6
     */
    private function isEqual(float $a, float $b): bool
    {
        return abs($a - $b) <= self::EPSILON;
    }

    /**
     * Compares if the first argument greater than the second argument.
     *
     * @param float $a
     * @param float $b
     *
     * @return bool
     * @since 101.0.6
     */
    private function isGreater(float $a, float $b): bool
    {
        return ($a - $b) > self::EPSILON;
    }

    /**
     * Compares if the first argument greater or equal to the second.
     *
     * @param float $a
     * @param float $b
     *
     * @return bool
     * @since 101.0.6
     */
    private function isGreaterThanOrEqual(float $a, float $b): bool
    {
        return $this->isEqual($a, $b) || $this->isGreater($a, $b);
    }
}
