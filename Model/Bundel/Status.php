<?php
namespace Magneto\ClubSilhouetteManager\Model\Bundel;

use Magento\Framework\Data\OptionSourceInterface;

class Status implements OptionSourceInterface
{
    /**
     * Get Options Array
     *
     * @return array
     */
    public function getOptionArray(): array
    {
        $options = ['0' => __('Not Sent'),'1' => __('Sent')];
        return $options;
    }

    /**
     * Get Grid row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }
    
    /**
     * Get Grid row type array for option element.
     *
     * @return array
     */
    public function getOptions(): array
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
    
    /**
     * To options Array
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return $this->getOptions();
    }
}
