<?php

namespace Magneto\ClubSilhouetteManager\Model;

use Magento\Framework\Model\AbstractModel;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouettePayments as Model;

class ClubSilhouettePayments extends AbstractModel
{
    /**
     * Constructor
     *
     * @return \Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouettePayments
     */
    public function _construct()
    {
        $this->_init(Model::class);
    }
}
