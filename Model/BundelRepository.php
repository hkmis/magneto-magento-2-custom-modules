<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magneto\ClubSilhouetteManager\Api\Data\BundelInterface;
use Magneto\ClubSilhouetteManager\Api\Data\BundelInterfaceFactory;
use Magneto\ClubSilhouetteManager\Api\Data\BundelSearchResultsInterface;
use Magneto\ClubSilhouetteManager\Api\Data\BundelSearchResultsInterfaceFactory;
use Magneto\ClubSilhouetteManager\Api\BundelRepositoryInterface;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel as ResourceBundel;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel\CollectionFactory as BundelCollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * BundelRepository Class
 * @package Magneto/ClubSilhouetteManager/Model
 */
class BundelRepository implements BundelRepositoryInterface
{

    /**
     * @var CollectionProcessorInterface
     */
    protected CollectionProcessorInterface $collectionProcessor;

    /**
     * @var BundelCollectionFactory
     */
    protected BundelCollectionFactory $BundelCollectionFactory;

    /**
     * @var BundelInterfaceFactory
     */
    protected BundelInterfaceFactory $BundelFactory;

    /**
     * @var BundelSearchResultsInterfaceFactory
     */
    protected BundelSearchResultsInterfaceFactory $searchResultsFactory;

    /**
     * @var ResourceBundel
     */
    protected ResourceBundel $resource;


    /**
     * @param ResourceBundel $resource
     * @param BundelInterfaceFactory $bundelFactory
     * @param BundelCollectionFactory $bundelCollectionFactory
     * @param BundelSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceBundel $resource,
        BundelInterfaceFactory $bundelFactory,
        BundelCollectionFactory $bundelCollectionFactory,
        BundelSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->bundelFactory = $bundelFactory;
        $this->bundelCollectionFactory = $bundelCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(BundelInterface $bundel): BundelInterface
    {
        try {
            $this->resource->save($bundel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the bundel: %1',
                $exception->getMessage()
            ));
        }
        return $bundel;
    }

    /**
     * @inheritDoc
     */
    public function get($id): BundelInterface
    {
        $bundel = $this->bundelFactory->create();
        $this->resource->load($bundel, $id);
        if (!$bundel->getId()) {
            throw new NoSuchEntityException(__('Bundel with id "%1" does not exist.', $id));
        }
        return $bundel;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ):BundelSearchResultsInterface {
        $collection = $this->bundelCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(BundelInterface $bundel):bool
    {
        try {
            $bundelModel = $this->bundelFactory->create();
            $this->resource->load($bundelModel, $bundel->getId());
            $this->resource->delete($bundelModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Bundel: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id):bool
    {
        return $this->delete($this->get($id));
    }
}

