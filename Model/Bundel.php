<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model;

use Magento\Framework\Model\AbstractModel;
use Magneto\ClubSilhouetteManager\Api\Data\BundelInterface;

/**
 * Bundel Class
 *
 * @package Magneto/ClubSilhouetteManager/Model
 */
class Bundel extends AbstractModel implements BundelInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel::class);
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?string
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id):BundelInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getBundelId(): string|null
    {
        return $this->getData(self::BUNDLE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setBundelId($bundelId):BundelInterface
    {
        return $this->setData(self::BUNDLE_ID, $bundelId);
    }

    /**
     * @inheritDoc
     */
    public function getDateAvailable():string|null
    {
        return $this->getData(self::DATE_AVAILABLE);
    }

    /**
     * @inheritDoc
     */
    public function setDateAvailable($dateAvailable):BundelInterface
    {
        return $this->setData(self::DATE_AVAILABLE, $dateAvailable);
    }

    /**
     * @inheritDoc
     */
    public function getBundleName():string|null
    {
        return $this->getData(self::BUNDLE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setBundleName($bundlename):BundelInterface
    {
        return $this->setData(self::BUNDLE_NAME, $bundlename);
    }

    /**
     * @inheritDoc
     */
    public function getSent():string|null
    {
        return $this->getData(self::SENT);
    }

    /**
     * @inheritDoc
     */
    public function setSent($sent):BundelInterface
    {
        return $this->setData(self::SENT, $sent);
    }

    
        
}

