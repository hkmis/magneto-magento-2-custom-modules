<?php

namespace Magneto\ClubSilhouetteManager\Model;

use Magento\Framework\Model\AbstractModel;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouetteActions as Model;

class ClubSilhouetteActions extends AbstractModel
{
    /**
     * Constructor
     *
     * @return \Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouetteActions
     */
    public function _construct()
    {
        $this->_init(Model::class);
    }
}
