<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Collection Class
 *
 * @package Magneto/ClubSilhouetteManager/Model/ResourceModel/Bundel
 */
class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'id';

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init(
            \Magneto\ClubSilhouetteManager\Model\Bundel::class,
            \Magneto\ClubSilhouetteManager\Model\ResourceModel\Bundel::class
        );
    }
}

