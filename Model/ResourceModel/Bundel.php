<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Bundel Class
 *
 * @package Magneto/ClubSilhouetteManager/Model/ResourceModel
 */
class Bundel extends AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init('_clubSilhouetteExclusiveDesignBundles', 'id');
    }
}

