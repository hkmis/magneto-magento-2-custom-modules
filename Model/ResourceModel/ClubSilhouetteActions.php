<?php
namespace Magneto\ClubSilhouetteManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ClubSilhouetteActions extends AbstractDb
{

    /**
     * @var boolean
     */
    protected $_isPkAutoIncrement = false;
    
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('_clubSilhouetteUserActions', 'id');
    }
}
