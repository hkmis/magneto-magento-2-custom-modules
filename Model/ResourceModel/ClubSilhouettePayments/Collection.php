<?php

namespace Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouettePayments;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePayments as Model;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouettePayments as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
