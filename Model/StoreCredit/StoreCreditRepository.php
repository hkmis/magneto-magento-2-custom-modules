<?php

namespace Magneto\ClubSilhouetteManager\Model\StoreCredit;

use Magneto\ClubSilhouetteManager\Api\StoreCreditRepositoryInterface;

//use Magneto\ClubSilhouetteManager\Model\StoreCredit\StoreCreditFactory;

class StoreCreditRepository implements StoreCreditRepositoryInterface
{
    /**
     * @var StoreCreditFactory
     */
    private $storeCreditFactory;

    /**
     * @var ResourceModel\Collection
     */
    private $storeCreditCollection;

    /**
     * @var array
     */
    private $storeCredits = [];

    /*public function __construct(
        StoreCreditFactory $storeCreditFactory,
        ResourceModel\Collection $storeCreditCollection
    ) {
        $this->storeCreditFactory = $storeCreditFactory;
        $this->storeCreditCollection = $storeCreditCollection;
    }*/

    /**
     * @inheritDoc
     */
    public function getByCustomerId($customerId)
    {
        /*if (!empty($this->storeCredits[$customerId])) {
            return $this->storeCredits[$customerId];
        }*/

        /*if ($storeCredit = $this->storeCreditCollection->getByCustomerId($customerId)) {
            $this->storeCredits[$customerId] = $storeCredit;

            return $storeCredit;
        }*/

        /** @var \Magneto\ClubSilhouetteManager\Api\Data\StoreCreditInterface $storeCredit */
        /*$storeCredit = $this->storeCreditFactory->create();
        $storeCredit->setCustomerId($customerId)
            ->setStoreCredit('0.00');

        $this->storeCredits[$customerId] = $storeCredit;*/
        $storeCredit = 5;
        return $storeCredit;
    }
}
