<?php
namespace Magneto\ClubSilhouetteManager\Model\StoreCredit;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;

class CheckoutConfigProvider implements ConfigProviderInterface
{

    public const CS_BASE_AMOUNT = 'clubsilhouettecredit_base_amount';

    public const CS_AMOUNT = 'clubsilhouettecredit_amount';

    public const CS_USE = 'clubsilhouettecredit_use';
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var StoreCreditRepository
     */
    private $storeCreditRepository;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    private $quote;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        Session $customerSession,
        CheckoutSession $checkoutSession,
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager,
        Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->quote = $checkoutSession->getQuote();
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $result = [];$csPoints = 0;
        $customerId = $this->customerSession->getCustomer()->getId();
        $csUserStatus = $this->helper->getCSUserStatus($customerId);

        if($csUserStatus == 'A')
        {
            $csPoints = $this->helper->getSilhouetteUserPoints(null, $customerId);
            if(!empty($csPoints))
            {
                $csPoints = floatval($csPoints);
            }    
        }
        
        
        $balance = ($this->customerSession->isLoggedIn())
            ? $this->priceCurrency->convertAndRound($csPoints)
            : 0;        

        $result['csPoints'] = [
            'isVisible' => $this->isEnabled()
                && $this->customerSession->isLoggedIn()
                && $balance > 0,
            'csPointsUsed' => (bool)$this->quote->getData(self::CS_USE),
            'csPointsAmount' => $this->quote->getData(self::CS_AMOUNT),
            'csPointsAmountAvailable' => $balance,
            'isTooltipEnabled' => $this->isTooltipEnabled(),
            'tooltipText' => $this->getTooltipText(),
            'encourage' => $this->isEncourage()
        ];

        return $result;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
    /**
     * @return bool
     */
    public function isTooltipEnabled(): bool
    {
        return false;
    }

    /**
     * @return string
     */
    public function getTooltipText(): string
    {
        return 'ToolTips Text';
    }

     /**
      * @return bool
      */
    public function isEncourage(): bool
    {
        return false;
    }
}
