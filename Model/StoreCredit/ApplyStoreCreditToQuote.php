<?php

namespace Magneto\ClubSilhouetteManager\Model\StoreCredit;

use Magneto\ClubSilhouetteManager\Api\ApplyStoreCreditToQuoteInterface;
use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magento\Quote\Model\Quote;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Directory\Model\CurrencyFactory;

class ApplyStoreCreditToQuote implements ApplyStoreCreditToQuoteInterface
{
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * Constructor
     *
     * @param CartRepositoryInterface $cartRepository
     * @param StoreManagerInterface $storeManager
     * @param CurrencyFactory $currencyFactory
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        StoreManagerInterface $storeManager,
        CurrencyFactory $currencyFactory
    ) {
        $this->cartRepository = $cartRepository;
        $this->_storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * @param int $cartId
     * @param float $amount
     * @return float
     */
    public function apply($cartId, $amount, $usedPoints)
    {        
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/custom.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');
        $logger->info($usedPoints);
        /** @var Quote $quote */
        $currencyCode = $this->getCurrentCurrencyCode();
        $usedPoints = $amount;
        $amount = $this->convertPriceAccordingToCurrency($currencyCode, $amount);
        $quote = $this->cartRepository->get($cartId);
        $quote->setData(SalesFieldInterface::CS_USE, 1);
        $quote->setData(SalesFieldInterface::CS_AMOUNT, abs($amount));
        $quote->setData(SalesFieldInterface::CS_USED_POINTS, abs($usedPoints));
        $quote->collectTotals();
        $this->cartRepository->save($quote);

        return $quote->getData(SalesFieldInterface::CS_AMOUNT);
    }

    /**
     * @param int $cartId
     * @return float
     */
    public function cancel($cartId)
    {
        /** @var Quote $quote */
        $quote = $this->cartRepository->get($cartId);
        $quote->setData(SalesFieldInterface::CS_USE, 0);
        $quote->collectTotals();
        $this->cartRepository->save($quote);

        return $quote->getData(SalesFieldInterface::CS_AMOUNT);
    }

    /**
     * Get Current Currency Code
     *
     * @return string
     */
    public function getCurrentCurrencyCode(): ?string
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    /**
     * Get price as per currency
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function convertPriceAccordingToCurrency($currency, $amount): float
    {
        if ($amount) {
            $baseCurrency = $this->_storeManager->getStore()->getBaseCurrency()->getCode();
            $rate = $this->currencyFactory->create()->load($currency)->getAnyRate($baseCurrency);
            $amount = $amount * $rate;
        }
        return $amount;
    }

}
