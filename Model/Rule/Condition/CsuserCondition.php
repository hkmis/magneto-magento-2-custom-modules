<?php

namespace Magneto\ClubSilhouetteManager\Model\Rule\Condition;

use Magento\Rule\Model\Condition\Context;
use Magento\Quote\Model\QuoteFactory;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Config\Model\Config\Source\Yesno as SourceConfig;

/**
 * Class CsuserCondition
 */
class CsuserCondition extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magneto\CartRule\Helper\Data
     */
    protected $helperData;

    /**
     * Constructor
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        QuoteFactory $quoteFactory,
        Data $helperData,
        SourceConfig $sourceConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->quoteFactory = $quoteFactory;
        $this->helperData = $helperData;
        $this->sourceConfig = $sourceConfig;
    }

    /**
     * Load attribute options
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setAttributeOption([
            'cs_user' => __('Club user')
        ]);
        return $this;
    }

    /**
     * Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'boolean';
    }

    /**
     * Get value element type
     * @return string
     */
    public function getValueElementType()
    {
        return 'select';
    }
    /* public function getValueElementType()
    {
        return 'text';
    } */

    /**
     * Get value select options
     * @return array|mixed
     */
    public function GetValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            $this->setData(
                'value_select_options',
                $this->sourceConfig->toOptionArray()
            );
        }
        return $this->getData('value_select_options');        
    }

    /**
     * Validate Customer First Order Rule Condition
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {        
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/CsuserCondition.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $quote = $model->getQuote();
        $customerId = $quote->getCustomer()->getId();
        $logger->info('CustromerId'. $customerId);
        if($customerId)
        {
            $csUserStatus = $this->helperData->getCSUserStatus($customerId);
            $logger->info('Status'. $csUserStatus);
            if($csUserStatus == 'A')
            {
                $model->setData('cs_user', true);
                return parent::validate($model);
            }
                
        }
        
        
    }
}
