<?php

namespace Magneto\ClubSilhouetteManager\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryModel;

class Options implements ArrayInterface
{
    /**
     * @var Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * Construct
     *
     * @param CountryModel         $countryCollectionFactory
     */
    public function __construct(
        CountryModel $countryCollectionFactory
    ) {
        $this->_countryCollectionFactory = $countryCollectionFactory;
    }

    /**
     * GetCountryCollection
     *
     * @return object
     */
    public function getCountryCollection(): ?object
    {
            $collection = $this->_countryCollectionFactory->create()->loadByStore();
            return $collection;
    }
    
    /**
     * GetCountries
     *
     * @return array
     */
    public function getCountries(): ?array
    {
            $options = $this->getCountryCollection()->toOptionArray();
            return $options;
    }

    /**
     * ToOptionArray
     *
     * @return array
     */
    public function toOptionArray(): ?array
    {
        return $this->getCountries();
    }
}
