<?php

namespace Magneto\ClubSilhouetteManager\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;


class Bundles implements ArrayInterface
{
    /**
     * @var Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Construct
     *
     * @param CollectionFactory $productCollectionFactory
     */
    public function __construct(
        CollectionFactory $productCollectionFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * GetCountryCollection
     *
     * @return object
     */
    public function getProductCollection(): ?object
    {
        $attributeSetId = 23;
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('attribute_set_id', $attributeSetId);
        
        return $collection;
    }
    
    /**
     * GetProducts
     *
     * @return array
     */
    public function getProducts(): ?array
    {
        $options = [];
        $collection = $this->getProductCollection();
        $options[] = [
            'value' => '',
            'label' => 'Please select'
        ];
        foreach($collection as $product){

            $options[] = [
                'value' => $product->getId(),
                'label' => $product->getName()
            ];
        }            
        
        return $options;
    }

    /**
     * ToOptionArray
     *
     * @return array
     */
    public function toOptionArray(): ?array
    {
        return $this->getProducts();
    }
}
