<?php

declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model\Total\Quote;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magneto\ClubSilhouetteManager\Api\StoreCreditRepositoryInterface;
use Magento\Framework\App\State;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class StoreCredit extends AbstractTotal
{
    /**
     * @var State
     */
    private $state;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var QuoteCollector
     */
    private $quoteCollectorPool;

    /**
     * @var StoreCreditRepositoryInterface
     */
    private $storeCreditRepository;

    public function __construct(
        State $state,
        PriceCurrencyInterface $priceCurrency,     
        StoreCreditRepositoryInterface $storeCreditRepository
    ) {
        $this->setCode('clubsilhouettecredit');
        $this->state = $state;
        $this->priceCurrency = $priceCurrency;     
        $this->storeCreditRepository = $storeCreditRepository;
    }

    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     */
    public function collect(Quote $quote, ShippingAssignmentInterface $shippingAssignment, Total $total)
    {
        if ($quote->getCustomerId()
            && $quote->getBaseToQuoteRate()
            && $total->getGrandTotal() > 0
        ) {
            $items = $shippingAssignment->getItems();
            $availableBaseCredit = 10;

            if (!$items || !$availableBaseCredit) {
                return $this;
            }

            $storeId = $quote->getStoreId();
            $currency = $quote->getQuoteCurrencyCode();
            $availableCredit = $this->priceCurrency->convertAndRound($availableBaseCredit, $storeId, $currency);            

            $this->calculateGrandTotal($quote, $total);
        }

        return $this;
    }

    /**
     * @param Quote $quote
     * @param Total $total
     * @return array|null
     */
    public function fetch(Quote $quote, Total $total)
    {        
            if ($quote->getData(SalesFieldInterface::CS_USE)) {
                return [
                    'code' => $this->getCode(),
                    'title' => __('Club Silhouette Store Credit'),
                    'value' => '-'.$quote->getData(SalesFieldInterface::CS_AMOUNT)
                ];
            }

            return [
                'code' => $this->getCode() . '_max',
                'title' => __('Club Silhouette Store Credit Max'),
                'value' => $quote->getData(SalesFieldInterface::CS_AMOUNT)
            ];     

        return null;
    }

    /**
     * @param Quote $quote
     * @param Total $total
     */
    private function calculateGrandTotal(Quote $quote, Total $total): void
    {        
        if ($quote->getData(SalesFieldInterface::CS_USE)) {            
            $grandTotal = $total->getGrandTotal() - $quote->getClubsilhouettecreditAmount();
            $grandBaseTotal = $total->getBaseGrandTotal() - $quote->getClubsilhouettecreditAmount();
            if ($grandTotal < 0.0001) {
                $grandTotal = $grandBaseTotal = 0;
            }

            $total->setGrandTotal($grandTotal);
            $total->setBaseGrandTotal($grandBaseTotal);
        } else {
            $quote->setData(SalesFieldInterface::CS_USE, 0);
        }
    }
}
