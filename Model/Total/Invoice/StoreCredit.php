<?php

declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Model\Total\Invoice;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class StoreCredit extends AbstractTotal
{    
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($data);
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param Invoice $invoice
     * @return $this
     */
    public function collect(Invoice $invoice): StoreCredit
    {
        $order = $invoice->getOrder();
        if ($order->getClubsilhouettecreditAmount() > 0) {
            $storeId = (int)$order->getStoreId();
            $currencyCode = (string)$order->getOrderCurrencyCode();
            $leftBaseStoreCredit = 0;
            $invoiceGrandTotal = $invoice->getGrandTotal();
            $invoiceBaseGrandTotal = $invoice->getBaseGrandTotal();                        
            
            $leftBaseStoreCredit = $order->getClubsilhouettecreditAmount();

            $leftStoreCredit = $this->priceCurrency->convertAndRound($leftBaseStoreCredit, $storeId, $currencyCode);
            if ($leftBaseStoreCredit > $invoiceBaseGrandTotal) {
                $invoice->setClubsilhouettecreditAmount($invoiceGrandTotal);
                $invoice->setClubsilhouettecreditBaseAmount($invoiceBaseGrandTotal);
                $invoiceGrandTotal = $invoiceBaseGrandTotal = 0;
            } else {
                $invoiceGrandTotal -= $leftStoreCredit;
                $invoice->setClubsilhouettecreditAmount($leftStoreCredit);
                $invoiceBaseGrandTotal -= $leftBaseStoreCredit;
                $invoice->setClubsilhouettecreditBaseAmount($leftBaseStoreCredit);                
            }

            if ($invoiceGrandTotal < 0.0001) {
                $invoiceGrandTotal = $invoiceBaseGrandTotal = 0;
            }

            $order->setClubsilhouettecreditInvoicedBaseAmount(
                $order->getClubsilhouettecreditInvoicedBaseAmount() + $invoice->getClubsilhouettecreditBaseAmount()
            );

            $order->setClubsilhouettecreditInvoicedAmount(
                $order->getClubsilhouettecreditInvoicedAmount() + $invoice->getClubsilhouettecreditAmount()
            );

            $invoice->setBaseGrandTotal($invoiceBaseGrandTotal);
            $invoice->setGrandTotal($invoiceGrandTotal);
        }

        return $this;
    }
}
