<?php

namespace Magneto\ClubSilhouetteManager\Model\Total\CreditMemo;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class StoreCredit extends AbstractTotal
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var PartialLeftCalculator
     */
    private $partialLeftCalculator;

    public function __construct(        
        RequestInterface $request,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        parent::__construct($data);
        $this->request = $request;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param Creditmemo $creditmemo
     *
     * @return $this
     * @throws LocalizedException
     */
    public function collect(Creditmemo $creditmemo)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/InvoiceRefund.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        if (!$creditmemo->getOrder()->getCustomerId()) {
            return $this;
        }

        $order = $creditmemo->getOrder();        
        $baseAmountEntered = $order->getClubsilhouettecreditUsedPoints();
        $logger->info(print_r($baseAmountEntered, true));

        if ($baseAmountEntered < 0) {
            throw new LocalizedException(__('ClubSilhouette Points Refund couldn\'t be less than zero.'));
        }

        $logger->info('Before Check');
        $returnToStoreCredit = $this->request->getParam('return_to_cs_points');        
        $logger->info('Params Val');
        $logger->info(print_r($returnToStoreCredit, true));

        $creditmemo->setData(SalesFieldInterface::CS_USE, $returnToStoreCredit);

        if ($returnToStoreCredit) {
            $logger->info('1');
            if ($baseAmountEntered > $creditmemo->getGrandTotal()) {
                $baseAmountEntered = $creditmemo->getGrandTotal();
            }
            $this->collectAllSegments($creditmemo, $baseAmountEntered);  // checkbox "Refund to Store Credit" is checked
        } else {
            $logger->info('2');
            $this->collectAppliedStoreCredits($creditmemo);  // checkbox "Refund to Store Credit" is unchecked
        }

        $logger->info('3');
        return $this;
    }

    /**
     * @param Creditmemo $creditmemo
     * @param float|null $baseAmountEntered
     */
    public function collectAllSegments(Creditmemo $creditmemo, ?float $baseAmountEntered): void
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/collectAllSegments.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $order = $creditmemo->getOrder();
        $storeId = (int)$order->getStoreId();
        $currencyCode = (string)$order->getOrderCurrencyCode();
        $leftBaseStoreCredit = 0;
        
        $leftCreditShippingAmnt = $creditmemo->getBaseShippingAmount();        
        $isChecked = $this->request->getParam('return_to_cs_points');
        $logger->info('isChecked');
        $logger->info(print_r($isChecked, true));

        if ($baseAmountEntered !== null && $isChecked) {            
            $leftBaseStoreCredit += $order->getClubsilhouettecreditAmount();
            $logger->info('leftBaseStoreCredit');
            $logger->info(print_r($leftBaseStoreCredit, true));

            // we cannot return to credit cart (or other) more than customer paid from it
            if ($baseAmountEntered < $leftBaseStoreCredit) {
                $baseStoreCreditAmount = $leftBaseStoreCredit;
                $storeCreditAmount
                    = $this->priceCurrency->convertAndRound($leftBaseStoreCredit, $storeId, $currencyCode);
                $logger->info('storeCreditAmount 1St');
                $logger->info(print_r($storeCreditAmount, true));
            } else {
                $baseStoreCreditAmount = $baseAmountEntered;
                $storeCreditAmount = $this->priceCurrency->convertAndRound($baseAmountEntered, $storeId, $currencyCode);
                $logger->info('storeCreditAmount 2');
                $logger->info(print_r($storeCreditAmount, true));
            }
        } else {
            $storeCreditAmount = $creditmemo->getGrandTotal();
            $baseStoreCreditAmount = $creditmemo->getBaseGrandTotal();
        }
        $logger->info('storeCreditAmount');
        $logger->info(print_r($storeCreditAmount, true));
        $logger->info('baseStoreCreditAmount');
        $logger->info(print_r($baseStoreCreditAmount, true));

        $logger->info('isChecked');
        $logger->info(print_r($isChecked, true));

        $this->setTotalsToCreditmemo($creditmemo, $baseStoreCreditAmount, $storeCreditAmount, $leftCreditShippingAmnt);
        $this->setTotalsToCreditmemoItem($creditmemo, $storeCreditAmount);
    }

    /**
     * @param Creditmemo $creditmemo
     */
    public function collectAppliedStoreCredits(Creditmemo $creditmemo): void
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/collectAppliedStoreCredits.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $order = $creditmemo->getOrder();
        $storeId = (int)$order->getStoreId();
        $currencyCode = (string)$order->getOrderCurrencyCode();
        $storeCreditAmount = $baseStoreCreditAmount = $leftBaseStoreCredit = 0;
                
        $leftBaseStoreCredit = $order->getClubsilhouettecreditAmount();
        $logger->info('leftBaseStoreCredit');
        $logger->info(print_r($leftBaseStoreCredit, true));

        $leftStoreCredit = $this->priceCurrency->convertAndRound($leftBaseStoreCredit, $storeId, $currencyCode);
        $logger->info('leftStoreCredit');
        $logger->info(print_r($leftStoreCredit, true));

        $leftCreditShippingAmnt = $creditmemo->getBaseShippingAmount();
        $logger->info('leftCreditShippingAmnt');
        $logger->info(print_r($leftCreditShippingAmnt, true));        

        /*$logger->info('getBaseGrandTotal');
        $grandTotal = $creditmemo->getOrder()->getBaseGrandTotal();
        $grandTotal = $grandTotal - $leftBaseStoreCredit; 
        $logger->info(print_r($grandTotal, true));*/

        if ($leftBaseStoreCredit <= $creditmemo->getBaseGrandTotal()) {
            $logger->info('Inside Condition');
            $storeCreditAmount = $leftStoreCredit;
            $baseStoreCreditAmount = $leftBaseStoreCredit;
        }

        
        $logger->info('storeCreditAmount');
        $logger->info(print_r($storeCreditAmount, true));

        $logger->info('baseStoreCreditAmount');
        $logger->info(print_r($baseStoreCreditAmount, true));

        $this->setTotalsToCreditmemo($creditmemo, $baseStoreCreditAmount, $storeCreditAmount, $leftCreditShippingAmnt);
    }

    /**
     * @return bool
     */
    private function isReturnToStoreCredit(): bool
    {
        $returnToStoreCredit = $this->request->getParam('return_to_cs_points');
        if ($returnToStoreCredit === null) {
            $returnToStoreCredit = 0;
        }

        return (bool)$returnToStoreCredit;
    }

    /**
     * @param Creditmemo $creditmemo
     * @param float $baseStoreCreditAmount
     * @param float $storeCreditAmount
     * @param float $leftCreditShippingAmount
     */
    private function setTotalsToCreditmemo(
        Creditmemo $creditmemo,
        $baseStoreCreditAmount,
        $storeCreditAmount,
        $leftCreditShippingAmount
    ) {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/setTotalsToCreditmemo.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info('Start');

        $order = $creditmemo->getOrder();
        $logger->info('storeCreditAmount');
        $logger->info(print_r($storeCreditAmount, true));
        $logger->info('baseStoreCreditAmount');
        $logger->info(print_r($baseStoreCreditAmount, true));

        $creditmemo->setClubsilhouettecreditAmount($storeCreditAmount);
        $creditmemo->setClubsilhouettecreditBaseAmount($baseStoreCreditAmount);        

        $grandTotal = $creditmemo->getGrandTotal();
        $baseGrandTotal = $creditmemo->getBaseGrandTotal();
        $grandTotal = $grandTotal - $storeCreditAmount;
        $baseGrandTotal = $baseGrandTotal - $baseStoreCreditAmount;

        $isOrderFullyCoveredByStoreCredit = $this->isFloatEmpty($order->getBaseGrandTotal());

        if ($this->isFloatEmpty($baseGrandTotal) || $isOrderFullyCoveredByStoreCredit) {
            $grandTotal = $baseGrandTotal = 0;
            $creditmemo->setAllowZeroGrandTotal(true);
        }
        $logger->info('grandTotal');
        $logger->info(print_r($grandTotal, true));
        $logger->info('baseGrandTotal');
        $logger->info(print_r($baseGrandTotal, true));

        $creditmemo->setGrandTotal($grandTotal);
        $creditmemo->setBaseGrandTotal($baseGrandTotal);
    }

    /**
     * @param Creditmemo $creditmemo
     * @param float|null $storeCreditAmount
     */
    private function setTotalsToCreditmemoItem(Creditmemo $creditmemo, ?float $storeCreditAmount)
    {
        $order = $creditmemo->getOrder();
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            $orderItem = $order->getItemById($creditmemoItem->getOrderItemId());
            if ($orderItem && $creditmemoItem->getQty() > 0) {
                $creditmemoItem->setData(SalesFieldInterface::CS_AMOUNT, $storeCreditAmount);
            }
        }
    }    

    /**
     * @param float $value
     * @return bool
     */
    private function isFloatEmpty($value)
    {
        return $value < 0.0001;
    }
}
