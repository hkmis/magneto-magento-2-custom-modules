<?php
namespace Magneto\ClubSilhouetteManager\Model;

use Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouetteUsers\CollectionFactory;
use Magento\Customer\Model\CustomerFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;
    
    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $clubsilhouetteCollectionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerfactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $clubsilhouetteCollectionFactory,
        CustomerFactory $customerfactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $clubsilhouetteCollectionFactory->create();
        $this->customerfactory = $customerfactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    
    /**
     * Get Data
     *
     * @return object
     */
    public function getData(): object
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $clubUsers) {
            $this->loadedData[$clubUsers->getId()] = $clubUsers->getData();
        }
        $data = $this->loadedData;

        // Convert the data array to an object
        $objectData = new \Magento\Framework\DataObject();
        $objectData->setData($data);

        return $objectData;
    }
}
