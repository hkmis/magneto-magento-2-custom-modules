<?php

namespace Magneto\ClubSilhouetteManager\Model\Adminhtml\System\Config\Source\Customer;

use Magento\Email\Model\ResourceModel\Template\CollectionFactory as EmailTemplateCollectionFactory;

class EmailTemplates implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * Constructor
     *
     * @param EmailTemplateCollectionFactory $emailCollectionFactory
     */
    public function __construct(
        EmailTemplateCollectionFactory $emailCollectionFactory
    ) {
        $this->emailCollectionFactory = $emailCollectionFactory;
    }

    /**
     * To options array
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $templates = $this->emailCollectionFactory->create();
        $options = [];
        foreach ($templates as $template) {
            $options[] = [
                'value' => $template->getId(),
                'label' => $template->getTemplateCode()
            ];
        }
        return $options;
    }
}
