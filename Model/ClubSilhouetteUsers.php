<?php

namespace Magneto\ClubSilhouetteManager\Model;

use Magento\Framework\Model\AbstractModel;
use Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouetteUsers as Model;

class ClubSilhouetteUsers extends AbstractModel
{
    /**
     * Constructor
     *
     * @return \Magneto\ClubSilhouetteManager\Model\ResourceModel\ClubSilhouetteUsers
     */
    public function _construct()
    {
        $this->_init(Model::class);
    }
}
