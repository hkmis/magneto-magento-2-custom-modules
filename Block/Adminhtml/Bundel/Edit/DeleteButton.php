<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Bundel\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * BackButton Class
 * @package Magneto/ClubSilhouetteManager/Block/Adminhtml/Bundel/Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * Get Button Data
     *
     * @return array
     */
    public function getButtonData():array
    {
        $data = [];
        if ($this->getModelId()) {
            $data = [
                'label' => __('Delete Bundel'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * Get URL for delete button
     *
     * @return string
     */
    public function getDeleteUrl():string
    {
        return $this->getUrl('*/*/delete', ['id' => $this->getModelId()]);
    }
}

