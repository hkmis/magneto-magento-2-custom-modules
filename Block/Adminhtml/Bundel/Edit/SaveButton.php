<?php

declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Bundel\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * SaveButton Class
 * @package Magneto/ClubSilhouetteManager/Block/Adminhtml/Bundel/Edit
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData():array
    {
        return [
            'label' => __('Save Bundle'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}

