<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Bundel\Edit;

use Magento\Backend\Block\Widget\Context;

/**
 * GenericButton Class
 * @package Magneto/ClubSilhouetteManager/Block/Adminhtml/Bundel/Edit
 */
abstract class GenericButton
{

    protected Context $context;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|string|null
     */
    public function getModelId():int|string|null
    {
        return $this->context->getRequest()->getParam('id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl(string $route = '', array $params = []):string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}

