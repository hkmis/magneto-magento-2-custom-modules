<?php
namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\CreditMemo;

use Magneto\ClubSilhouetteManager\Api\Data\SalesFieldInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magento\Backend\Block\Template;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order\Creditmemo;

class NewReturn extends Template
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var PriceCurrencyInterface
     */
    private $currency;

    /**
     * @var Config
     */
    private $configProvider;

    public function __construct(
        Registry $coreRegistry,
        Template\Context $context,
        PriceCurrencyInterface $currency,
        Config $configProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->currency = $currency;
        $this->configProvider = $configProvider;
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        $memo = $this->getCreditMemo();
        if (!$this->configProvider->getModuleStatus()
            || !($memo && $memo->getCustomerId())
        ) {
            return '';
        }

        return parent::toHtml();
    }

    /**
     * @return bool
     */
    public function isUseStoreCredit()
    {
        return $this->getCreditMemo() && $this->getCreditMemo()->getData(SalesFieldInterface::CS_USE);
    }

    /**
     * @return float|int
     */
    public function getMaxStoreCredit()
    {
        if ($memo = $this->getCreditMemo()) {
            if ($memo->getData('clubsilhouettecredit_base_amount') !== null) {
                return $this->currency->roundPrice($memo->getData('clubsilhouettecredit_base_amount'));
            }
            return $this->currency->roundPrice($memo->getBaseGrandTotal());
        }

        return 0;
    }

    /**
     * @return Creditmemo|null
     */
    public function getCreditMemo(): ?Creditmemo
    {
        return $this->coreRegistry->registry('current_creditmemo');
    }
}
