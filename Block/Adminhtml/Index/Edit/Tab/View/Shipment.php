<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Index\Edit\Tab\View;

use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\AreaList;
use Magento\Backend\Block\Template\Context;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as ClubUsers;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePaymentsFactory as ClubPayments;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Shipment extends \Magento\Backend\Block\Template
{
    private const STARTDATE = "2001-01-01 00:00:00";
    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $helper
     * @param Http $request
     * @param AreaList $areaList
     */
    public function __construct(
        Context $context,
        Data $helper,
        Http $request,
        AreaList $areaList,
        ClubUsers $clubUsers,
        ClubPayments $clubPayments,
        TimezoneInterface $timezoneInterface
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->request = $request;
        $this->areaList = $areaList;
        $this->clubUsers = $clubUsers;
        $this->clubPayments = $clubPayments;
        $this->timezoneInterface = $timezoneInterface;
    }

    /**
     * Get Admin Url
     *
     * @return string
     */
    public function getAdminUrl(): string
    {
        $adminUrl = $this->areaList->getFrontName('adminhtml');
        return $adminUrl;
    }

    /**
     * Get Current Id
     *
     * @return string
     */
    public function getId() : ?string
    {
        $id = $this->request->getParam('id');
        return $id;
    }

    /**
     * Get User Id
     *
     * @return string
     */
    public function getUserId() : ?string
    {
        $userId = '';
        $id = $this->getId();
        $collection = $this->clubUsers->create()->getCollection()->addFieldToFilter('id', $id);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $userId = $collectionData[0]['userId'];
        }
            
        return $userId;
    }

    /**
     * Date Formate
     *
     * @param date $date
     * @return mixed
     */
    public function dateFormate($date) : mixed
    {
        $newDate = $this->timezoneInterface->date(new \DateTime($date))->format('M d,Y');
        return $newDate;
    }

    /**
     * Get Mat Products
     *
     * @return array
     */
    public function getMatProducts(): ?array
    {
        $matProducts = $this->helper->getRefillProducts($this->helper::REFILL_PRODUCT_MAT);
        return $matProducts;
    }

    /**
     * Get Blade Products
     *
     * @return array
     */
    public function getBladeProducts(): ?array
    {
        $bladeProducts = $this->helper->getRefillProducts($this->helper::REFILL_PRODUCT_BLADE);
        return $bladeProducts;
    }

    /**
     * Get Current User Mat Product
     *
     * @return string
     */
    public function getCurrentUserMatProd(): ?string
    {
        $matProduct = '';
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $matProduct = $collectionData[0]['refill1ItemNo'];
        }
                
        return $matProduct;
    }

    /**
     * Get Current User Mat Product
     *
     * @return string
     */
    public function getCurrentUserBladeProd(): ?string
    {
        $bladeProduct = '';
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $bladeProduct = $collectionData[0]['refill2ItemNo'];
        }
        
        return $bladeProduct;
    }

    /**
     * Get Shipping Id
     *
     * @return array
     */
    public function getShippingInfo(): ?array
    {
        $shippingInfo = [];
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $shippingId = $collectionData[0]['shippingId'];
        }
        if ($shippingId) {
            $shippingId = explode(',', $shippingId);
            if (is_array($shippingId)) {
                foreach ($shippingId as $id) {
                    $shippingData = $this->helper->getShippingInfo($id);
                    $addressId = $shippingData->getId();
                    $name =  $shippingData->getFirstName().' '.$shippingData->getLastName();
                    $city = $shippingData->getCity();
                    $showAddress = $name.','.$city;
                    $shippingCollect = [];
                    $shippingCollect['id'] = $addressId;
                    $shippingCollect['data'] = $showAddress;
                    
                    $shippingInfo[] = $shippingCollect;
                }
            }
        }
        return $shippingInfo;
    }

     /**
      * Get Shipping Id
      *
      * @return string
      */
    public function getShippingId(): ?string
    {
        $shippingId = '';
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $shippingId = $collectionData[0]['shippingId'];
        }

        return $shippingId;
    }

    /**
     * Get ShippedShipments of user
     *
     * @return array
     */
    public function getShippedShipments(): ?array
    {
        $pastShipmentData = [];
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();

        if ($collectionData) {
            $startDate = self::STARTDATE;
            $endDate = $this->timezoneInterface->date()->format('Y-m-d H:i:s');
            $matProduct =$collectionData[0]['refill1ItemNo'];
            $bladeProduct =$collectionData[0]['refill2ItemNo'];
            $pastShipmentData['matProduct'] = $matProduct;
            $pastShipmentData['bladeProduct'] = $bladeProduct;

            $shippingCollection = $this->helper->getShippedShipments($userId, $startDate, $endDate);
            
            if ($shippingCollection) {
                $shippingDate = [];
                foreach ($shippingCollection as $shipColl) {
                    $shippingDate[] = $shipColl['scheduledDate'];
                }
                $pastShipmentData['shippingDate'] = $shippingDate;
                
            }
            
            return $pastShipmentData;
        }
    }

    /**
     * Get UnshippedShipments of user
     *
     * @return array
     */
    public function getUnshippedShipments(): ?array
    {
        $pastUnshippedtData = [];
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();

        if ($collectionData) {
            $startDate = self::STARTDATE;
            $endDate = $this->timezoneInterface->date()->format('Y-m-d H:i:s');
            
            $matProduct =$collectionData[0]['refill1ItemNo'];
            $bladeProduct =$collectionData[0]['refill2ItemNo'];
            $pastUnshippedtData['matProduct'] = $matProduct;
            $pastUnshippedtData['bladeProduct'] = $bladeProduct;

            $shippingCollection = $this->helper->getUnshippedShipments($userId, $startDate, $endDate);
            
            if ($shippingCollection) {
                $pastUnshipped = [];
                foreach ($shippingCollection as $shipColl) {
                    $shippingData = [];
                    $shippingData['shippingDate'] = $shipColl['scheduledDate'];
                    $shippingData['id'] = $shipColl['id'];
                    $pastUnshipped[] = $shippingData;
                }
                $pastUnshippedtData['shippingData'] = $pastUnshipped;
                
            }
            return $pastUnshippedtData;
        }
    }


    /**
     * Get Shipments of user
     *
     * @return array
     */
    public function getUserShipments(): ?array
    {
        $futureShipmentsData = [];
        $userId = $this->getUserId();
        $collection = $this->clubUsers->create()->getCollection();
        $collection->addFieldToFilter('userId', $userId);
        $collectionData = $collection->getData();

        if ($collectionData) {
            $startDate = $this->timezoneInterface->date()->format('Y-m-d H:i:s');
            ;
            $currentDateTime = $this->timezoneInterface->date();
            $futureDateTime = clone $currentDateTime;
            $futureDateTime->add(new \DateInterval('P10Y'));

            // Format the future date and time
            $endDate = $futureDateTime->format('Y-m-d H:i:s');
            
            $matProduct =$collectionData[0]['refill1ItemNo'];
            $bladeProduct =$collectionData[0]['refill2ItemNo'];
            $futureShipmentsData['matProduct'] = $matProduct;
            $futureShipmentsData['bladeProduct'] = $bladeProduct;

            $shippingCollection = $this->helper->getUserShipments($userId, $startDate, $endDate);
            
            if ($shippingCollection) {
                $pastUnshipped = [];
                foreach ($shippingCollection as $shipColl) {
                    $shippingData = [];
                    $shippingData['shippingDate'] = $shipColl['scheduledDate'];
                    $shippingData['id'] = $shipColl['id'];
                    $pastUnshipped[] = $shippingData;
                }
                $futureShipmentsData['shippingData'] = $pastUnshipped;
                
            }
            return $futureShipmentsData;
        }
    }
}
