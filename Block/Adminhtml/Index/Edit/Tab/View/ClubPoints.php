<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Index\Edit\Tab\View;

use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\AreaList;
use Magento\Backend\Block\Template\Context;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magento\Backend\Model\UrlInterface;

class ClubPoints extends \Magento\Backend\Block\Template
{
    public const TYPEKEY = 'type';

    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $helper
     * @param Http $request
     * @param AreaList $areaList
     */
    public function __construct(
        Context $context,
        Data $helper,
        Http $request,
        AreaList $areaList,
        Config $config,
        UrlInterface $_redirect
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->request = $request;
        $this->areaList = $areaList;
        $this->config = $config;
        $this->context = $context;
        $this->_redirect = $_redirect;
    }

    /**
     * get Points
     *
     * @return string|null
     */
    public function getSilhouetteUserPoints(): ?string
    {
        $id = $this->request->getParam('id');
        $points = $this->helper->getSilhouetteUserPoints($id);
        return $points;
    }

    /**
     * Get Join Date
     *
     * @return string
     */
    public function getJoinDate() : ?string
    {
        $id = $this->request->getParam('id');
        $joinDate = $this->helper->getJoinDate($id);
        $time = strtotime($joinDate);
        $joinDate = date('d-m-Y',$time);

        return $joinDate;
    }

    /**
     * Get Admin Url
     *
     * @return string
     */
    public function getAdminUrl(): string
    {
        $adminUrl = $this->areaList->getFrontName('adminhtml');
        return $adminUrl;
    }

    /**
     * Get Current Id
     *
     * @return string
     */
    public function getId() : ?string
    {
        $id = $this->request->getParam('id');
        return $id;
    }

    /**
     * Get Current User Status
     *
     * @return string
     */
    public function getUserStatus(): ?string
    {
        $status = '';
        
        $id= $this->getId();
        $collection = $this->helper->getClubUser(null, $id);
        $collection = $collection->getData();
        foreach ($collection as $data) {
            $status = $data['membershipStatus'];            
        }
        return $status;
    }

    /**
     * Get Current User endDate
     *
     * @return string
     */
    public function getEndDate()
    {
        $endDate = '';
        
        $id= $this->getId();
        $collection = $this->helper->getClubUser(null, $id);
        $collection = $collection->getData();
        foreach ($collection as $data) {
            $endDate = $data['endDate'];            
        }
        $time = strtotime($endDate);
        $endDate = date('Y-m-d',$time);

        return $endDate;
    }

    /**
     * Get Bundle Url
     *
     * @return string
     */
    public function getBundleUrl(): ?string
    {                 
        $setId = $this->config::DEFAULTSETID;                  
        $tempParam = [$this->config::SETKEY=>$setId,
            self::TYPEKEY => $this->config::PRODUCTTYPE];
        $splitButtonOptions = $this->context->getUrlBuilder()->getUrl($this->config::NEWPRODUCTPATH, $tempParam);
        
        return $splitButtonOptions;
    }    
}
