<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Index\Edit\Tab\View;

use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\AreaList;
use Magento\Backend\Block\Template\Context;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteUsersFactory as ClubUsers;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePaymentsFactory as ClubPayments;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Payments extends \Magento\Backend\Block\Template
{
    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $helper
     * @param Http $request
     * @param AreaList $areaList
     */
    public function __construct(
        Context $context,
        Data $helper,
        Http $request,
        AreaList $areaList,
        ClubUsers $clubUsers,
        ClubPayments $clubPayments,
        TimezoneInterface $timezoneInterface
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->request = $request;
        $this->areaList = $areaList;
        $this->clubUsers = $clubUsers;
        $this->clubPayments = $clubPayments;
        $this->timezoneInterface = $timezoneInterface;
    }

    /**
     * Get Admin Url
     *
     * @return string
     */
    public function getAdminUrl(): string
    {
        $adminUrl = $this->areaList->getFrontName('adminhtml');
        return $adminUrl;
    }

    /**
     * Get Current Id
     *
     * @return string
     */
    public function getId() : ?string
    {
        $id = $this->request->getParam('id');
        return $id;
    }

    /**
     * Get User Id
     *
     * @return string
     */
    public function getUserId() : ?string
    {
        $userId = '';
        $id = $this->getId();
        $collection = $this->clubUsers->create()->getCollection()->addFieldToFilter('id', $id);
        $collectionData = $collection->getData();
        if ($collectionData) {
            $userId = $collectionData[0]['userId'];
        }
            
        return $userId;
    }

    /**
     * Get Past Payment Data of user
     *
     * @return array
     */
    public function getPastPayments() : ?array
    {
        $userId = $this->getUserId();
        if ($userId) {
            $collection = $this->clubPayments->create()->getCollection();
            $collection->addFieldToFilter('userId', $userId);
            $collection->addFieldToFilter('paymentDate', ['neq' => '0000-00-00 00:00:00']);
            $collection->setOrder('paymentDate', 'DESC');
            $collection = $collection->getData();
            return $collection;
        }
    }

    /**
     * Get Future Payment Data of user
     *
     * @return array
     */
    public function getFuturePayments() : ?array
    {
        $userId = $this->getUserId();        
        if ($userId) {
            $collection = $this->clubPayments->create()->getCollection();
            $collection->addFieldToFilter('userId', $userId);
            $collection->addFieldToFilter('paymentDate', array('null' => true));
            $collection->setOrder('dueDate', 'ASC');
            $collection = $collection->getData();
            return $collection;
        }
    }

    /**
     * Date Formate
     *
     * @param date $date
     * @return mixed
     */
    public function dateFormate($date) : mixed
    {
        $newDate = $this->timezoneInterface->date(new \DateTime($date))->format('M d,Y');
        return $newDate;
    }

    /**
     * Get Payment Cycle Month
     *
     * @return string
     */
    public function getPaymentCycleMonth(): ?string
    {
        $paymentMonth = '';
        $userId = $this->getUserId();
        if ($userId) {
            $paymentMonth = $this->helper->getPaymentCycleMonth($userId);
            return $paymentMonth;
        }
        return $paymentMonth;
    }
}
