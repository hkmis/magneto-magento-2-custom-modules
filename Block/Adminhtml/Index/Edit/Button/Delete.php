<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Adminhtml\Index\Edit\Button;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Delete extends Generic implements ButtonProviderInterface
{
    /**
     * @var Context
     */
    protected $context;
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->context = $context;
    }
    /**
     * Get button data
     *
     * @return array
     */
    public function getButtonData(): array
    {
        $data = [];
        $blog_id = $this->context->getRequest()->getParam('id');
        if ($blog_id) {
            $data = [
                'label' => __('Delete'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }
    /**
     * GetDeleteUrl
     *
     * @return string
     */
    public function getDeleteUrl(): string
    {
        $maintenanceId = $this->context->getRequest()->getParam('id');
        return $this->getUrl('*/*/delete', ['id' => $maintenanceId]);
    }
}
