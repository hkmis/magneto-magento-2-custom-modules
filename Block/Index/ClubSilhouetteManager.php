<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as httpContext;
use Magento\Framework\UrlInterface;
use Magneto\ClubSilhouetteManager\Helper\Config;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magneto\ClubSilhouetteManager\Helper\Data;
use Magento\Authorization\Model\UserContextInterface; 

class ClubSilhouetteManager extends \Magento\Framework\View\Element\Template
{
    public const ALLOWED_COUNTRIES = 'clubsilhouette/general/availableCountry';
    private const STATUS_ACTIVE = 'A';
    private const STATUS_CANCEL = 'X';
    private const STATUS_EXPIRED = 'I';
    private const STATUS_POTENTIAL = 'P';
    private const STATUS_ERROR = 'E';    
    /**
     * @var object
     */
    protected $_customerSession;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Session $customerSession
     * @param UrlInterface $urlInterface
     * @param Config $config
     * @param RemoteAddress $remoteAddress
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        httpContext $customerSession,
        UrlInterface $urlInterface,
        Config $config,
        RemoteAddress $remoteAddress,
        Data $helper,
        UserContextInterface $userContextInterface,
        $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_urlInterface = $urlInterface;
        $this->config = $config;
        $this->remoteAddress = $remoteAddress;
        $this->helper = $helper;
        $this->userContextInterface = $userContextInterface;
        parent::__construct($context, $data);
    }

    /**
     * Login User Check
     *
     * @return int
     */
    public function isLogin(): ?int
    {        
        $isLogin = '';
        if ($this->_customerSession->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
            $isLogin = 1;
            return $isLogin;
        } else {
            $isLogin = 0;
            return $isLogin;
        }
    }

    /**
     * Get Customer Id
     *
     * @return mixed
     */
    public function getCustomerId(): mixed
    {
        $customerId = '';
        if ($this->_customerSession->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
            $customerId = $this->userContextInterface->getUserId();
            return $customerId;
        } else {
            return $customerId;
        }
    }

    /**
     * Get Login Url
     *
     * @return string
     */
    public function getLoginUrl(): ?string
    {
        $url  = $this->_urlInterface->getUrl('club-silhouette', ['_current' => true, '_use_rewrite' => true]);
        $login_url = $this->_urlInterface->getUrl('customer/account/login', ['referer' => base64_encode($url)]);
        return $login_url;
    }

    /**
     * Get user Country
     *
     * @return array
     */
    public function getUserCountry(): array | string
    {
        // Get the user's IP address
        $userIp = $this->remoteAddress->getRemoteAddress();
        //$userIp = '147.135.77.241';
        
        // Request IP geolocation data from ip-api.com
        $url = "http://ip-api.com/json/" . $userIp . "?fields=countryCode";
        $response = file_get_contents($url);
        
        $data = json_decode($response, true);
        if ($data && isset($data['countryCode'])) {
            return $data['countryCode'];
        }

        // Return a default value (e.g., 'UNKNOWN') in case of errors or invalid response
        return 'UNKNOWN';
    }

    /**
     * Check Country
     *
     * @return int
     */
    public function checkCountry(): ?int
    {
        $returntype = 0;

        $userCountry = $this->getUserCountry();        

        $allowedCountries = $this->config->getWebsiteConfig(self::ALLOWED_COUNTRIES);
        if ($allowedCountries) {
            $allowedCountries = explode(',', $allowedCountries);
            if (in_array($userCountry, $allowedCountries)) {
                $returntype = 1;
                return $returntype;
            }
        }            
        
        return $returntype;
    }

    /**
     * Get Base Url
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        $url = $this->_urlInterface->getBaseUrl();
        return $url;
    }

    /**
     * Get Refill Items Mat
     *
     * @return array
     */
    public function getRefillItemsMat(): ?array
    {
        return $this->helper->getRefillProducts($this->helper::REFILL_PRODUCT_MAT);
    }

    /**
     * Get Refill Items Blade
     *
     * @return array
     */
    public function getRefillItemsBlade(): ?array
    {
        return $this->helper->getRefillProducts($this->helper::REFILL_PRODUCT_BLADE);
    }

    /**
     * Get Membership Items
     *
     * @return array
     */
    public function getMembershipProducts(): ?array
    {
        return $this->helper->getMembershipProducts();
    }

    /**
     * Get Helper
     *
     * @return object
     */
    public function getHelper() : ?object
    {
        return $this->helper;
    }

    /**
     * Get Shipping Info
     *
     * @return array
     */
    public function getShippingInfo() : ?array
    {
        $shippingId = '';
        $shippingInfo = [];
        $userId = $this->getCustomerId();
        if ($userId) {
            $clubUser = $this->helper->getClubUser($userId);
            $clubUser = $clubUser->getData();
            if (isset($clubUser)) {
                foreach ($clubUser as $user) {
                    $shippingId = $user['shippingId'];
                }
                if ($shippingId) {
                        $shippingData = $this->helper->getShippingInfo($shippingId);
                        $addressId = $shippingData->getId();
                        $name =  $shippingData->getFirstName().' '.$shippingData->getLastName();
                        $city = $shippingData->getCity();
                        $street = $shippingData->getStreet();
                    if ($street) {
                        $street = $street[0];
                        $streetAdd = $street.', '.$city;
                    } else {
                        $streetAdd = $city;
                    }
                                            
                        $shippingInfo['id'] = $addressId;
                        $shippingInfo['name'] = $name;
                        $shippingInfo['address'] = $streetAdd;
                                                
                }
                
                return $shippingInfo;
            }
        } else {
            return $shippingInfo;
        }
    }

     /**
      * Get Current User Mat Product
      *
      * @return array
      */
    public function getCurrentUserData(): ?array
    {
        $collectionData = '';
        $userId = $this->getCustomerId();
        $collection = $this->helper->getClubUser($userId);
        $collectionData = $collection->getData();
        
        return $collectionData;
    }

    /**
     * Get Status Title
     *
     * @param string $status
     * @return string
     */
    public function getStatusName($status): ?string
    {
        switch ($status):
            case self::STATUS_ACTIVE:
                $display_name = 'Active';
                break;
            case self::STATUS_CANCEL:
                $display_name = 'Cancel';
                break;
            case self::STATUS_EXPIRED:
                $display_name = 'Expired/Inactive';
                break;
            case self::STATUS_ERROR:
                $display_name = 'Error';
                break;
            default:
                $display_name = 'N/A';
        endswitch;

        return $display_name;
    }

    public function getMembershipProductSku()
    {
        $membershipProds = $this->helper->getMembershipProducts();
        $membershipProdSku = [];
        foreach ($membershipProds as $membershipProd) {
            $membershipProdSku[] = $membershipProd['sku'];
        }

        return $membershipProdSku;
    }

    public function isProductExistCart()
    {
        $membershipProdSku = $this->getMembershipProductSku();
        $result = $this->helper->getCustomerCart();
        
        $productExist = 0;
        foreach ($result as $cartItem) {
            $prodSku = $cartItem->getProduct()->getSku();
            if (in_array($prodSku, $membershipProdSku)) {
                $productExist = 1;
            }
        }

        return $productExist;
    }

    /**
     * Get Customer is member or not
     *
     * @return boolean
     */
    public function isMember(): ?bool
    {
        $userId = $this->helper->getLoginCustomerId();

        if($userId){
            return $this->helper->is_member($userId);
        }
        
        return false;
    }    
}
