<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magneto\ClubSilhouetteManager\Helper\Data;

class Popup extends Template
{
    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
    
    /**
     * Get Customer Club Silhouette Points
     *
     * @return int|string|null
     */
    public function getPoints(): int|string|null
    {
        $points = 0;
        $customerId = $this->helper->getLoginCustomerId();
        if ($customerId) {
            $clubUser = $this->helper->getClubUser($customerId);
            foreach ($clubUser as $user) {
                $points = $user->getPoints();
            }
            return $points;
        }
        
        return $points;
    }

    /**
     * Check club user member
     *
     * @return bool
     */
    public function is_member(): ?bool
    {
        $customerId = $this->helper->getLoginCustomerId();
        if($customerId){
            $isMember = $this->helper->is_member($customerId);
            return $isMember;
        }

        return false;
    }
}
