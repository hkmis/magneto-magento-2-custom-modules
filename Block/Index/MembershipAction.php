<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteActions;
use Magento\Framework\Pricing\Helper\Data as priceHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;

class MembershipAction extends Template
{
    private const ACTION_AGREE_TERMS = 'T';
    private const ACTION_CANCEL = 'X';
    private const ACTION_CASH_IN_POINTS = '$';
    private const ACTION_DELAY_NOTIFICATION = 'D';
    private const ACTION_RECEIVE_POINTS = 'P';
    private const ACTION_RENEWAL = 'R';
    private const ACTION_SIGNUP = 'S';
    private const ACTION_TRANSFER_MEMBERSHIP = 'Z';
    private const ACTION_REINSTATE = 'I';
    private const ACTION_UPDATE = 'U';

    protected $customCollection;
    protected $priceHepler;
    /**
     * Constructor
     *
     * @param Context $context
     * @param ClubSilhouetteActions $customCollection
     * @param priceHelper $priceHepler
     */
    public function __construct(
        Context $context,
        ClubSilhouetteActions $customCollection,
        priceHelper $priceHepler,
        TimezoneInterface $timezoneInterface,
        Data $helper
    ) {
        $this->customCollection = $customCollection;
        $this->priceHepler = $priceHepler;
        $this->timezoneInterface = $timezoneInterface;
        $this->helper = $helper;
        parent::__construct($context);
    }
    /**
     * Prepare Layout
     *
     * @return mixed
     */
    protected function _prepareLayout(): mixed
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Membership Action Pagination'));
        if ($this->getCustomCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.membership.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
               ->setPageVarName('m')
               ->setLimitVarName('limitmember')
            ->setShowPerPage(true)->setCollection(
                $this->getCustomCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCustomCollection()->load();
        }
        return $this;
    }
    /**
     * Get Pager Html
     *
     * @return mixed
     */
    public function getPagerHtml(): mixed
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get Collection
     *
     * @return object
     */
    public function getCustomCollection(): ?object
    {
        $page = ($this->getRequest()->getParam('m')) ? $this->getRequest()->getParam('m') : 1;
        $pageSize = ($this->getRequest()->getParam('limitmember')) ? $this->getRequest()->getParam('limitmember') : 5;
        $collection = $this->customCollection->getCollection();
        $customerId = $this->helper->getLoginCustomerId();
        $collection->addFieldToFilter('userId', $customerId);
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    /**
     * Date Formate
     *
     * @param date $date
     * @return mixed
     */
    public function dateFormate($date) : mixed
    {
        $newDate = $this->timezoneInterface->date(new \DateTime($date))->format('Y/m/d h:m:s');
        return $newDate;
    }

    /**
     * Get Eveny Name
     *
     * @param string $action
     * @return string
     */
    public function getEvent($action): ?string
    {
        switch ($action):
            case self::ACTION_AGREE_TERMS:
                $display_name = 'Agreed to terms of membership';
                break;
            case self::ACTION_CANCEL:
                $display_name = 'Cancelled membership';
                break;
            case self::ACTION_CASH_IN_POINTS:
                $display_name = 'Redeemed points';
                break;
            case self::ACTION_DELAY_NOTIFICATION:
                $display_name = 'Inventory delay notification';
                break;
            case self::ACTION_RECEIVE_POINTS:
                $display_name = 'Received points';
                break;
            case self::ACTION_RENEWAL:
                $display_name = 'Renewed membership';
                break;
            case self::ACTION_SIGNUP:
                $display_name = 'Signed up';
                break;
            case self::ACTION_UPDATE:
                $display_name = 'Updated membership settings';
                break;
            default:
                $display_name = 'Updated membership settings';
        endswitch;

        return $display_name;
    }
}
