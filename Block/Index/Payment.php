<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouettePayments;
use Magento\Framework\Pricing\Helper\Data as priceHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;

class Payment extends Template
{
    protected $customCollection;
    protected $priceHepler;
    /**
     * Constructor
     *
     * @param Context $context
     * @param ClubSilhouettePayments $customCollection
     * @param priceHelper $priceHepler
     */
    public function __construct(
        Context $context,
        ClubSilhouettePayments $customCollection,
        priceHelper $priceHepler,
        TimezoneInterface $timezoneInterface,
        Data $helper
    ) {
        $this->customCollection = $customCollection;
        $this->priceHepler = $priceHepler;
        $this->timezoneInterface = $timezoneInterface;
        $this->helper = $helper;
        parent::__construct($context);
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Custom Pagination'));
        if ($this->getCustomCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.history.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
            ->setShowPerPage(true)->setCollection(
                $this->getCustomCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCustomCollection()->load();
        }
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getCustomCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $collection = $this->customCollection->getCollection();
        $customerId = $this->helper->getLoginCustomerId();
        $collection->addFieldToFilter('userId', $customerId);
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    /**
     * Date Formate
     *
     * @param date $date
     * @return mixed
     */
    public function dateFormate($date) : mixed
    {
        $newDate = $this->timezoneInterface->date(new \DateTime($date))->format('Y/m/d');
        return $newDate;
    }
}
