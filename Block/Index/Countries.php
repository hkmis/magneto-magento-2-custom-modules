<?php
declare(strict_types=1);
namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Json\EncoderInterface;
use Magento\Directory\Helper\Data;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionModel;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryModel;
use Magento\Customer\Model\SessionFactory;

class Countries extends \Magento\Directory\Block\Data
{
    protected $_customerSession;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $directoryHelper
     * @param EncoderInterface $jsonEncoder
     * @param Config $configCacheType
     * @param RegionModel $regionCollectionFactory
     * @param CountryModel $countryCollectionFactory
     * @param SessionFactory $customerSession
     */
    public function __construct(
        Context $context,
        Data $directoryHelper,
        EncoderInterface $jsonEncoder,
        Config $configCacheType,
        RegionModel $regionCollectionFactory,
        CountryModel $countryCollectionFactory,
        SessionFactory $customerSession,
        array $data = []
    ) {
        $this->_customerSession = $customerSession->create();
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $data
        );
    }

    public function getFormData()
    {
        $data = $this->getData('form_data');
        if ($data === null) {
            $formData = $this->_customerSession->getCustomerFormData(true);
            $data = new \Magento\Framework\DataObject();
            if ($formData) {
                $data->addData($formData);
                $data->setCustomerData(1);
            }
            if (isset($data['region_id'])) {
                $data['region_id'] = (int)$data['region_id'];
            }
            $this->setData('form_data', $data);
        }
        return $data;
    }

    public function getConfig($path)
    {
        return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
