<?php
declare(strict_types=1);

namespace Magneto\ClubSilhouetteManager\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magneto\ClubSilhouetteManager\Model\ClubSilhouetteRefillShipments;
use Magento\Framework\Pricing\Helper\Data as priceHelper;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magneto\ClubSilhouetteManager\Helper\Data;

class Shipment extends Template
{
    protected $customCollection;
    protected $priceHepler;
    /**
     * Constructor
     *
     * @param Context $context
     * @param ClubSilhouetteRefillShipments $customCollection
     * @param priceHelper $priceHepler
     */
    public function __construct(
        Context $context,
        ClubSilhouetteRefillShipments $customCollection,
        priceHelper $priceHepler,
        TimezoneInterface $timezoneInterface,
        Data $helper
    ) {
        $this->customCollection = $customCollection;
        $this->priceHepler = $priceHepler;
        $this->timezoneInterface = $timezoneInterface;
        $this->helper = $helper;
        parent::__construct($context);
    }
    /**
     * Prepare Layout
     *
     * @return mixed
     */
    protected function _prepareLayout(): mixed
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Shipment Pagination'));
        if ($this->getCustomCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.shipment.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
               ->setPageVarName('s')
               ->setLimitVarName('limitshipment')
            ->setShowPerPage(true)->setCollection(
                $this->getCustomCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCustomCollection()->load();
        }
        return $this;
    }
    /**
     * Get Pager Html
     *
     * @return mixed
     */
    public function getPagerHtml(): mixed
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get Collection
     *
     * @return object
     */
    public function getCustomCollection(): ?object
    {
        $page = ($this->getRequest()->getParam('s')) ? $this->getRequest()->getParam('s') : 1;
        $pageSize = ($this->getRequest()->getParam('limitshipment')) ? $this->getRequest()->getParam('limitshipment') : 5;
        $collection = $this->customCollection->getCollection();
        $customerId = $this->helper->getLoginCustomerId();
        $collection->addFieldToFilter('userId', $customerId);
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    /**
     * Date Formate
     *
     * @param date $date
     * @return mixed
     */
    public function dateFormate($date) : mixed
    {
        $newDate = $this->timezoneInterface->date(new \DateTime($date))->format('Y/m/d h:m:s');
        return $newDate;
    }
}
